using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using MudBlazor;
using MudBlazor.Services;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Reportes.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Data;
using POS_Foraneo.Helper.SPGeneral;

namespace POS_Foraneo
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMudServices(config =>
			{
				config.SnackbarConfiguration.PreventDuplicates = false;
				config.SnackbarConfiguration.NewestOnTop = false;
				config.SnackbarConfiguration.ShowCloseIcon = true;
				config.SnackbarConfiguration.VisibleStateDuration = 3000;
				config.SnackbarConfiguration.HideTransitionDuration = 500;
				config.SnackbarConfiguration.ShowTransitionDuration = 500;
				config.SnackbarConfiguration.SnackbarVariant = Variant.Filled;
				config.SnackbarConfiguration.PositionClass = Defaults.Classes.Position.TopCenter;
			});

			services.AddMudServices();

			services.AddRazorPages();
			services.AddServerSideBlazor();
			services.AddScoped<IDapperManager, DapperManager>();
			services.AddScoped<IAdministracion, Administracion>();
			services.AddScoped<IGenerales, Generales>();
			services.AddScoped<IComandaManager, ComandaManager>();
			services.AddScoped<ICajaRegistradora, CajaRegistradora>();
			services.AddScoped<IReportesManager, ReportesManager>();

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapBlazorHub();
				endpoints.MapFallbackToPage("/_Host");
			});
		}
	}
}
