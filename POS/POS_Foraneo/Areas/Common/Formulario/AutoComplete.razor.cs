﻿using Microsoft.AspNetCore.Components;

using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Common.Formulario
{
	public partial class AutoComplete
	{


		[Parameter]
		public IList<OptionModel> LstOptionModel { get; set; }

		[Parameter]
		public int iIdOpcion { get; set; }

		[Parameter]
		public EventCallback<int> IdSelected { get; set; }

		public OptionModel Option { get; set; }


		protected override void OnInitialized()
		{
			Option = new OptionModel();
		}

		protected override void OnParametersSet()
		{
			if(Option.IdOpcion != iIdOpcion)
			{
				Option = LstOptionModel.Where(e => e.IdOpcion == iIdOpcion).SingleOrDefault();
			}
		}

		public void OnChange()
		{
			iIdOpcion = Option.IdOpcion;

			IdSelected.InvokeAsync(iIdOpcion);
		}

		



		private async Task<IEnumerable<OptionModel>> Search1(string value)
		{
			if (string.IsNullOrEmpty(value))
				return LstOptionModel;
			return LstOptionModel.Where(x => x.vchValue.Contains(value, StringComparison.InvariantCultureIgnoreCase));
		}

	}
}
