﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Common.Modales
{
    public partial class InfoModal
    {
        [CascadingParameter]
        public MudDialogInstance MudDialog { get; set; }

        [Parameter]
        public string vchMensaje { get; set; }



        void Submit() => MudDialog.Close(DialogResult.Ok(true));
       // void Cancel() => MudDialog.Cancel();
    }
}
