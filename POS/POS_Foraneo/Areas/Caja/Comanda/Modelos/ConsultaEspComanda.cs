﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Modelos
{
	public static class ConsultaEspComanda
	{
		public class Request
		{
			public int iIdEspComanda { get; set; }
			public int iIdGralComanda { get; set; }
			public bool bTodas { get; set; }
		}

		public class Result
		{
			public int iIdGralComanda { get; set; }
			public int iIdEspComanda { get; set; }
			public int iIdEstatus { get; set; }
			public string vchEstatus { get; set; } = string.Empty;
			public int iIdCatProducto { get; set; }
			public string vchNombreProducto { get; set; }
			public int iIdEspProductosPrecio { get; set; }
			public decimal mPrecioUnitario { get; set; }
			public string vchDescripcion { get; set; } = string.Empty;
			public string vchComentario { get; set; } = string.Empty;
			public short smCantidad { get; set; }
			public short smDescuento { get; set; }
			public decimal mTotal { get; set; }

		}

		public static Result FromProducto(ConsultaProduto.Result result)
		{
			return new Result()
			{
				iIdEspComanda = -1,
				iIdCatProducto = result.iIdCatProducto,
				vchNombreProducto = result.vchNombre,
				vchDescripcion = $"{result.vchNombreCategoria} - {result.vchNombre}"
			};
		}
	}
}
