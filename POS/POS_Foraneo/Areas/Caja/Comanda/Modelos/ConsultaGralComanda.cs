﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Modelos
{
	public static class ConsultaGralComanda
	{

		public class Request
		{
			public int iIdGralComanda { get; set; }
			public bool bActivas { get; set; }
		}


		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdGralComanda { get; set; }
			public DateTime dtFechaAlta { get; set; } = Default.Date;
			public string vchNombre { get; set; } = string.Empty;
			public string vchComentarioGeneral { get; set; } = string.Empty;
			public int iIdTipoVenta { get; set; }
			public string vchTipoVenta { get; set; } = string.Empty;
			public int iIdEstatus { get; set; }
			public string vchEstatus { get; set; } = string.Empty;
			public decimal mTotalCobrado { get; set; }
			public decimal mSubTotal { get; set; }
			public short smDescuento { get; set; }

			/**/

		}
	}
}
