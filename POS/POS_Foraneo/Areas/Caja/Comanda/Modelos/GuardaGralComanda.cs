﻿using POS_Foraneo.Areas.Objetos;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Modelos
{
	public static class GuardaGralComanda
	{

		public class Request
		{
			public int iIdGralComanda { get; set; }
			[Required]
			public string vchNombre { get; set; } = string.Empty;
			public string vchComentarioGeneral { get; set; } = string.Empty;
			public int iIdTipoVenta { get; set; }
			public int iIdEstatus { get; set; }
			public decimal mTotalCobrado { get; set; }
			public decimal mSubTotal { get; set; }
			public short smDescuento { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdGralComanda { get; set; }

		}

		public static Request ParseFrom(GralComanda comanda)
		{
			return new Request()
			{
				iIdGralComanda = comanda.iIdGralComanda,
				vchNombre = comanda.vchNombre,
				vchComentarioGeneral = comanda.vchComentarioGeneral,
				iIdTipoVenta = comanda.iIdTipoVenta,
				iIdEstatus = comanda.iIdEstatus,
				mTotalCobrado = comanda.mTotalCobrado,
				smDescuento = comanda.smDescuento,
				mSubTotal = comanda.mSubTotal

			};
		}

	}
}
