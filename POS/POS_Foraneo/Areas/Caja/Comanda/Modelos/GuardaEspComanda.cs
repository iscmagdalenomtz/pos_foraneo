﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Objetos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Modelos
{
	public static class GuardaEspComanda
	{


		public class Request
		{
			public int iIdGralComanda { get; set; }
			public int iIdEspComanda { get; set; }
			public int iIdEstatus { get; set; }
			public int iIdCatProducto { get; set; }
			public int iIdEspProductosPrecio { get; set; }
			public string vchDescripcion { get; set; } = string.Empty;
			public string vchComentario { get; set; } = string.Empty;
			public short smCantidad { get; set; }
			public short smDescuento { get; set; }
			public decimal mTotal { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdEspComanda { get; set; }

		}


		public static Request FromConsulta(EspComanda comanda)
		{
			return new Request
			{
				iIdGralComanda = comanda.iIdGralComanda,
				iIdEspComanda = comanda.iIdEspComanda,
				iIdEstatus = comanda.iIdEstatus,
				iIdCatProducto = comanda.iIdCatProducto,
				iIdEspProductosPrecio = comanda.iIdEspProductosPrecio,
				vchDescripcion = comanda.vchDescripcion,
				vchComentario = comanda.vchComentario,
				smCantidad = comanda.smCantidad,
				smDescuento = comanda.smDescuento,
				mTotal = comanda.mTotal
			};
		}

	}
}
