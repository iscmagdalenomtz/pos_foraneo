﻿using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Data
{
	public interface IComandaManager
	{
		Task<DataResult<GuardaGralComanda.Result>> GuardaGralComanda_Async(GuardaGralComanda.Request model);

		Task<DataResult<IList<ConsultaGralComanda.Result>>> ConsultaGralComanda_Async(ConsultaGralComanda.Request model);

		Task<DataResult<GuardaEspComanda.Result>> GuardaEspComanda_Async(GuardaEspComanda.Request model);

		Task<DataResult<IList<ConsultaEspComanda.Result>>> ConsultaEspComanda_Async(ConsultaEspComanda.Request model);

		Task<DataResult<IList<RetornaNombreObjetosBD.Result>>> RetornaNombreObjetosBD_Async(RetornaNombreObjetosBD.Request model);
	}
}
