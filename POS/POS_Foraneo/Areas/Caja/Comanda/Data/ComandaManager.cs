﻿using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Data;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Data
{
    public class ComandaManager : IComandaManager
	{

		private DapperHelper Helper { get; }

		public ComandaManager(IDapperManager iDapperManager)
		{
			Helper = new DapperHelper(iDapperManager);
		}

		public async Task<DataResult<GuardaGralComanda.Result>> GuardaGralComanda_Async(GuardaGralComanda.Request model)
		{
			return await Helper.GetAsync<GuardaGralComanda.Result, GuardaGralComanda.Request>(
				"prcGuardaGralComanda", model
			);
		}

		public async Task<DataResult<IList<ConsultaGralComanda.Result>>> ConsultaGralComanda_Async(ConsultaGralComanda.Request model)
		{
			return await Helper.GetAllAsync<ConsultaGralComanda.Result, ConsultaGralComanda.Request>(
				"prcConsultaGralComanda", model
			);
		}

		public async Task<DataResult<GuardaEspComanda.Result>> GuardaEspComanda_Async(GuardaEspComanda.Request model)
		{
			return await Helper.GetAsync<GuardaEspComanda.Result, GuardaEspComanda.Request>(
				"prcGuardaEspComanda", model
			);
		}

		public async Task<DataResult<IList<ConsultaEspComanda.Result>>> ConsultaEspComanda_Async(ConsultaEspComanda.Request model)
		{
			return await Helper.GetAllAsync<ConsultaEspComanda.Result, ConsultaEspComanda.Request>(
				"prcConsultaEspComanda", model
			);
		}

		public async Task<DataResult<IList<RetornaNombreObjetosBD.Result>>> RetornaNombreObjetosBD_Async(RetornaNombreObjetosBD.Request model)
		{
			return await Helper.GetAllAsync<RetornaNombreObjetosBD.Result, RetornaNombreObjetosBD.Request>(
				"prcRetornaNombreObjetosBD", model
			);
		}
	}
}
