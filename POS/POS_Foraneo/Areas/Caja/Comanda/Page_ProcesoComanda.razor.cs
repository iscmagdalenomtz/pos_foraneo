﻿using POS_Foraneo.Areas.Objetos;

namespace POS_Foraneo.Areas.Caja.Comanda
{
	public partial class Page_ProcesoComanda
	{
		public GralComanda Comanda { get; set; }

		public Proceso FaseProceso { get; set; }

		protected override void OnInitialized()
		{
			FaseProceso = Proceso.MenuComanda;
		}

		public void EC_GralComandaMenu(GralComanda request)
		{
			Comanda = request;
			FaseProceso = Proceso.GenerarPedido;
		}

		public void EC_PagarComanda(GralComanda request)
		{
			Comanda = request;
			FaseProceso = Proceso.PagarComanda;
		}

		public void EC_GralComandaPedido(GralComanda request)
		{
			Comanda = request;
			FaseProceso = Proceso.MenuComanda;
		}
		public void EC_VolverComandas()
		{
			FaseProceso = Proceso.MenuComanda;
		}

		public enum Proceso
		{
			MenuComanda = 0,
			GenerarPedido = 1,
			PagarComanda = 2,
		}
	}
}