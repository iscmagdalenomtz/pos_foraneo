﻿using Microsoft.AspNetCore.Components;

using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System.Collections.Generic;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
	public partial class Ticket
	{


		[Parameter] public GralComanda PGralComanda { get; set; }


		public short smPropiraSugerida { get; set; }
		public decimal smTotalSugerido { get; set; }


		protected override void OnInitialized()
		{
			smPropiraSugerida = 10;


			if (PGralComanda.IsNull())
			{
				InitTest();
			}

			decimal subTotal = ((decimal)smPropiraSugerida / 100) * PGralComanda.mTotalCobrado;
			smTotalSugerido = PGralComanda.mTotalCobrado + subTotal;
		}



		private void OnBlourPropina()
		{
			decimal subTotal = ((decimal)smPropiraSugerida / 100) * PGralComanda.mTotalCobrado;
			smTotalSugerido = PGralComanda.mTotalCobrado + subTotal;
		}

		public void InitTest()
		{
			PGralComanda = new()
			{
				vchNombre = "MESA - 5",
				LstEspComanda = new List<EspComanda>
				{
					new EspComanda
					{
						vchNombreProducto = "Alitas"
					},
					new EspComanda
					{
						vchNombreProducto = "Boneless"
					},
				},
				mTotalCobrado = 100
				
			};
		}
	}
}
