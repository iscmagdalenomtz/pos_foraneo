﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
    public partial class MenuCarta
    {
        [Inject] private IAdministracion IAdministracion { get; set; }
        [Inject] private IJSRuntime JSRuntime { get; set; }
        [Inject] private IDialogService dialogService { get; set; }

        /**/

        [Parameter]
        public EventCallback<CatProductos> EC_CatProductos { get; set; }

        /**/
        public Valida Validar { get; set; }

        public Dictionary<int, IList<ConsultaCategorias.Result>> DiccCategoria { get; set; }

        public List<CatProductos> LstProductos { get; set; }

        protected override void OnInitialized()
        {
            DiccCategoria = new();
            LstProductos = new();
        }

        protected override async Task OnInitializedAsync()
        {
            Validar = new Valida(dialogService);

            var model = new ConsultaCategorias.Request
            {
                iIdCatCategoriaPadre = -1,
                bSoloActivos = true
            };

            var consulta = await Validar.ValidarSP(await IAdministracion.ConsultaCategoria_Async(model));
            if (consulta.Ok)
            {
                DiccCategoria.Add(1, consulta.Data);
            }
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await JSRuntime.InvokeVoidAsync("SetHeight");
        }


        public async Task BuscarSubCategoriaProducto(int iNivel, int iIdCatCategoria)
        {
            BorrarDeDiccionar(iNivel);

            var model = new ConsultaCategorias.Request
            {
                iIdCatCategoriaPadre = iIdCatCategoria,
                bSoloActivos = true
            };

            var consulta = await Validar.ValidarSP( await IAdministracion.ConsultaCategoria_Async(model));
            if (consulta.Ok && consulta.Data.Count > 0)
            {
                DiccCategoria.Add((iNivel + 1), consulta.Data);
            }

            DiccCategoria = DiccCategoria.OrderBy(e => e.Key).ToDictionary(pair => pair.Key, pair => pair.Value);

            LstProductos = new();
            var Consulta = new ConsultaProduto.Request() { iIdCatCategoria = iIdCatCategoria };
            var consultaProd = await IAdministracion.ConsultaProduto_Async(Consulta);
            if (consultaProd.Ok)
            {
                if (consultaProd.Data.FirstOrDefault().bResultado)
                {
                    LstProductos = GeneraLstProducto(consultaProd.Data);
                }
            }
        }

        public void OnClic_Producto(CatProductos catProducto)
        {
            EC_CatProductos.InvokeAsync(catProducto);
        }

        public void BorrarDeDiccionar(int iNevel)
        {
            var lstNiveles = new List<int>();

            foreach (var item in DiccCategoria)
            {
                if (iNevel < item.Key)
                {
                    lstNiveles.Add(item.Key);
                }
            }

            foreach (var item in lstNiveles)
            {
                DiccCategoria.Remove(item);
            }
        }

        public List<CatProductos> GeneraLstProducto(IList<ConsultaProduto.Result> consultaProdutos)
        {
            var lstProductos = new List<CatProductos>();

            foreach (var item in consultaProdutos)
            {

                var producto = CatProductos.ParseFrom(item);
                lstProductos.Add(producto);

            }
            return lstProductos;
        }

    }
}