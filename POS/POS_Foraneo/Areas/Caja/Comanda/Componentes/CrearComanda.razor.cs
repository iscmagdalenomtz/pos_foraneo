﻿
using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.ClasesGenerales;
using POS_Foraneo.Helper.Constantes;
using POS_Foraneo.Helper.SPGeneral;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
	public partial class CrearComanda
	{
		[Inject] private IGenerales Generales { get; set; }
		[Inject] private IComandaManager ComandaManager { get; set; }
		[Inject] private IDialogService dialogService { get; set; }


		/**/
		[Parameter]
		public EventCallback<GralComanda> EC_GralComanda { get; set; }
		/**/

		public GuardaGralComanda.Request ModelComanda { get; set; }

		public MudForm Form { get; set; }
		public Valida Validar { get; set; }

		public IList<OptionModel> LstTipoVenta { get; set; }

		/**/

		public bool bSucces { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(dialogService);

			ModelComanda = new GuardaGralComanda.Request();
			LstTipoVenta = new List<OptionModel>();

			ValoresIniciales();

		}


		protected async override Task OnInitializedAsync()
		{
			var model = new ConsultaConstantes.Request { iIdSubAgrupador = 1 };
			var request = await Validar.ValidarSP(await Generales.ConsultaConstantes_Async(model));
			if (request.Ok)
			{
				LstTipoVenta = ToOptionModel.From(request.Data);
			}
		}


		public async Task OnGuardar()
		{
			await Form.Validate();
			if (bSucces)
			{
				ModelComanda.iIdEstatus = _2_EstatusGralComanda.ORDEN;
				var guarda = await Validar.ValidarSP(await ComandaManager.GuardaGralComanda_Async(ModelComanda));
				if (guarda.Ok)
				{
					ModelComanda.iIdGralComanda = guarda.Data.iIdGralComanda;
					var gralComanda = GralComanda.ParseFrom(ModelComanda);
					gralComanda.dtFechaAlta = DateTime.Now;
					await EC_GralComanda.InvokeAsync(gralComanda); 
				}
			}
		}

		public void ValoresIniciales()
		{
			ModelComanda.iIdTipoVenta = _1_TipoVenta.LOCAL;
			ModelComanda.vchNombre = "MESA - ";
		}

	}
}
