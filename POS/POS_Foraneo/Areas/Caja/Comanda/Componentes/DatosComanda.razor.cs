﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.ClasesGenerales;
using POS_Foraneo.Helper.SPGeneral;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
	public partial class DatosComanda
	{
		[Inject] private IComandaManager ComandaManager { get; set; }
		[Inject] private IDialogService dialogService { get; set; }
		[Inject] private IGenerales Generales { get; set; }

		/**/

		[Parameter]
		public GralComanda PGralComanda { get; set; }

		[Parameter]
		public EventCallback<GralComanda> EC_GralComanda { get; set; }

		/**/

		public MudForm Form { get; set; }
		public Valida Validar { get; set; }

		public IList<OptionModel> LstTipoVenta { get; set; }
		public IList<OptionModel> LstEstatus { get; set; }

		/**/

		public bool bshowDatos { get; set; }
		public bool bSucces { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(dialogService);

			LstTipoVenta = new List<OptionModel>();
			LstEstatus = new List<OptionModel>();
			bshowDatos = false;
		}

		protected override async Task OnInitializedAsync()
		{
			var model = new ConsultaConstantes.Request { iIdSubAgrupador = 1 };
			var request = await Generales.ConsultaConstantes_Async(model);
			if (request.Ok)
			{
				LstTipoVenta = Helper.ToOptionModel.From(request.Data);
			}

			var model2 = new ConsultaConstantes.Request { iIdSubAgrupador = 2 };
			var request2 = await Generales.ConsultaConstantes_Async(model2);
			if (request2.Ok)
			{
				LstEstatus = Helper.ToOptionModel.From(request2.Data);
			}
		}


		public void ShowDatosComanda()
		{
			bshowDatos = !bshowDatos;
		}


		public async Task OnGuardar()
		{
			var descuento = ((decimal)PGralComanda.smDescuento / 100) * PGralComanda.mSubTotal;
			PGralComanda.mTotalCobrado -= descuento;
			await Form.Validate();
			if (bSucces)
			{

				var model = GuardaGralComanda.ParseFrom(PGralComanda);

				var guarda = await Validar.ValidarSP(await ComandaManager.GuardaGralComanda_Async(model));
				if (guarda.Ok)
				{
					await EC_GralComanda.InvokeAsync(PGralComanda);
				}
			}
		}


	}
}