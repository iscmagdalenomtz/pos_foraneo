﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper.Constantes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
	public partial class GenerarPedido
	{
		[Inject] private IComandaManager IComandaManager { get; set; }

		[Inject] private ISnackbar Snackbar { get; set; }
		[Inject] private IJSRuntime JSRuntime { get; set; }
		/**/

		[Parameter]
		public GralComanda PGralComanda { get; set; }

		[Parameter]
		public EventCallback<GralComanda> EC_GralComanda { get; set; }

		/**/
		public IList<ConsultaProduto.Result> LstProductos { get; set; }


		protected override void OnInitialized()
		{
			LstProductos = new List<ConsultaProduto.Result>();

		}

		protected override async Task OnInitializedAsync()
		{
			var tempComnda = await IComandaManager.ConsultaEspComanda_Async(new ConsultaEspComanda.Request { iIdGralComanda = PGralComanda.iIdGralComanda });
			if (tempComnda.Ok)
			{
				PGralComanda.LstEspComanda = GralComanda.ParseFrom(tempComnda.Data);
			}
		}


		public void EC_CatProductos(CatProductos result)
		{
			var modelEspComanda = EspComanda.ParseFrom(result);
			modelEspComanda.smCantidad = 1;

			if (PGralComanda.LstEspComanda.Any(e => e.iIdCatProducto == result.iIdCatProducto && e.iIdEstatus == _3_EstatusEspComanda.ACTIVO))
			{
				var comandaBorrar = PGralComanda.LstEspComanda.Where(e => e.iIdCatProducto == result.iIdCatProducto).SingleOrDefault();
				var index = PGralComanda.LstEspComanda.IndexOf(comandaBorrar);
				PGralComanda.LstEspComanda.Remove(comandaBorrar);
				comandaBorrar.smCantidad = (short)(comandaBorrar.smCantidad + 1);

				modelEspComanda = comandaBorrar;
			}


			modelEspComanda.iIdEstatus = _3_EstatusEspComanda.ACTIVO;
			modelEspComanda.iIdGralComanda = PGralComanda.iIdGralComanda;
			modelEspComanda.mTotal = CaluclarTotal(modelEspComanda);
			PGralComanda.LstEspComanda.Add(modelEspComanda);

		}

		public async Task EC_GralComandaDatos(GralComanda gralComanda)
		{
			PGralComanda = gralComanda;
			if(PGralComanda.iIdEstatus == _2_EstatusGralComanda.CANCELADA)
			{
				await EC_GralComanda.InvokeAsync(PGralComanda);
			}
		}



		public decimal CaluclarTotal(EspComanda comanda)
		{
			var tmptotal = comanda.smCantidad * comanda.mPrecioUnitario;

			var descuento = tmptotal * ((decimal)comanda.smDescuento / 100);

			tmptotal -= descuento;
			return tmptotal;
		}


	}
}
