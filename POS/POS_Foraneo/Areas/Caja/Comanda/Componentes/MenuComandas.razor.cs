﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.Modales;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
	public partial class MenuComandas
	{
		[Inject] private IComandaManager ComandaManager { get; set; }

		[Inject] private IDialogService DialogService { get; set; }

		/**/

		[Parameter] public GralComanda PGralComanda { get; set; }

		[Parameter] public EventCallback<GralComanda> EC_GralComanda { get; set; }
		[Parameter] public EventCallback<GralComanda> EC_PagarComanda { get; set; }

		/**/

		public IList<ConsultaGralComanda.Result> LstComandas { get; set; }
		public Valida Validar { get; set; }

		/**/

		public bool bShowCrearComanda { get; set; }
		public string strTextoCrearComanda { get; set; }
		public string strSelectedIcon { get; set; }
		public Color iColorBtn { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(DialogService);

			LstComandas = new List<ConsultaGralComanda.Result>();
			bShowCrearComanda = false;
			strSelectedIcon = Icons.Material.Filled.Add;
			iColorBtn = Color.Primary;
			strTextoCrearComanda = "Crear Nueva Comanda";

			if (PGralComanda.IsNull())
			{
				PGralComanda = new();
			}
		}

		protected async override Task OnInitializedAsync()
		{
			var model = new ConsultaGralComanda.Request { bActivas = true };
			var respuesta = await Validar.ValidarSP(await ComandaManager.ConsultaGralComanda_Async(model));
			if (respuesta.Ok)
			{
				LstComandas = respuesta.Data;
			}
		}

		public void OnClicAdd()
		{
			bShowCrearComanda = bShowCrearComanda.False();

			if (bShowCrearComanda)
			{
				strSelectedIcon = Icons.Material.Filled.ArrowBack;
				iColorBtn = Color.Secondary;
				strTextoCrearComanda = "Cancelar creación";
			}
			else
			{
				strSelectedIcon = Icons.Material.Filled.Add;
				iColorBtn = Color.Primary;
				strTextoCrearComanda = "Crear Nueva Comanda";

			}
		}

		public async Task OnClickComanda(ConsultaGralComanda.Result result)
		{
			PGralComanda = GralComanda.ParseFrom(result);
			await EC_ComandaAsync(PGralComanda);
		}

		public async Task EC_ComandaAsync(GralComanda request)
		{
			PGralComanda = request;
			await EC_GralComanda.InvokeAsync(PGralComanda);
		}


		public async Task PreviewTicket(ConsultaGralComanda.Result comada)
		{
			PGralComanda = GralComanda.ParseFrom(comada);
			var tmpConsulta = await Validar.ValidarSP(await ComandaManager.ConsultaEspComanda_Async(
				new ConsultaEspComanda.Request { iIdGralComanda = PGralComanda.iIdGralComanda }));
			if (tmpConsulta.Ok)
			{
				PGralComanda.LstEspComanda = new List<EspComanda>();
				foreach (var esp in tmpConsulta.Data)
				{
					PGralComanda.LstEspComanda.Add(EspComanda.ParseFrom(esp));
				}
			}

			var parameters = new DialogParameters { ["PGralComanda"] = PGralComanda };

			var dialog = DialogService.Show<ModalTicket>("Imprimir Ticket ", parameters);
			var result = await dialog.Result;

		}


		public async Task OnClickPagarComanda(ConsultaGralComanda.Result result)
		{
			PGralComanda = GralComanda.ParseFrom(result);
			await EC_PagarComanda.InvokeAsync(PGralComanda);
		}
	}
}
