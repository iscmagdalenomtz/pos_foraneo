﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

namespace POS_Foraneo.Areas.Caja.Comanda.Componentes
{
    public partial class Pedido
    {
        [Inject] private IComandaManager IComandaManager { get; set; }
        [Inject] private ISnackbar Snackbar { get; set; }
        [Inject] private IAdministracion Administracion { get; set; }
        [Inject] private IDialogService dialogService { get; set; }

        /**/

        [Parameter] public GralComanda PGralComanda { get; set; }

        [Parameter] public EventCallback<GralComanda> EC_GralComanda { get; set; }

        /**/

        public IList<ConsultaPrecioProducto.Result> LstPrecio { get; set; }
        public EspComanda tmpCambiarPrecio { get; set; }
        public Valida Validar { get; set; }

        public decimal mTotal { get; set; }

        public int iTotalITems { get; set; }

        protected override void OnInitialized()
        {
            Validar = new Valida(dialogService);

            mTotal = 0;
            iTotalITems = 0;
            tmpCambiarPrecio = new();
        }


        protected override void OnParametersSet()
        {
            mTotal = 0;
            iTotalITems = 0;

            foreach (var item in PGralComanda.LstEspComanda)
            {
                mTotal += item.mTotal;
                iTotalITems += item.smCantidad;
            }

            PGralComanda.mSubTotal = mTotal;
            PGralComanda.mTotalCobrado = mTotal;
            if (PGralComanda.smDescuento > 0)
            {
                var descuento = ((decimal)PGralComanda.smDescuento / 100) * mTotal;
                PGralComanda.mTotalCobrado -= descuento;
            }
        }

        public async Task ConsultarPrecios(EspComanda espComanda)
        {
            tmpCambiarPrecio = espComanda;
            LstPrecio = new List<ConsultaPrecioProducto.Result>();
            var model = new ConsultaPrecioProducto.Request
            {
                iIdCatProducto = espComanda.iIdCatProducto,
                bSoloActivos = true
            };
            var resp = await Validar.ValidarSP(await Administracion.ConsultaPrecioProducto_Async(model));
            if (resp.Ok)
            {
                LstPrecio = resp.Data;
            }

        }

        public void CambiarPrecio(ConsultaPrecioProducto.Result result)
        {
            PGralComanda.LstEspComanda.Where(e => e.iIdEspComanda == tmpCambiarPrecio.iIdEspComanda).FirstOrDefault().mPrecioUnitario = result.mPrecio;
            PGralComanda.LstEspComanda.Where(e => e.iIdEspComanda == tmpCambiarPrecio.iIdEspComanda).FirstOrDefault().iIdEspProductosPrecio = result.iIdEspProductosPrecio;
        }


        public async Task GuardarComanda()
        {
            var tempLista = PGralComanda.LstEspComanda;
            foreach (var item in tempLista)
            {
                var model = GuardaEspComanda.FromConsulta(item);
                var guardar = await Validar.ValidarSP(await IComandaManager.GuardaEspComanda_Async(model));
                if (guardar.Ok.False())
                {
                    return;
                }
            }

            var modelComada = GuardaGralComanda.ParseFrom(PGralComanda);
            var guarda = await Validar.ValidarSP(await IComandaManager.GuardaGralComanda_Async(modelComada), "Pedido Guardado", true);
            if (guarda.Ok)
            {
                await EC_GralComanda.InvokeAsync(PGralComanda);
            }


        }

    }
}
