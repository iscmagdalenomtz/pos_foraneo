﻿using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Data
{
	public interface ICajaRegistradora
	{
		Task<DataResult<GuardaEspCorteCaja.Result>> GuardaEspCorteCaja_Async(GuardaEspCorteCaja.Request model);
		Task<DataResult<IList<ConsultaGralCorteCaja.Result>>> ConsultaGralCorteCaja_Async(ConsultaGralCorteCaja.Request model);
		Task<DataResult<IList<ConsultaEspCorteCaja.Result>>> ConsultaEspCorteCaja_Async(ConsultaEspCorteCaja.Request model);
		Task<DataResult<ConsultaGralCorteCaja.Result>> ConsultaActualCorteCaja_Async();
		Task<DataResult<prcCerrarCorteCaja.Result>> prcCerrarCorteCaja_Async(prcCerrarCorteCaja.Request model);

	}
}
