﻿using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Data;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Data
{
	public class CajaRegistradora : ICajaRegistradora
	{


		private DapperHelper Helper { get; }

		public CajaRegistradora(IDapperManager iDapperManager)
		{
			Helper = new DapperHelper(iDapperManager);
		}


		public async Task<DataResult<GuardaEspCorteCaja.Result>> GuardaEspCorteCaja_Async(GuardaEspCorteCaja.Request model)
		{
			return await Helper.GetAsync<GuardaEspCorteCaja.Result, GuardaEspCorteCaja.Request>(
				"prcGuardaEspCorteCaja", model
			);
		}

		public async Task<DataResult<IList<ConsultaGralCorteCaja.Result>>> ConsultaGralCorteCaja_Async(ConsultaGralCorteCaja.Request model)
		{
			return await Helper.GetAllAsync<ConsultaGralCorteCaja.Result, ConsultaGralCorteCaja.Request>(
			"prcConsultaGralCorteCaja", model
		);
		}
		public async Task<DataResult<IList<ConsultaEspCorteCaja.Result>>> ConsultaEspCorteCaja_Async(ConsultaEspCorteCaja.Request model)
		{
			return await Helper.GetAllAsync<ConsultaEspCorteCaja.Result, ConsultaEspCorteCaja.Request>(
			"prcConsultaEspCorteCaja", model
		);
		}

		public async Task<DataResult<ConsultaGralCorteCaja.Result>> ConsultaActualCorteCaja_Async()
		{
			var model = new object();
			return await Helper.GetAsync<ConsultaGralCorteCaja.Result, object>(
			"prcConsultaCorteActual", model);
		}

		public async Task<DataResult<prcCerrarCorteCaja.Result>> prcCerrarCorteCaja_Async(prcCerrarCorteCaja.Request model)
		{
			return await Helper.GetAsync<prcCerrarCorteCaja.Result, prcCerrarCorteCaja.Request>(
				"prcCerrarCorteCaja", model
			);
		}
	}
}
