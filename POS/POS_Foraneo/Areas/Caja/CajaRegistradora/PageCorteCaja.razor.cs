﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.Constantes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora
{
	public partial class PageCorteCaja
	{

		[Inject] private ICajaRegistradora CajaRegistradora { get; set; }
		[Inject] private IDialogService dialogService { get; set; }

		/**/

		public GralCorteCaja ActualCoteCaja { get; set; }
		public Valida Validar { get; set; }


		protected override void OnInitialized()
		{
			Validar = new Valida(dialogService);

			ActualCoteCaja = new();
		}


		protected override async Task OnInitializedAsync()
		{
			var tmpGralCorteCaja = await Validar.ValidarSP(await CajaRegistradora.ConsultaActualCorteCaja_Async());
			if (tmpGralCorteCaja.Ok)
			{
				ActualCoteCaja = GralCorteCaja.ParseFrom(tmpGralCorteCaja.Data);

				var tmpEspCorteCaja = await Validar.ValidarSP(await CajaRegistradora.ConsultaEspCorteCaja_Async(new ConsultaEspCorteCaja.Request { iIdGralCorteCaja = ActualCoteCaja.iIdGralCorteCaja }));
				if (tmpEspCorteCaja.Ok)
				{
					ActualCoteCaja.LstEspCorteCaja = EspCorteCaja.ParseFrom(tmpEspCorteCaja.Data);
				}
			}
		}


		public void EC_GralCorteCaja(GralCorteCaja corteCaja)
		{
			ActualCoteCaja = corteCaja;
		}

		public void EC_EspCorteCaja(EspCorteCaja corteCaja)
		{
			ActualCoteCaja.mTotalSistema += corteCaja.mTotalRecaudado;

			ActualCoteCaja.LstEspCorteCaja.Add(corteCaja);
		}
	}
}
