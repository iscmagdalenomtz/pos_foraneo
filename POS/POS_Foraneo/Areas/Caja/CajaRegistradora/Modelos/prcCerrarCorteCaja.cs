﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos
{
	public static class prcCerrarCorteCaja
	{

		public class Request
		{
			public int iIdGralCorteCaja { get; set; }
			public string vchDescripcion { get; set; }
			public decimal mTotal { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; }
		}

	}
}
