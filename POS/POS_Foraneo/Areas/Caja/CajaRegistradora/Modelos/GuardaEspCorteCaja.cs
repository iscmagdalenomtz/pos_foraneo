﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos
{
	public static class GuardaEspCorteCaja
	{
		public class Request
		{
			public int iIdGralCorteCaja { get; set; }
			public int iIdGralComanda { get; set; }
			public int iIdTipoPago { get; set; }
			public decimal mTotalRecaudado { get; set; }
			public string vchComentario { get; set; } = string.Empty;
			public int  iIdTipoMovimiento { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdEspCorteCaja { get; set; }

		}
	}
}
