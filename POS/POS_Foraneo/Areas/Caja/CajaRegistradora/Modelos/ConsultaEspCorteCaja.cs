﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos
{
	public static class ConsultaEspCorteCaja
	{

		public class Request
		{
			public int iIdGralCorteCaja { get; set; }
		}

		public class Result
		{
			public int bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdEspCorteCaja { get; set; }
			public int iIdGralCorteCaja { get; set; }
			public int iIdGralComanda { get; set; }
			public int iIdTipoPago { get; set; }
			public string vchTipoPago { get; set; }
			public int iIdTipoMovimiento { get; set; }
			public string vchTipoMovimiento { get; set; }
			public decimal mTotalRecaudado { get; set; }
			public DateTime dtFechaUltModificacion { get; set; } = Default.Date;
		}
	}
}
