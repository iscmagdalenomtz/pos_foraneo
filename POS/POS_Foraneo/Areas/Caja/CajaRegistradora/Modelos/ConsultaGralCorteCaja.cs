﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos
{
	public static class ConsultaGralCorteCaja
	{

		public class Request
		{
			public bool bActiva { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdGralCorteCaja { get; set; }
			public DateTime dtFechaIncicio { get; set; } = Default.Date;
			public DateTime dtFechaFin { get; set; } = Default.Date;
			public string vchDescripcion { get; set; } = string.Empty;
			public decimal mTotal { get; set; }
			public decimal mTotalSistema { get; set; }
			public DateTime dtFechaUltModificacion { get; set; } = Default.Date;
			public bool bActivo { get; set; }
		}
	}
}

