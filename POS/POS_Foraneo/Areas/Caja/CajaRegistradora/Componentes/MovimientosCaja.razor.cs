﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.Constantes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Componentes
{
	public partial class MovimientosCaja
	{
		[Inject] private IDialogService dialogService { get; set; }
		[Inject] private ICajaRegistradora CajaRegistradora { get; set; }


		[Parameter] public int PiIdCorteCaja { get; set; }
		[Parameter] public int PiIdTipoMovimiento { get; set; }

		[Parameter] public EventCallback<EspCorteCaja> EC_EspCorteCaja { get; set; }

		public GuardaEspCorteCaja.Request ModelEspComanda { get; set; }


		public MudForm Form { get; set; }
		public Valida Validar { get; set; }

		public bool bSucces { get; set; }


		public string strTitulo { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(dialogService);

			ModelEspComanda = new GuardaEspCorteCaja.Request();

			switch (PiIdTipoMovimiento)
			{
				case _5_TipoMovimiento.DEPOSITO:
					strTitulo = "Depositar Dinero";
					break;

				case _5_TipoMovimiento.RETIRO:
					strTitulo = "Retirar Dinero";
					break;
				default:
					strTitulo = "Sin Movimiento";
					break;
			}

		}
		public async Task OnGuardar()
		{
			ModelEspComanda.iIdGralComanda = -1;
			ModelEspComanda.iIdTipoPago = _4_FormaPago.EFECTIVO;
			ModelEspComanda.iIdGralCorteCaja = PiIdCorteCaja;
			ModelEspComanda.iIdTipoMovimiento = PiIdTipoMovimiento;

			await Form.Validate();
			if (bSucces)
			{
				if(ModelEspComanda.iIdTipoMovimiento == _5_TipoMovimiento.RETIRO)
				{
					ModelEspComanda.mTotalRecaudado = (ModelEspComanda.mTotalRecaudado * (-1));
				}

				var resp = await Validar.ValidarSP(await CajaRegistradora.GuardaEspCorteCaja_Async(ModelEspComanda), "Datos Guardados", true);
				if (resp.Ok)
				{
					await EC_EspCorteCaja.InvokeAsync(EspCorteCaja.ParseFrom(ModelEspComanda));
					ModelEspComanda = new GuardaEspCorteCaja.Request();
				}
			}
		}
	}
}
