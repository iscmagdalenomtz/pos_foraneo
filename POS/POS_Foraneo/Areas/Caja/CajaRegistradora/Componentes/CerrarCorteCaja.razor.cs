﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Componentes
{
	public partial class CerrarCorteCaja
	{
		[Inject] private IDialogService dialogService { get; set; }
		[Inject] private ICajaRegistradora CajaRegistradora { get; set; }

		[Parameter] public GralCorteCaja PGralCorteCaja { get; set; }
		[Parameter] public EventCallback<GralCorteCaja> EC_GralCorteCaja { get; set; }

		public MudForm Form { get; set; }
		public Valida Validar { get; set; }
		public bool bSucces { get; set; }

		public prcCerrarCorteCaja.Request ModelCorteCaja { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(dialogService);

			ModelCorteCaja = new();
		}


		public async Task OnGuardar()
		{
			ModelCorteCaja.iIdGralCorteCaja = PGralCorteCaja.iIdGralCorteCaja;
			await Form.Validate();
			if (bSucces)
			{
				var resp = await Validar.ValidarSP(await CajaRegistradora.prcCerrarCorteCaja_Async(ModelCorteCaja), "Datos Guardados", true);
				if (resp.Ok)
				{
					PGralCorteCaja.mTotal = ModelCorteCaja.mTotal;
					PGralCorteCaja.vchDescripcion = ModelCorteCaja.vchDescripcion;
					PGralCorteCaja.dtFechaFin = DateTime.Now;
					PGralCorteCaja.bActivo = false;

					await EC_GralCorteCaja.InvokeAsync(PGralCorteCaja);
				}
			}
		}

	}
}
