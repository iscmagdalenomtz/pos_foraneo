﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;
using POS_Foraneo.Helper.ClasesGenerales;
using POS_Foraneo.Helper.Constantes;
using POS_Foraneo.Helper.SPGeneral;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Caja.CajaRegistradora.Componentes
{
    public partial class PagarComanda
    {
        [Inject] private IComandaManager ComandaManager { get; set; }
        [Inject] private ICajaRegistradora CajaRegistradora { get; set; }
        [Inject] private ISnackbar Snackbar { get; set; }
        [Inject] private IGenerales Generales { get; set; }
        [Inject] private IDialogService dialogService { get; set; }

        /**/
        [Parameter] public GralComanda PGralComanda { get; set; }
        [Parameter] public EventCallback EC_VolverComandas { get; set; }

        /**/
        public IList<OptionModel> LstTipoPago { get; set; }

        public EspCorteCaja Model { get; set; }

        public MudForm Form { get; set; }
        public Valida Validar { get; set; }

        public GralCorteCaja CorteActual { get; set; }

        /**/
        public bool bSucces { get; set; }
        public decimal mPagoCliente { get; set; }
        public decimal mRestoCliente { get; set; }

        protected override void OnInitialized()
        {
            Validar = new Valida(dialogService);

            CorteActual = new();
            LstTipoPago = new List<OptionModel>();
            if (PGralComanda.IsNull())
            {
                InitTest();
            }
            Model = new();

            Model.mTotalRecaudado = PGralComanda.mTotalCobrado;
        }

        protected override async Task OnInitializedAsync()
        {
            var model = new ConsultaConstantes.Request { iIdSubAgrupador = 4 };
            var request = await Validar.ValidarSP(await Generales.ConsultaConstantes_Async(model));
            if (request.Ok)
            {
                LstTipoPago = ToOptionModel.From(request.Data);
                Model.iIdTipoPago = _4_FormaPago.EFECTIVO;
            }

            var tempComnda = await Validar.ValidarSP(await ComandaManager.ConsultaEspComanda_Async(new ConsultaEspComanda.Request { iIdGralComanda = PGralComanda.iIdGralComanda }));
            if (tempComnda.Ok)
            {
                PGralComanda.LstEspComanda = GralComanda.ParseFrom(tempComnda.Data);
            }

            var tempCorteActual = await Validar.ValidarSP(await CajaRegistradora.ConsultaActualCorteCaja_Async());
            if (tempCorteActual.Ok)
            {
                CorteActual = GralCorteCaja.ParseFrom( tempCorteActual.Data);
            }

        }


        public void OnKeyUpPago()
        {
            mRestoCliente = mPagoCliente - PGralComanda.mTotalCobrado;
        }

        public async Task OnGuardar()
        {
            if(mRestoCliente < 0 || mPagoCliente == 0)
            {
                Snackbar.Add($"Error en el pago del cliente ", Severity.Warning);

                return;
            }

            var guarda = new GuardaEspCorteCaja.Request
            {
                iIdGralComanda = PGralComanda.iIdGralComanda,
                iIdGralCorteCaja = CorteActual.iIdGralCorteCaja,
                mTotalRecaudado = Model.mTotalRecaudado,
                iIdTipoPago = Model.iIdTipoPago,
                iIdTipoMovimiento = _5_TipoMovimiento.COMANDA,
                vchComentario = string.Empty
            };
            var resp = await Validar.ValidarSP(await CajaRegistradora.GuardaEspCorteCaja_Async(guarda),"Pedido Pagado", true);
            if (resp.Ok)
            {
                await EC_VolverComandas.InvokeAsync();
            }
        }

        public void InitTest()
        {

            PGralComanda = new()
            {
                vchNombre = "MESA - 5",
                LstEspComanda = new List<EspComanda>
                {
                    new EspComanda
                    {
                        vchNombreProducto = "Alitas"
                    },
                    new EspComanda
                    {
                        vchNombreProducto = "Boneless"
                    },
                },
                mTotalCobrado = 100

            };



        }

    }
}
