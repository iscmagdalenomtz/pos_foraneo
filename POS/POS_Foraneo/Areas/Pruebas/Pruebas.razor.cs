﻿using Microsoft.AspNetCore.Components;

using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Pruebas
{
	public partial class Pruebas
	{
		[Inject]
		private IComandaManager ComandaManager { get; set; }

		public IList<RetornaNombreObjetosBD.Result> LstSP { get; set; }

		public Dictionary<string, IList<RetornaNombreObjetosBD.Result>> DiccObjetos { get; set; }
		public Dictionary<string, IList<RetornaNombreObjetosBD.Result>> DiccSP { get; set; }

		protected override void OnInitialized()
		{
			DiccObjetos = new();
			DiccSP = new(); 
		}
		protected async override Task OnInitializedAsync()
		{
			/*
			var str = "prcConsultaGralComanda";
			string[] split = Regex.Split(str, @"(?<!^)(?=[A-Z])");
			*/
			var sp = await ComandaManager.RetornaNombreObjetosBD_Async(
				new RetornaNombreObjetosBD.Request() { type = "" });

			if (sp.Ok)
			{
				LstSP = sp.Data;
				SeparaDicc();
			}
		}

		public void SeparaDicc()
		{
			var GroupByMS = LstSP.GroupBy(s => s.DescType);

			foreach (var item in GroupByMS.ToList())
			{
				DiccObjetos.Add(item.Key, LstSP.Where(e => e.DescType == item.Key).ToList());
			}



		}


	}
}
