﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Reportes.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Reportes.Componetes
{
	public partial class Graficas
	{
		[Inject] private IReportesManager ReportesManager  { get; set; }


		private ChartOptions options = new ChartOptions();
		public List<ChartSeries> Series = new List<ChartSeries>()
{
		new ChartSeries() { Name = "Series 1", Data = new double[] { 90, 79, 72, 69, 62, 62, 55, 65, 70 } },
		new ChartSeries() { Name = "Series 2", Data = new double[] { 35, 41, 35, 51, 49, 62, 69, 91, 148 } },
	};
		public string[] XAxisLabels = { "Lunes", "MARTES", "MIERCO", "JUEVES", "VIERNES", "SABADO", "DOMINGO"};

		Random random = new Random();
		protected override void OnInitialized()
		{
			options.InterpolationOption = InterpolationOption.NaturalSpline;
			options.YAxisFormat = "c2";
		}

		protected override async Task OnInitializedAsync()
		{
			//var consulta = await ReportesManager.ConsultaEspCorteCaja_Async(new ConsultaEspCorteCaja.Request { iIdGralCorteCaja = 1 });
			//var lista = new ChartSeries() { Name = "Series 2", Data = new double[9]};
			//int i = 0;
			//foreach (var item in consulta.Data)
			//{
			//	lista.Data[i] = (double)(item.mTotalRecaudado);
			//	i++;
			//}

		}

		public void RandomizeData()
		{
			var new_series = new List<ChartSeries>()
			{
			new ChartSeries() { Name = "Series 1", Data = new double[9] },
			new ChartSeries() { Name = "Series 2", Data = new double[9] },
			};
			for (int i = 0; i < 9; i++)
			{
				new_series[0].Data[i] = random.NextDouble() * 100;
				new_series[1].Data[i] = random.NextDouble() * 100;
			}
			Series = new_series;
			StateHasChanged();
		}

		void OnClickMenu(InterpolationOption interpolationOption)
		{
			options.InterpolationOption = interpolationOption;
			StateHasChanged();
		}

	}
}
