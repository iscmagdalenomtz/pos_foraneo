﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.CajaRegistradora.Data;
using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Reportes.Componetes
{
	public partial class RP_CortesCaja
	{

		[Inject] private ICajaRegistradora CajaRegistradora { get; set; }
		[Inject] private IDialogService DialogService { get; set; }

		/**/
		public Valida Validar { get; set; }

		/**/
		public IList<GralCorteCaja> LstCorteCaja { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(DialogService);
			LstCorteCaja = new List<GralCorteCaja>();
		}

		protected override async Task OnInitializedAsync()
		{
			var model = new ConsultaGralCorteCaja.Request { bActiva = false };
			var respuesta = await Validar.ValidarSP(await CajaRegistradora.ConsultaGralCorteCaja_Async(model));
			if (respuesta.Ok)
			{
				LstCorteCaja = GralCorteCaja.ParseFrom(respuesta.Data);
			}
		}


		public async Task OnClickRow(int iIdGralCorteCaja)
		{
			var bShowTable = LstCorteCaja.Where(e => e.iIdGralCorteCaja == iIdGralCorteCaja).SingleOrDefault().bShowDetalles;

			var model = new ConsultaEspCorteCaja.Request { iIdGralCorteCaja = iIdGralCorteCaja };
			var tmpConsulta = await Validar.ValidarSP(await CajaRegistradora.ConsultaEspCorteCaja_Async(model));
			if (tmpConsulta.Ok)
			{
				LstCorteCaja.Where(e => e.iIdGralCorteCaja == iIdGralCorteCaja).SingleOrDefault().bShowDetalles = bShowTable.False();
				LstCorteCaja.Where(e => e.iIdGralCorteCaja == iIdGralCorteCaja).SingleOrDefault().LstEspCorteCaja = EspCorteCaja.ParseFrom(tmpConsulta.Data);
			}
		}
	}
}
