﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Caja.Comanda.Data;
using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Reportes.Componetes
{
	public partial class RP_Comandas
	{
		[Inject] private IComandaManager ComandaManager { get; set; }

		[Inject] private IDialogService DialogService { get; set; }


		/**/

		public Valida Validar { get; set; }

		/**/

		public IList<GralComanda> LstComandas { get; set; }

		protected override void OnInitialized()
		{
			Validar = new Valida(DialogService);
			LstComandas = new List<GralComanda>();
		}

		protected override async Task OnInitializedAsync()
		{
			var model = new ConsultaGralComanda.Request { bActivas = false };
			var respuesta = await Validar.ValidarSP(await ComandaManager.ConsultaGralComanda_Async(model));
			if (respuesta.Ok)
			{
				LstComandas = GralComanda.ParseFrom(respuesta.Data);
			}
		}



		public async Task OnClickRow(int iIdGralComanda)
		{
			var bShowTable = LstComandas.Where(e => e.iIdGralComanda == iIdGralComanda).SingleOrDefault().bShowDetalles;

			var model = new ConsultaEspComanda.Request { iIdGralComanda = iIdGralComanda };
			var tmpConsulta = await Validar.ValidarSP(await ComandaManager.ConsultaEspComanda_Async(model));
			if (tmpConsulta.Ok)
			{
				LstComandas.Where(e => e.iIdGralComanda == iIdGralComanda).SingleOrDefault().bShowDetalles = bShowTable.False();
				LstComandas.Where(e => e.iIdGralComanda == iIdGralComanda).SingleOrDefault().LstEspComanda = GralComanda.ParseFrom(tmpConsulta.Data);
			}
		}
	}
}
