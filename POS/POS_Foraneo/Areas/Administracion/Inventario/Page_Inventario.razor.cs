﻿using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

namespace POS_Foraneo.Areas.Administracion.Inventario
{
	public partial class Page_Inventario
	{

		public CatProductos PCatProductos { get; set; }
		public CatCategoria PCatCategoria { get; set; }

		public bool bFromProducto { get; set; }
		public bool bFromCategoria { get; set; }
		public bool bTablaProductos { get; set; }
		public bool bTablaCategorias { get; set; }

		protected override void OnInitialized()
		{
			PCatProductos = new();
			PCatCategoria = new();
		}



		/*Productos*/
		public void OnclicProducto()
		{
			bFromProducto = bFromProducto.False();
			PCatProductos = new();
		}
		public void OnclicTablaProducto()
		{
			bTablaProductos = bTablaProductos.False();
		}

		public void OnSelectProducto(CatProductos catProductos)
		{
			PCatProductos = catProductos;
			bFromProducto = true;
		}



		/*Categoria*/
		public void OnclicTablaCategorias ()
		{
			bTablaCategorias = bTablaCategorias.False();
		}
		public void OnclicCategoria()
		{
			bFromCategoria = bFromCategoria.False();
			PCatCategoria = new ();
		}

		public void EC_CatCategoriaM(CatCategoria catCategoria)
		{
			PCatCategoria = catCategoria;
			bFromCategoria = false;
			bTablaCategorias = true;
		}

		public void OnSelectCategoria(CatCategoria catCategoria)
		{
			PCatCategoria = catCategoria;
			bFromCategoria = true;
		}
	}
}