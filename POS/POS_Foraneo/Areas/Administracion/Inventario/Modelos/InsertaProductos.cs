﻿using POS_Foraneo.Areas.Objetos;

using System.ComponentModel.DataAnnotations;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class InsertaProductos
	{
		public class Request
		{
			public int iIdCatProducto { get; set; }
			[Required(ErrorMessage = "El campo nombre es requerido.")]
			public string vchNombre { get; set; }
			public string vchDescripcion { get; set; }
			public int iIdCatCategoria { get; set; }
			public bool bActivo { get; set; }
		}


		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; }
			public int iIdCatProducto { get; set; }
		}


		public static Request RequestFrom(CatProductos catProductos)
		{
			return new Request
			{
				iIdCatProducto = catProductos.iIdCatProducto,
				iIdCatCategoria = catProductos.iIdCatCategoria,
				vchNombre = catProductos.vchNombre,
				vchDescripcion = catProductos.vchDescripcion,
				bActivo = catProductos.bActivo,
			};
		}
	}
}
