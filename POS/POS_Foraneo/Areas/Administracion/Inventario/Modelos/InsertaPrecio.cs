﻿using POS_Foraneo.Areas.Objetos;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class InsertaPrecio
	{
		public class Request
		{
			public int iIdEspProductosPrecio { get; set; }
			public int iIdCatProducto { get; set; }
			[Required]
			public decimal mPrecio { get; set; }
			[Required]
			public string vchDescripcion { get; set; } = string.Empty;
			public bool bActivo  { get; set; }
			public bool bDefault { get; set; }
		}

		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
		}

		public static Request RequestFrom(EspProductosPrecios espPrecio)
		{
			return new Request
			{
				iIdEspProductosPrecio = espPrecio.iIdEspProductosPrecio,
				iIdCatProducto = espPrecio.iIdCatProducto,
				mPrecio = espPrecio.mPrecio,
				vchDescripcion = espPrecio.vchDescripcion,
				bActivo = espPrecio.bActivo,
				bDefault = espPrecio.bDefault
			};
		}

	}
}
