﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class ConsultaProduto
	{

		public class Request
		{
			public int iIdCatProducto { get; set; }
			public int iIdCatCategoria { get; set; }
			public string vchNombre { get; set; } = string.Empty;
			public bool bSoloActivos { get; set; }
			public bool bTodos { get; set; }

		}


		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdCatProducto { get; set; }
			public string vchNombre { get; set; } = string.Empty;
			public string vchDescripcion { get; set; } = string.Empty;
			public bool bActivo { get; set; }
			public int iIdCatCategoria { get; set; }
			public string vchNombreCategoria { get; set; } = string.Empty;
			public int iIdEspProductosPrecioDefault { get; set; }
            public decimal mPrecioDefault { get; set; }


        }

	}
}
