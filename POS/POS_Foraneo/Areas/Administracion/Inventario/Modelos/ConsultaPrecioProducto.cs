﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class ConsultaPrecioProducto
	{
		public class Request
		{
			public int iIdCatProducto { get; set; }
			public bool bSoloActivos { get; set; }

		}


		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdEspProductosPrecio { get; set; }
			public int iIdCatProducto { get; set; }
			public decimal mPrecio { get; set; }
			public string vchDescripcion { get; set; } = string.Empty;
			public bool bDefault { get; set; }
			public bool bActivo { get; set; }

		}
	}
}
