﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class ConsultaCategorias
	{
		public class Request
		{
			public int iIdCatCategoria { get; set; }
			public int iIdCatCategoriaPadre { get; set; } = -2;
			public string vchNombre { get; set; } = string.Empty;
			public bool bSoloActivos { get; set; }
			public bool bTodos { get; set; }

		}


		public class Result
		{
			public int bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdCatCategoria { get; set; }
			public string vchNombre { get; set; } = string.Empty;
			public string vchDescripcion { get; set; } = string.Empty;
			public int iIdCatCategoriaPadre { get; set; }
			public string vchNombrePadre { get; set; } = string.Empty;
			public bool bActivo { get; set; }
			public DateTime dtFechaUltModificacion { get; set; } = new DateTime(1900, 1, 1);

		}


	}
}
