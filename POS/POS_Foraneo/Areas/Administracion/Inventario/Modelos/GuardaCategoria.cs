﻿using POS_Foraneo.Areas.Objetos;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Modelos
{
	public static class GuardaCategoria
	{
		public class Request
		{
			public int iIdCatCategoria { get; set; }
			[Required(ErrorMessage = "El campo nombre es requerido.")]
			public string vchNombre { get; set; } = string.Empty;
			public string vchDescripcion { get; set; } = string.Empty;
			public int iIdCatCategoriaPadre { get; set; }
			public bool bActivo { get; set; }
		}


		public class Result
		{
			public bool bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
		}


		public static Request RequestFrom(CatCategoria catCategoria)
		{
			return new Request
			{
				iIdCatCategoria = catCategoria.iIdCatCategoria,
				vchNombre = catCategoria.vchNombre,
				vchDescripcion = catCategoria.vchDescripcion,
				iIdCatCategoriaPadre = catCategoria.iIdCatCategoriaPadre,
				bActivo = catCategoria.bActivo,
			};
		}
	}
}
