﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Helper.ClasesGenerales;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using POS_Foraneo.Areas.Objetos;

namespace POS_Foraneo.Areas.Administracion.Inventario.Componentes
{
    public partial class GuardaProducto
    {

        [Inject] private IAdministracion IAdministracion { get; set; }
        [Inject] private ISnackbar Snackbar { get; set; }
        [Inject] private IDialogService dialogService { get; set; }

        /**/

        public MudForm Form { get; set; }

        [Parameter] public CatProductos CatProducto { get; set; }

        public IList<ConsultaCategorias.Result> LstCategoria { get; set; }

        public OptionModel OpCategoria { get; set; }
        public Valida Validar { get; set; }


        /**/

        public bool bSucces { get; set; }

        protected override void OnInitialized()
        {
            Validar = new Valida(dialogService);

            OpCategoria = new OptionModel();

            LstCategoria = new List<ConsultaCategorias.Result>();

            if (CatProducto.IsNull())
            {
                CatProducto = new();
            }
        }

        protected override async Task OnInitializedAsync()
        {

            var model = new ConsultaCategorias.Request
            {
                bTodos = true
            };

            var consulta = await IAdministracion.ConsultaCategoria_Async(model);
            if (consulta.Ok)
            {
                LstCategoria = consulta.Data;
            }
        }

        public async Task OnGuardar()
        {
            await Form.Validate();

            if (bSucces && CatProducto.LstEspProductosPrecios.Count() > 0)
            {
                var gurdarProdcto = await Validar.ValidarSP(await IAdministracion.InsertaProducto_Async(InsertaProductos.RequestFrom(CatProducto)), "Producto Guardado", true);
                if (gurdarProdcto.Ok)
                {
                    CatProducto.iIdCatProducto = gurdarProdcto.Data.iIdCatProducto;

                    foreach (var precio in CatProducto.LstEspProductosPrecios)
                    {
                        precio.iIdCatProducto = gurdarProdcto.Data.iIdCatProducto;

                        await Validar.ValidarSP(await IAdministracion.InsertaPrecio_Async(InsertaPrecio.RequestFrom(precio)));

                    }
                }
            }
        }
    }
}
