﻿using Microsoft.AspNetCore.Components;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Componentes
{
    public partial class ConsultaProducto
    {

        [Inject] private IAdministracion IAdministracion { get; set; }

        /**/
        [Parameter] public EventCallback<CatProductos> EC_CatProductos { get; set; }

        /**/

        public ConsultaProduto.Request Consulta { get; set; }

        public IList<CatProductos> LstProducto { get; set; }

        /**/

        protected override void OnInitialized()
        {
            Consulta = new ConsultaProduto.Request();
            LstProducto = new List<CatProductos>();
        }


        protected override async Task OnInitializedAsync()
        {
            Consulta.bTodos = true;

            var consulta = await IAdministracion.ConsultaProduto_Async(Consulta);
            if (consulta.Ok)
            {
                foreach (var item in consulta.Data)
                {
                    LstProducto.Add(CatProductos.ParseFrom(item));
                }
            }
        }

        public async Task ConsultarProductoCompleto(CatProductos Productos)
        {
            var model = new ConsultaPrecioProducto.Request
            {
                iIdCatProducto = Productos.iIdCatProducto,
                bSoloActivos = false
            };
            var resp = await IAdministracion.ConsultaPrecioProducto_Async(model);
            if (resp.Ok)
            {

                Productos.LstEspProductosPrecios = CatProductos.LstPrecioParseFrom(resp.Data);
            }

            await EC_CatProductos.InvokeAsync(Productos);
        }
    }
}
