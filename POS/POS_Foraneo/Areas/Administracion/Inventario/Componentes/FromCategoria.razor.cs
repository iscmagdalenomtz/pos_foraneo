﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System.Collections.Generic;
using System.Threading.Tasks;
namespace POS_Foraneo.Areas.Administracion.Inventario.Componentes
{
    public partial class FromCategoria
    {
        [Inject] private IAdministracion IAdministracion { get; set; }
        [Inject] private IDialogService dialogService { get; set; }
        [Inject] private ISnackbar Snackbar { get; set; }

        [Parameter] public CatCategoria PCatCategoria { get; set; }
        [Parameter] public EventCallback<CatCategoria> EC_CatCategoria { get; set; }


        public MudForm Form { get; set; }
        public Valida Validar { get; set; }

        public IList<ConsultaCategorias.Result> LstCategorias { get; set; }

        public bool bSucces { get; set; }

        protected override void OnInitialized()
        {
            Validar = new Valida(dialogService);

            if (PCatCategoria.IsNull())
            {
                PCatCategoria = new();
            }
            LstCategorias = new List<ConsultaCategorias.Result>();

            bSucces = false;
        }

        protected override async Task OnInitializedAsync()
        {

            var model = new ConsultaCategorias.Request
            {
                bSoloActivos = true
            };
            var consulta = await Validar.ValidarSP(await IAdministracion.ConsultaCategoria_Async(model));
            if (consulta.Ok)
            {
                LstCategorias = consulta.Data;
            }
        }


        public async Task OnValid()
        {
            await Form.Validate();

            if (bSucces)
            {
                PCatCategoria.bActivo = true;
                var model = GuardaCategoria.RequestFrom(PCatCategoria);
                var resp = await Validar.ValidarSP(await IAdministracion.GuardaCategoria_Async(model), "Categoría guardada correctamente", true);
                if (resp.Ok)
                {

                    PCatCategoria = new();
                    if (EC_CatCategoria.HasDelegate)
                    {
                        await EC_CatCategoria.InvokeAsync(PCatCategoria);
                    }

                }
            }
        }




    }
}
