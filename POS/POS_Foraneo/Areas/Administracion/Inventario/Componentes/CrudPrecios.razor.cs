﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Objetos;
using POS_Foraneo.Helper;

using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Componentes
{
	public partial class CrudPrecios
	{

		[Parameter]
		public CatProductos PCatProducto { get; set; }

		public EspProductosPrecios Precio { get; set; }

		public MudForm Form { get; set; }

		/**/

		public bool bSucces { get; set; }


		protected override void OnInitialized()
		{
			Precio = new()
			{
				iIdCatProducto = PCatProducto.iIdCatProducto
			};
			if (PCatProducto.LstEspProductosPrecios.IsEmpty())
			{
				PCatProducto.LstEspProductosPrecios = new ();
				Precio.vchDescripcion = "Sucursal";
			}



		}



		public async Task OnGuardar()
		{
			await Form.Validate();

			if (bSucces)
			{
				Precio.bDefault = PCatProducto.LstEspProductosPrecios.Any(e => e.bDefault).False();
	
				Precio.bActivo = true;
				Precio.bActivo = true;
				Precio.iIdCatProducto = PCatProducto.iIdCatProducto;
				PCatProducto.LstEspProductosPrecios.Add(Precio);

				Precio = new()
				{
					iIdCatProducto = PCatProducto.iIdCatProducto
				};
			}
		}

	}
}
