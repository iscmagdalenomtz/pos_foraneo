﻿using Microsoft.AspNetCore.Components;

using POS_Foraneo.Areas.Administracion.Inventario.Data;
using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Areas.Objetos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Componentes
{
	public partial class TablaCategoria
	{

		[Inject] private IAdministracion IAdministracion { get; set; }

		[Parameter] public EventCallback<CatCategoria> EC_Categoria { get; set; }


		public IList<ConsultaCategorias.Result> LstCategorias { get; set; }

		protected override void OnInitialized()
		{
			LstCategorias = new List<ConsultaCategorias.Result>();
		}

		protected override async Task OnInitializedAsync()
		{
			await ConstarCategorias();
		}

		public async Task ConstarCategorias()
		{

			var consulta = await IAdministracion.ConsultaCategoria_Async(new ConsultaCategorias.Request());
			if (consulta.Ok)
			{
				LstCategorias = consulta.Data;
			}
		}


		public void OnSelectCatregoria(ConsultaCategorias.Result catProductos)
		{
			EC_Categoria.InvokeAsync(CatCategoria.ParseFrom(catProductos));
		}
	}
}
