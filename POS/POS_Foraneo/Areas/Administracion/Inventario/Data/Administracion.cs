﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Data;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Data
{
	public class Administracion : IAdministracion
	{

		private DapperHelper Helper { get; }

		public Administracion(IDapperManager iDapperManager)
		{
			Helper = new DapperHelper(iDapperManager);
		}


		public async Task<DataResult<InsertaProductos.Result>> InsertaProducto_Async(InsertaProductos.Request model)
		{
			return await Helper.GetAsync<InsertaProductos.Result, InsertaProductos.Request>(
				"prcInsertaProducto", model
			);
		}
		
		public async Task<DataResult<InsertaPrecio.Result>> InsertaPrecio_Async(InsertaPrecio.Request model)
		{
			return await Helper.GetAsync<InsertaPrecio.Result, InsertaPrecio.Request>(
				"prcInsertaPrecio", model
			);
		}

		public async Task<DataResult<IList<ConsultaProduto.Result>>> ConsultaProduto_Async(ConsultaProduto.Request model)
		{
			return await Helper.GetAllAsync<ConsultaProduto.Result, ConsultaProduto.Request>(
				"prcConsultaProducto", model
			);
		}

		public async Task<DataResult<IList<ConsultaPrecioProducto.Result>>> ConsultaPrecioProducto_Async(ConsultaPrecioProducto.Request model)
		{
			return await Helper.GetAllAsync<ConsultaPrecioProducto.Result, ConsultaPrecioProducto.Request>(
							"prcConsultaPrecioProducto", model
						);
		}

		public async Task<DataResult<GuardaCategoria.Result>> GuardaCategoria_Async(GuardaCategoria.Request model)
		{
			return await Helper.GetAsync<GuardaCategoria.Result, GuardaCategoria.Request>(
				"prcGuardaCategoria", model
			);
		}

		public async Task<DataResult<IList<ConsultaCategorias.Result>>> ConsultaCategoria_Async(ConsultaCategorias.Request model)
		{
			return await Helper.GetAllAsync<ConsultaCategorias.Result, ConsultaCategorias.Request>(
				"prcConsultaCategoria", model
			);
		}

	
	}
}
