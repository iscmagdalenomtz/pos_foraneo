﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Administracion.Inventario.Data
{
	public interface IAdministracion
	{
		Task<DataResult<InsertaProductos.Result>> InsertaProducto_Async(InsertaProductos.Request model);

		Task<DataResult<InsertaPrecio.Result>> InsertaPrecio_Async(InsertaPrecio.Request model);

		Task<DataResult<IList<ConsultaProduto.Result>>> ConsultaProduto_Async(ConsultaProduto.Request model);

		Task<DataResult<IList<ConsultaPrecioProducto.Result>>> ConsultaPrecioProducto_Async(ConsultaPrecioProducto.Request model);

		Task<DataResult<GuardaCategoria.Result>> GuardaCategoria_Async(GuardaCategoria.Request model);

		Task<DataResult<IList<ConsultaCategorias.Result>>> ConsultaCategoria_Async(ConsultaCategorias.Request model);

	}
}
