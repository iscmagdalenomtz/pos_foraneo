﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;

namespace POS_Foraneo.Areas.Objetos
{
	public class CatProductos
	{
		public int iIdCatProducto { get; set; }
		public string vchNombre { get; set; } = string.Empty;
		public string vchDescripcion { get; set; } = string.Empty;
		public int iIdCatCategoria { get; set; }
		public string vchNombreCategoria { get; set; }
		public bool bActivo { get; set; }
		public DateTime dtFechaUltModificacion { get; set; } = Default.Date;

		public List<EspProductosPrecios> LstEspProductosPrecios { get; set; } = new();
		public int iIdEspProductosPrecioDefault { get; set; }
		public decimal mPrecioDefault { get; set; }


		public static CatProductos ParseFrom(ConsultaProduto.Result request)
		{
			return new CatProductos
			{
				iIdCatProducto = request.iIdCatProducto,
				vchNombre = request.vchNombre,
				vchDescripcion = request.vchDescripcion,
				iIdCatCategoria = request.iIdCatCategoria,
				vchNombreCategoria = request.vchNombreCategoria,
				bActivo = request.bActivo,
				iIdEspProductosPrecioDefault = request.iIdEspProductosPrecioDefault,
				mPrecioDefault = request.mPrecioDefault

			};
		}

		public static List<EspProductosPrecios> LstPrecioParseFrom(IList<ConsultaPrecioProducto.Result> request)
		{

			var tmpLista = new List<EspProductosPrecios>();
            foreach (var item in request)
            {
				tmpLista.Add(new EspProductosPrecios
				{
					iIdEspProductosPrecio = item.iIdEspProductosPrecio,
					iIdCatProducto = item.iIdCatProducto,
					bActivo = item.bActivo,
					bDefault = item.bDefault,
					mPrecio = item.mPrecio,
					vchDescripcion = item.vchDescripcion,
				});

			}

			return tmpLista;
		}
	}
}
