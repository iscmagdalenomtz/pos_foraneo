﻿using POS_Foraneo.Areas.Caja.Comanda.Modelos;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class GralComanda
	{
		public int iIdGralComanda { get; set; }
		public DateTime dtFechaAlta { get; set; } = Default.Date;
		public string vchNombre { get; set; } = string.Empty;
		public string vchComentarioGeneral { get; set; } = string.Empty;
		public int iIdTipoVenta { get; set; }
		public string vchTipoVenta { get; set; } = string.Empty;
		public int iIdEstatus { get; set; }
		public string vchEstatus { get; set; } = string.Empty;
		public decimal mTotalCobrado { get; set; }
		public decimal mSubTotal { get; set; }
		public short smDescuento { get; set; }

		public List<EspComanda> LstEspComanda { get; set; }

		/**/
		public bool bShowDetalles { get; set; }

		public void UpdateFrom(ConsultaGralComanda.Result request)
		{
			this.iIdGralComanda = request.iIdGralComanda;
			this.vchNombre = request.vchNombre;
			this.vchComentarioGeneral = request.vchComentarioGeneral;
			this.iIdTipoVenta = request.iIdTipoVenta;
			this.iIdEstatus = request.iIdEstatus;
			this.mTotalCobrado = request.mTotalCobrado;
			this.smDescuento = request.smDescuento;
			this.dtFechaAlta = request.dtFechaAlta;

		}


		public static List<EspComanda> ParseFrom(IList<ConsultaEspComanda.Result> request)
		{
			var respuesta = new List<EspComanda>();

			foreach (var item in request)
			{
				respuesta.Add(EspComanda.ParseFrom(item));
			}

			return respuesta;
		}

		public static List<GralComanda> ParseFrom(IList<ConsultaGralComanda.Result> request)
		{
			var respuesta = new List<GralComanda>();

			foreach (var item in request)
			{
				respuesta.Add(ParseFrom(item));
			}

			return respuesta;
		}


		public static GralComanda ParseFrom(GuardaGralComanda.Request request)
		{
			return new GralComanda
			{
				iIdGralComanda = request.iIdGralComanda,
				vchNombre = request.vchNombre,
				vchComentarioGeneral = request.vchComentarioGeneral,
				iIdTipoVenta = request.iIdTipoVenta,
				iIdEstatus = request.iIdEstatus,
				mTotalCobrado = request.mTotalCobrado,
				smDescuento = request.smDescuento,
			};
		}

		public static GralComanda ParseFrom(ConsultaGralComanda.Result request)
		{
			return new GralComanda
			{
				iIdGralComanda = request.iIdGralComanda,
				vchNombre = request.vchNombre,
				vchComentarioGeneral = request.vchComentarioGeneral,
				iIdTipoVenta = request.iIdTipoVenta,
				vchTipoVenta = request.vchTipoVenta,
				iIdEstatus = request.iIdEstatus,
				vchEstatus = request.vchEstatus,
				mTotalCobrado = request.mTotalCobrado,
				mSubTotal = request.mSubTotal,
				smDescuento = request.smDescuento,
				dtFechaAlta = request.dtFechaAlta,
			};
		}

	}
}
