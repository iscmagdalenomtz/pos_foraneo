﻿using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class EspCorteCaja
	{
		public int iIdEspCorteCaja { get; set; }
		public int iIdGralCorteCaja { get; set; }
		public int iIdGralComanda { get; set; }
		public int iIdTipoPago { get; set; }
		public int iIdTipoMovimiento { get; set; }
		public string vchTipoMovimiento { get; set; }
		public string vchTipoPago { get; set; }
		public decimal mTotalRecaudado { get; set; }
		public DateTime dtFechaUltModificacion { get; set; } = Default.Date;

		public static EspCorteCaja ParseFrom(ConsultaEspCorteCaja.Result tmpCorteCaja)
		{
			return new EspCorteCaja
			{
				iIdEspCorteCaja = tmpCorteCaja.iIdEspCorteCaja,
				iIdGralCorteCaja = tmpCorteCaja.iIdGralCorteCaja,
				iIdGralComanda = tmpCorteCaja.iIdGralComanda,
				iIdTipoPago = tmpCorteCaja.iIdTipoPago,
				vchTipoPago = tmpCorteCaja.vchTipoPago,
				iIdTipoMovimiento = tmpCorteCaja.iIdTipoMovimiento,
				vchTipoMovimiento = tmpCorteCaja.vchTipoMovimiento,
				mTotalRecaudado = tmpCorteCaja.mTotalRecaudado,
				dtFechaUltModificacion = tmpCorteCaja.dtFechaUltModificacion,
				
			};
		}

		public static EspCorteCaja ParseFrom(GuardaEspCorteCaja.Request tmpCorteCaja)
		{
			return new EspCorteCaja
			{
				iIdEspCorteCaja = -1,
				iIdGralCorteCaja = tmpCorteCaja.iIdGralCorteCaja,
				iIdGralComanda = tmpCorteCaja.iIdGralComanda,
				iIdTipoPago = tmpCorteCaja.iIdTipoPago,
				iIdTipoMovimiento = tmpCorteCaja.iIdTipoMovimiento,
				mTotalRecaudado = tmpCorteCaja.mTotalRecaudado,
				dtFechaUltModificacion = Default.Date,
				
			};
		}

		public static List<EspCorteCaja> ParseFrom(IList<ConsultaEspCorteCaja.Result> tmpLstCorteCaja)
		{
			var tempLst = new List<EspCorteCaja>();

			foreach (var item in tmpLstCorteCaja)
			{
				tempLst.Add(ParseFrom(item));
			}

			return tempLst;

		}

	}
}
