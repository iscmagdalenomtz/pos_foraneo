﻿using POS_Foraneo.Areas.Administracion.Inventario.Modelos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class CatCategoria
	{

		public int iIdCatCategoria { get; set; }
		public string vchNombre { get; set; } = string.Empty;
		public string vchDescripcion { get; set; } = string.Empty;
		public int iIdCatCategoriaPadre { get; set; } = -1;
		public string vchNombrePadre { get; set; } = string.Empty;
		public bool bActivo { get; set; }
		public DateTime dtFechaUltModificacion { get; set; } = new DateTime(1900, 1, 1);


		public static CatCategoria ParseFrom(ConsultaCategorias.Result result)
		{
			return new CatCategoria
			{
				iIdCatCategoria = result.iIdCatCategoria,
				vchNombre = result.vchNombre,
				vchDescripcion = result.vchDescripcion,
				iIdCatCategoriaPadre = result.iIdCatCategoriaPadre,
				bActivo = result.bActivo,
				dtFechaUltModificacion =  result.dtFechaUltModificacion,
			};
		}
	}
}
