﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class EspProductosPrecios
	{
		public int iIdEspProductosPrecio { get; set; }
		public int iIdCatProducto { get; set; }
		public decimal mPrecio { get; set; }
		public string vchDescripcion { get; set; } = string.Empty;
		public bool bActivo { get; set; }
		public DateTime dtFechaUltModificacion { get; set; } = Default.Date;
		public bool bDefault { get; set; }
	}
}
