﻿using POS_Foraneo.Areas.Caja.Comanda.Modelos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class EspComanda
	{
		public int iIdGralComanda { get; set; }
		public int iIdEspComanda { get; set; }
		public int iIdEstatus { get; set; }
		public string vchEstatus { get; set; } = string.Empty;
		public int iIdCatProducto { get; set; }
		public string vchNombreProducto { get; set; }
		public int iIdEspProductosPrecio { get; set; }
		public decimal mPrecioUnitario { get; set; }
		public string vchDescripcion { get; set; } = string.Empty;
		public string vchComentario { get; set; } = string.Empty;
		public short smCantidad { get; set; }
		public short smDescuento { get; set; }
		public decimal mTotal { get; set; }


		public static EspComanda ParseFrom(ConsultaEspComanda.Result result)
		{
			return new EspComanda
			{
				iIdGralComanda = result.iIdGralComanda,
				iIdEspComanda = result.iIdEspComanda,
				iIdEstatus = result.iIdEstatus,
				iIdCatProducto = result.iIdCatProducto,
				vchNombreProducto = result.vchNombreProducto,
				iIdEspProductosPrecio = result.iIdEspProductosPrecio,
				mPrecioUnitario = result.mPrecioUnitario,
				vchDescripcion = result.vchDescripcion,
				vchComentario = result.vchComentario,
				smCantidad = result.smCantidad,
				smDescuento = result.smDescuento,
				mTotal = result.mTotal,

			};
		}

		public static EspComanda ParseFrom(CatProductos result)
		{

			var iIdEspProductosPrecio = result.LstEspProductosPrecios.Where(e => e.bDefault).SingleOrDefault();
			return new EspComanda()
			{
				iIdEspComanda = -1,
				iIdCatProducto = result.iIdCatProducto,
				iIdEspProductosPrecio = result.iIdEspProductosPrecioDefault,
				mPrecioUnitario = result.mPrecioDefault,
				vchNombreProducto = result.vchNombre,
				vchDescripcion = $"{result.vchNombreCategoria} - {result.vchNombre}"
			};
		}
	}
}
