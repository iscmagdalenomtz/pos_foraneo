﻿using POS_Foraneo.Areas.Caja.CajaRegistradora.Modelos;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Areas.Objetos
{
	public class GralCorteCaja
	{

		public int iIdGralCorteCaja { get; set; }
		public DateTime dtFechaIncicio { get; set; } = Default.Date;
		public DateTime dtFechaFin { get; set; } = Default.Date;
		public string vchDescripcion { get; set; } = string.Empty;
		public decimal mTotal { get; set; }
		public decimal mTotalSistema { get; set; }
		public DateTime dtFechaUltModificacion { get; set; } = Default.Date;
		public bool bActivo { get; set; }
		public bool bShowDetalles { get; set; }
		public List<EspCorteCaja> LstEspCorteCaja { get; set; }



		public static GralCorteCaja ParseFrom(ConsultaGralCorteCaja.Result tmpCorteCaja)
		{
			return new GralCorteCaja
			{
				iIdGralCorteCaja = tmpCorteCaja.iIdGralCorteCaja,
				dtFechaIncicio = tmpCorteCaja.dtFechaIncicio,
				dtFechaFin = tmpCorteCaja.dtFechaFin,
				vchDescripcion = tmpCorteCaja.vchDescripcion,
				dtFechaUltModificacion = tmpCorteCaja.dtFechaUltModificacion,
				bActivo = tmpCorteCaja.bActivo,
				mTotalSistema = tmpCorteCaja.mTotalSistema,
				mTotal = tmpCorteCaja.mTotal,
				LstEspCorteCaja = new()
			};
		}



		public static IList<GralCorteCaja> ParseFrom(IList<ConsultaGralCorteCaja.Result> tmpLstCorteCaja)
		{
			var tmplst = new List<GralCorteCaja>();

			foreach (var item in tmpLstCorteCaja)
			{
				tmplst.Add(ParseFrom(item));
			}
			return tmplst;

		}



	}
}
