﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.Constantes
{
	public static class _1_TipoVenta
	{
		public const int iIdSubAgrupador = 1;
		public const int LOCAL = 1;
		public const int LLEVAR = 2;
		public const int VENTA_LINEA = 3;

	}
}
