﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.Constantes
{
	public static class  _3_EstatusEspComanda
	{
		public const int iIdSubAgrupador = 3;
		public const int ACTIVO = 8;
		public const int CANCELADO = 9;
		public const int DEVELTO = 10;
		public const int CORTESIA = 11;
	}
}
