﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.Constantes
{
	public static class _5_TipoMovimiento
	{
		public const int iIdSubAgrupador = 5;
		public const int COMANDA = 15;
		public const int DEPOSITO = 16;
		public const int RETIRO = 17;
	}
}
