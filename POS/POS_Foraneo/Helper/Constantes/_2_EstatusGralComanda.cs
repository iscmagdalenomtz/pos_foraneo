﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.Constantes
{
	public static class _2_EstatusGralComanda
	{
		public const int iIdSubAgrupador = 2;
		public const int ORDEN = 4;
		public const int ENTREGADA = 5;
		public const int PAGADA = 6;
		public const int CANCELADA = 7;
	}

}
