﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper
{
    public class Result
    {
        public bool Ok { get; }
        public string Msg { get; }
        public Exception Ex { get; }
        public Error Error { get; }

        public Result(bool ok)
        {
            Ok = ok;
        }

        public Result(bool ok, string devMsg, Exception ex)
        {
            Ok = ok;
            Msg = devMsg;
            Ex = ex;
        }

        private Result(bool ok, Error error, Exception ex)
        {
            Ok = ok;
            Error = error;
            Msg = error.Message();
            Ex = ex;
        }


        public static Result Success()
        {
            return new Result(true);
        }

        public static Result Fail(string devMsg = "", Exception ex = null)
        {
            return new Result(false, devMsg, ex);
        }

        public static Result Fail(Error error, Exception ex = null)
        {
            return new Result(false, error, ex);
        }
    }
}
