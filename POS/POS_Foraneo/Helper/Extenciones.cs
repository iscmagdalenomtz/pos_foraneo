﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper
{
	public static class Extenciones
	{


		/*Extensiones para Objetos*/

		public static bool IsNull(this object obj)
		{
			return obj == null;
		}

		public static bool NotNull(this object obj)
		{
			return obj != null;
		}


		/*Extenciones para String*/

		public static bool IsEmpty(this string value)
		{
			return string.IsNullOrEmpty(value);
		}

		public static bool NotEmpty(this string value)
		{
			return string.IsNullOrEmpty(value).False();
		}


		/**Extensiones para bool*/
		public static bool False(this bool @bool)
		{
			return !@bool;
		}

		/*Extenciones para Listas*/

		public static bool IsEmpty<T> (this IEnumerable<T> lst)
		{
			if (lst.IsNull()) return true;

			if (lst.Count() == 0) return true;

			return false;
		}

		public static bool NotEmpty<T>(this IEnumerable<T> lst)
		{
			if (lst.IsNull()) return false;

			return lst.Any();
		}


		/*Objetos*/
		public static DataResult<T> SearchProperty<T>(this object obj, string propertyName)
		{
			try
			{
				var properties = obj.GetType().GetProperties();
				var property = properties.FirstOrDefault(p => p.Name == propertyName);

				if (property.NotNull())
				{
					return DataResult<T>.Success((T)property.GetValue(obj));
				}
			}
			catch (Exception ex)
			{
				//Logger.Get.Exception(ex);
			}

			return DataResult<T>.Fail();
		}

	}
}
