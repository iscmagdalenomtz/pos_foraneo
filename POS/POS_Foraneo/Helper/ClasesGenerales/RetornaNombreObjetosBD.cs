﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.ClasesGenerales
{
	public static class RetornaNombreObjetosBD
	{
		public class Request
		{
			public string type { get; set; }
		}

		public class Result
		{
			public string name { get; set; }
			public string type { get; set; }
			public string DescType { get; set; }
			public DateTime crdate { get; set; }

		}
	}
}
