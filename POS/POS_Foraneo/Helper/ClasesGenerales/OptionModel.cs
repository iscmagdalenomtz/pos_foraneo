﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.ClasesGenerales
{
	public class OptionModel
	{
		public int IdOpcion { get; set; }
		public string vchValue { get; set; }

		public OptionModel(){}


		public OptionModel( int id, string value)
		{
			IdOpcion = id;
			vchValue = value;
		}

		public override string ToString()
		{
			return vchValue;
		}
	}
}
