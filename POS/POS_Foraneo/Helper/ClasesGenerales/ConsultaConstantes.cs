﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.ClasesGenerales
{
	public static class ConsultaConstantes
	{

		public class Request
		{
			public int iIdCatConstante { get; set; }
			public int iIdSubAgrupador { get; set; }
		}


		public class Result
		{
			public int bResultado { get; set; }
			public string vchMensaje { get; set; } = string.Empty;
			public int iIdCatConstante { get; set; }
			public int iIdSubAgrupador { get; set; }
			public string vchNombre { get; set; } = string.Empty;
			public string vchDescripcion { get; set; } = string.Empty;

		}
	}
}
