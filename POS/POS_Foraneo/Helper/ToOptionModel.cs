﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper
{
	public static class ToOptionModel
	{

		public static IList<OptionModel> From(IList<ConsultaConstantes.Result> requests)
		{
			var lista = new List<OptionModel>();
			foreach (var item in requests)
			{
				lista.Add(new OptionModel(item.iIdCatConstante, $"[{item.iIdCatConstante}]-{item.vchNombre}"));
			}

			return lista;
		}
	}
}
