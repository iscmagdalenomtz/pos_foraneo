﻿using Microsoft.AspNetCore.Components;

using MudBlazor;

using POS_Foraneo.Areas.Objetos;
namespace POS_Foraneo.Helper.Modales
{
	public partial class ModalTicket
	{


		[CascadingParameter] MudDialogInstance MudDialog { get; set; }


		[Parameter] public GralComanda PGralComanda { get; set; }


		private void Cancel()
		{
			MudDialog.Cancel();
		}


		
	}
}
