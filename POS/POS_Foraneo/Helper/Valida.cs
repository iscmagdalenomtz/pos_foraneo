﻿using MudBlazor;

using POS_Foraneo.Areas.Common.Modales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper
{
    public class Valida
    {
        private readonly IDialogService dialogService;

        public Valida(IDialogService DialogService)
        {
            dialogService = DialogService;
        }


        public async Task<DataResult<T>> ValidarSP<T>(DataResult<T> dataResult, string vchMsg = "", bool bShowModal = false)
        {
            var vchMensje = string.Empty;
            bool bValidacion = false;
            var vchTipoError = string.Empty;


            if (dataResult.Ok)
            {
                if (dataResult.Data.NotNull())
                {
                    var validateErrorResult = ValidateError(dataResult.Data);
                    if (validateErrorResult.Ok)
                    {
                        vchMensje = vchMsg;
                        bValidacion = true;
                    }
                    else
                    {
                        bValidacion = false;
                        vchMensje = validateErrorResult.Msg;
                    }
                }
            }
            else
            {
                bValidacion = false;
                vchMensje = dataResult.Ex.Message;
            }



            var parameters = new DialogParameters();
            parameters.Add("vchMensaje", vchMensje);

            if (bValidacion.False())
            {
                await dialogService.Show<InfoModal>("ALV !!", parameters).Result;
                return dataResult;
            }
            else
            {
                if (bShowModal)
                {
                    await dialogService.Show<InfoModal>("Nice Bro ..", parameters).Result;
                }
                return dataResult;
            }

        }


        public static Result ValidateError<T>(T obj)
        {
            var searchResultadoResult = obj.SearchProperty<bool>("bResultado");

            if (searchResultadoResult.Ok && searchResultadoResult.Data.False())
            {
                var searchMensajeResult = obj.SearchProperty<string>("vchMensaje");

                if (searchMensajeResult.Ok)
                {
                    return Result.Fail(searchMensajeResult.Data);
                }
            }

            return Result.Success();
        }





    }
}
