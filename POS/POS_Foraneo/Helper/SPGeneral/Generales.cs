﻿using POS_Foraneo.Data;
using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.SPGeneral
{
	public class Generales : IGenerales
	{

		private DapperHelper Helper { get; }

		public Generales(IDapperManager iDapperManager)
		{
			Helper = new DapperHelper(iDapperManager);
		}

		public async Task<DataResult<IList<ConsultaConstantes.Result>>> ConsultaConstantes_Async(ConsultaConstantes.Request model)
		{
			return await Helper.GetAllAsync<ConsultaConstantes.Result, ConsultaConstantes.Request>(
				"prcConsultaConstantes", model
			);
		}
	}
}
