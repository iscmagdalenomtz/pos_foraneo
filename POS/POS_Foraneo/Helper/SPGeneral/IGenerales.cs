﻿using POS_Foraneo.Helper.ClasesGenerales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Helper.SPGeneral
{
	public interface IGenerales
	{
		Task<DataResult<IList<ConsultaConstantes.Result>>> ConsultaConstantes_Async(ConsultaConstantes.Request model);

	}
}
