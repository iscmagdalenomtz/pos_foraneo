﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Shared
{
	public partial class MainLayout
	{

		[Inject] private IJSRuntime JSRuntime { get; set; }

		public bool open { get; set; }
		public int HeightScreen { get; set; }


		protected override async Task OnAfterRenderAsync(bool firstRender)
		{
			if (firstRender)
			{
				HeightScreen = await JSRuntime.InvokeAsync<int>("GetHeight");
				StateHasChanged();
			}
		}



		public void ToggleDrawer()
		{
			open = !open;
		}

	}
}
