﻿using Dapper;

using Microsoft.Data.SqlClient;

using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_Foraneo.Data
{
	public class DapperHelper
	{
		private  DateTime DateDefault = new DateTime(1900, 1, 1);
		private IDapperManager Manager { get; set; }

		public DapperHelper(IDapperManager dapperManager)
		{
			Manager = dapperManager;
		}



		public async Task<DataResult<TResult>> GetAsync<TResult, TRequest>(string name, TRequest request)
		{
			var sp = $"[dbo].[{name}]";
			var executableString = string.Empty;

			try
			{
				var dynamicParameters = CreateDynamicParameters(name, request);

				executableString = dynamicParameters.ExcecutableString;

				var data = Manager.Get<TResult>(sp, dynamicParameters.Parameters);

				return await Task.FromResult(DataResult<TResult>.Success(data));
			}
			catch (Exception ex)
			{
				//Logger.Get.Exception(ex, name);

				if (ex is SqlException sqlEx)
				{
					return await Task.FromResult(DataResult<TResult>.Fail(Error.DBException(sqlEx.Number, executableString), sqlEx));
				}

				return await Task.FromResult(DataResult<TResult>.Fail(Error.Exception("DPG", ex.Message, executableString)));
			}
		}

		public async Task<DataResult<IList<TResult>>> GetAllAsync<TResult, TRequest>(string name, TRequest model)
		{
			var sp = $"[dbo].[{name}]";
			var executableString = string.Empty;

			try
			{
				var dynamicParameters = CreateDynamicParameters(name, model);
				executableString = dynamicParameters.ExcecutableString;

				var data = Manager.GetAll<TResult>(sp, dynamicParameters.Parameters);

				return await Task.FromResult(DataResult<IList<TResult>>.Success(data));
			}
			catch (Exception ex)
			{
				//Logger.Get.Exception(ex, name);

				if (ex is SqlException sqlEx)
				{
					return await Task.FromResult(DataResult<IList<TResult>>.Fail(Error.DBException(sqlEx.Number, executableString), sqlEx));
				}

				return await Task.FromResult(DataResult<IList<TResult>>.Fail(Error.Exception("DPGA", ex.Message, executableString)));
			}
		}

		private DynamicParametersPair CreateDynamicParameters(string name, object obj)
		{
			var parameters = new DynamicParameters();
			var builder = new StringBuilder();
			var linebreak = string.Empty;
			var space = " ";
			var tab = string.Empty;


			builder.Append($"{linebreak}EXEC").Append(" dbo.").Append(name).Append(' ').Append(space);

			foreach (var prop in obj.GetType().GetProperties())
			{
				var property = prop.Name;
				var pair = AnalyzeProperty(prop.PropertyType, prop.GetValue(obj));

				parameters.Add(property, pair.Item1, pair.Item2);
				builder.Append($"{tab}@").Append(property).Append(" = ").Append(Beautify(pair.Item1)).Append(',').Append(space);
			}

			var end = builder.Length;

			builder.Remove(end - 2, 2);

			Console.WriteLine(builder);
			//Logger.Get.Info($"{builder}{linebreak}");

			return new DynamicParametersPair(parameters, builder.ToString());
		}

		private object Beautify(object value)
		{
			var type = value.GetType();

			switch (type.FullName)
			{
				case "System.String":
					value ??= string.Empty;
					value = $"'{value}'";

					break;

				case "System.DateTime":
					value ??= DateDefault;
					value = $"'{((DateTime)value):yyyy-MM-dd}'";

					break;
			}

			return value;
		}

		private static Tuple<object, DbType> AnalyzeProperty(Type type, object value)
		{
			var dbType = DbType.Object;

			switch (type.FullName)
			{
				case "System.Boolean":
					dbType = DbType.Boolean;

					break;

				case "System.Byte":
					dbType = DbType.Byte;

					break;

				case "System.Byte[]":
					dbType = DbType.Binary;

					break;

				case "System.Int16":
					dbType = DbType.Int16;

					break;

				case "System.Int32":
					dbType = DbType.Int32;

					break;

				case "System.Int64":
					dbType = DbType.Int64;

					break;

				case "System.String":
					dbType = DbType.String;
					value ??= string.Empty;

					break;

				case "System.DateTime":
					dbType = DbType.DateTime;
					value ??= new DateTime(1900,1,1);

					break;

				case "System.Decimal":
				case "System.Single":

					dbType = DbType.Decimal;

					break;
			}

			return new Tuple<object, DbType>(value, dbType);
		}

		private class DynamicParametersPair
		{
			public DynamicParameters Parameters { get; }

			public string ExcecutableString { get; }

			public DynamicParametersPair(DynamicParameters parameters, string excecutableString)
			{
				Parameters = parameters;
				ExcecutableString = excecutableString;
			}
		}
	}
}