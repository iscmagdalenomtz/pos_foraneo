﻿using Dapper;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Data
{
	public interface IDapperManager :IDisposable
	{

		DbConnection GetConnection();

		T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);
		
		IList<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure);

	}
}
