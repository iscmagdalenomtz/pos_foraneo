﻿using Dapper;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

using POS_Foraneo.Helper;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace POS_Foraneo.Data
{
	public class DapperManager : IDapperManager
	{
		public string ConnectionString { get; set; }



		public DapperManager( IConfiguration config)
		{
			if(ConnectionString.IsEmpty())
			{
				ConnectionString = config.GetSection("ConnectionStrings").GetSection("POSConnection").Value;
			}
		}

		public DbConnection GetConnection()
		{
			return new SqlConnection(ConnectionString);
		}

		public T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
		{
			using (IDbConnection db = new SqlConnection(ConnectionString))
			{
				return db.Query<T>(sp,parms, commandTimeout: 250, commandType: commandType).FirstOrDefault();
			}
		}

		public IList<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
		{
			using (IDbConnection db = new SqlConnection(ConnectionString))
			{
				return db.Query<T>(sp, parms, commandTimeout: 250, commandType: commandType).ToList();
			}
		}



		public void Dispose()
		{
		}
	}
}
