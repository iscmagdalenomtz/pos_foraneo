

ALTER procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020,16/Dic/2020,21/Dic/2020,31/Marzo/2021
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante,RConstante,RConstante,RConstante
** Revisi�n:				8
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''_SPNombre_'' ) '
	Print '		DROP PROCEDURE _SPNombre_'
	Print 'GO'	
	Print 'CREATE PROCEDURE _SPNombre_ (
			@iIdUsuario INT,
			@bRetornarMensaje BIT = 1
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					_SPNombre_' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		'  --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '''' '
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado BIT  DEFAULT(1),'
	Print '		vchMensaje VARCHAR(500)  DEFAULT('''')'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print '	SET @trancount = @@TRANCOUNT'	
	Print '	IF @trancount > 0 '
	Print '	  SAVE TRANSACTION _SPNombre_;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	'
	Print '	'
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF @trancount = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF  @trancount <> -1 AND XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION _SPNombre_;  	'
	Print '	Goto _Fin'
	Print '_FinTran:	'
	Print '		IF @trancount = 0'
	Print '			COMMIT TRANSACTION;  	'	
	Print '	'
	Print '	'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	PRINT Concat(''_SPNombre_: '', ERROR_MESSAGE(),'' '', ERROR_PROCEDURE(),'' '', ERROR_LINE() )'
	Print '	'
	Print '	IF @trancount = 0'
	Print '		ROLLBACK TRANSACTION'
	Print '	ELSE IF @trancount <> -1 '
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION _SPNombre_;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print '	IF(LEN(@vchError) > 0 )	'
	Print '		BEGIN'
	Print '			INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '			SELECT 0, @vchError	'
	Print '		END	'
	Print '	ELSE	'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		SELECT 1, ''''	'
	Print ''
	Print '	SELECT bResultado,vchMensaje'
	Print '	From @Respuesta'
	Print ''
	Print 'GO'	
End



