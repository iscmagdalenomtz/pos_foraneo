USE master
GO
--[RConstante 30/Abril/2020 se agrega filtro para agrupador de INGRES para el Cotrol + 4]
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'SP_SELECT_ALL' ) 
	DROP PROCEDURE SP_SELECT_ALL
GO
CREATE procedure dbo.SP_SELECT_ALL(
@objname varchar(500),
@filtro varchar(50) = '',
@campo varchar(50) = ''
)
as
/*
** Nombre:					SP_SELECT_ALL
** Prop�sito:				Facilitar los selects
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		?,30/Abril/2020
** Autor modificaci�n: 		RConstante,RConstante
** Revisi�n:				8
*/
Declare @sysobj_type char(2),
@columna varchar(100)
Declare @vchCommand varchar(700)
Declare @nvchCommand2 nVarchar(512)
Declare @nvchParametros nVarchar(128)
Declare @nvchEnter nvarchar(2)
Declare @vchNombreObjname nvarchar(128)
Declare @vchAliasObjeto nvarchar(128)
Declare @vchNombreSquema	varchar(80)
Declare @iIdBaseDatos int --Select * From bd_sicyproh..CatGralObjeto
Declare @vchNombreBaseDatos varchar(256)
Declare @vchQuery varchar(512)
Declare @siAgrupador int
Declare @iIdIdentificador int
Declare @iIdGralObjeto int
Declare @vchNombreCampoLlave varchar(50)
Declare @vchNombreCampoAgrupador varchar(50)
DECLARE @vchNombre2doCampoAgrupador varchar(50)
Declare @iError int
Declare @iErrorSql int
Declare @iRegistros int
Declare @vchOrderByPrimaryKey nvarchar(256)

Set nocount on

Set @iError = 0
Set @vchNombreBaseDatos = ''
Set @vchNombreCampoLlave = ''
Set @vchNombreCampoAgrupador = ''
Set @nvchEnter = char(13) + char(10)

Declare @tmpColumna table 
(
	vchColumna varchar(256) null
)

Declare @helpIndex table 
(
	index_name			sysname collate database_default NOT NULL,
	index_description	varchar(210),
	index_keys			nvarchar(2126) collate database_default NOT NULL
)

If isnumeric(@objname) = 0 --No es un numero
Begin
	If @objname like '%..%'
	Begin		
		Select @vchNombreBaseDatos = substring(@objname, 1, CHARINDEX('.',@objname) - 1)
		Select @vchNombreObjname = substring(@objname, CHARINDEX('.',@objname) + 2, len(@objname))
	End
	Else
	Begin
		Set @vchNombreObjname = @objname
	End
End
Else
Begin
	Set @vchNombreObjname = @objname
End

Select @sysobj_type = xtype
From sysobjects
where id = object_id(@vchNombreObjname)

If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
Begin
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	
	Set @vchCommand = 'Select'
	If @filtro = '' or @filtro is null --No hay un filtro
	Begin --Retornar unicamente 20 registros
		Set @vchCommand = @vchCommand + ' Top 20 * From ' + @vchNombreSquema + '.' + @vchNombreObjname + ' ' + @nvchEnter
		
		--Hacer un order by por la llave primaria de la tabla
		Exec SP_PpInOrderByPrimaryKey
		@vchTabla = @vchNombreObjname,
		@vchOrderByPrimaryKey = @vchOrderByPrimaryKey Output
		
		Set @vchCommand = @vchCommand + @vchOrderByPrimaryKey		
	End
	Else
	Begin
		Set @vchCommand = @vchCommand + ' * From ' + @vchNombreSquema + '.' + @vchNombreObjname + ' '
	End
End
Else
Begin
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + '..VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	
	Set @vchCommand = 'Select'
	If @filtro = '' or @filtro is null --No hay un filtro
	Begin --Retornar unicamente 20 registros
		Set @vchCommand = @vchCommand + ' top 20 * From ' + @vchNombreBaseDatos + '.' + @vchNombreSquema + '.' + @vchNombreObjname + ' ' + @nvchEnter
		
		--Hacer un order by por la llave primaria de la tabla
		Exec SP_PpInOrderByPrimaryKey
		@vchTabla = @vchNombreObjname,
		@vchOrderByPrimaryKey = @vchOrderByPrimaryKey Output
		
		Set @vchCommand = @vchCommand + @vchOrderByPrimaryKey		
	end
	Else
	Begin
		Set @vchCommand = @vchCommand + ' * From ' + @vchNombreBaseDatos + '.' + @vchNombreSquema + '.' + @vchNombreObjname + ' '
	End
End

SET NOCOUNT ON

-- Si s�lo pasan un n�mero este n�mero se busca en las listas
If (@filtro = '' And @campo = '' And IsNumeric(@vchNombreObjname) = 1)
Begin

	Set @iIdIdentificador = cast( @vchNombreObjname as int )
	
	Select Top 1 @iIdGralObjeto = iIdGralObjeto
	From BDControlVehicular_TEST..CatEspObjetoRangoIdentificador
	Where iRangoInicial <= @iIdIdentificador
	And iRangoFinal >= @iIdIdentificador

	If @iIdGralObjeto is null or @iIdGralObjeto <= 0
	Begin
		Set @vchCommand = 'El identificar no corresponde a un valor valido (no existe en CatEspObjetoRangoIdentificador)'
		Goto fin
	End
	
	Select @vchNombreObjname = vchNombre--,
	/*@iIdBaseDatos = iIdBaseDatos,
	@vchAliasObjeto = vchAlias*/
	From BDControlVehicular_TEST..CatGralObjeto
	Where iIdGralObjeto = @iIdGralObjeto
	And iIdTipoObjeto = 40000 --Tabla

	--Set @vchNombreObjname = 'CatConstantes'
	
	If @vchNombreObjname is null or @vchNombreObjname = ''
	Begin
		Set @vchCommand = 'El id del objeto no corresponde a una tabla (CatGralObjeto.iIdGralObjeto--> ' + cast(@iIdGralObjeto as varchar) + ')'
		Goto fin
	End	
	
	--recuperar la base de datos
	--la que este relacionada al objeto
	--Select @vchNombreBaseDatos = vchDescripcion
	--From bd_sicyproh..sysValorLista
	--Where siAgrupador = 73280
	--And siConsecutivo = @iIdBaseDatos
	
	If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
	Begin
		Set @vchNombreBaseDatos = 'BDControlVehicular_TEST'
	End
		
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + '..VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2, @vchNombreSquema as vchNombreSquema
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	

	--Buscar el nombre del campo que es el campo llave o
	--Por el campo que se haria la busqueda
	--Select @vchNombreCampoLlave = CEO.vchNombre
	--From Bd_sicyproh..TranParametroValorDinamicoObj TPVD, Bd_sicyproh..CatEspObjeto CEO
	--Where TPVD.iIdEspDicDatos = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	--And TPVD.iIdParametroDinamico = 66020 --Campo llave de la tabla
	--And CEO.iIdGralObjeto = @iIdGralObjeto


	Select @vchNombreCampoLlave = CEO.vchNombre
	From BDControlVehicular_TEST..CatEspObjetoTipoValor TPVD, BDControlVehicular_TEST..CatEspObjeto CEO
	Where TPVD.iIdEspObjeto = CEO.iIdEspObjeto 
	And TPVD.iIdTipoValor = 40420 --Campo llave de la tabla
	And CEO.iIdGralObjeto = @iIdGralObjeto


	--Set @vchNombreCampoLlave = 'iIdConstante'
	
	
	--Buscar el nombre del campo agrupador
	--Para ejecutar la segunda consulta, por el agrupador
	--Select @vchNombreCampoAgrupador = CEO.vchNombre
	--From Bd_sicyproh..TranParametroValorDinamicoObj TPVD, Bd_sicyproh..CatEspObjeto CEO
	--Where TPVD.iIdEspDicDatos = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	--And TPVD.iIdParametroDinamico = 66029 --Campo agrupador de la tabla
	--And CEO.iIdGralObjeto = @iIdGralObjeto

	Select @vchNombreCampoAgrupador = CEO.vchNombre
	From BDControlVehicular_TEST..CatEspObjetoTipoValor TPVD, BDControlVehicular_TEST..CatEspObjeto CEO
	Where TPVD.iIdEspObjeto = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	And TPVD.iIdTipoValor = 40421 --Campo agrupador de la tabla
	And CEO.iIdGralObjeto = @iIdGralObjeto

	--Set @vchNombreCampoAgrupador = 'iAgrupador'

	Select @vchNombre2doCampoAgrupador = CEO.vchNombre
	From BDControlVehicular_TEST..CatEspObjetoTipoValor TPVD, BDControlVehicular_TEST..CatEspObjeto CEO
	Where TPVD.iIdEspObjeto = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	And TPVD.iIdTipoValor = 40422
	And CEO.iIdGralObjeto = @iIdGralObjeto
	
	If @vchNombreCampoLlave is null or @vchNombreCampoLlave = ''
		or @vchNombreCampoAgrupador is null or @vchNombreCampoAgrupador = ''
	Begin
		Print 'Hace falta configurar el campo llave y el agrupador en la tabla CatEspObjetoTipoValor.iIdTipoValor = 40421'
	End		
	
	---Revisar si existen registros en tabla
	Set @nvchParametros = N'@iRegistros int output '     
   
	Set @nvchCommand2 = N'Select @iRegistros = count(*) '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname + N' '
	Set @nvchCommand2 = @nvchCommand2 + N'Where ' + @vchNombreCampoLlave + N' = ' + cast(@objname as varchar)  + N' '
	
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @iRegistros = @iRegistros output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
	Begin    
		Goto Fin
	End
	
	--Si el query anterior no arrojo registros
	--Intentar a hacer los querys con el Alias
	If @iRegistros = 0
	Begin
		Set @vchNombreObjname = @vchAliasObjeto
	End
	
	Set @vchCommand = 'Select * From ' + @vchNombreBaseDatos 
	--Si el nombre de la tabla, ya incluye el esquema
	--no concatenar nuevamente el esquema
	if @vchNombreObjname like '%.%' 
	Begin
		Set @vchCommand = @vchCommand + '.' + @vchNombreObjname 
	End
	Else
	Begin
		Set @vchCommand = @vchCommand + '.' + @vchNombreSquema + '.' + @vchNombreObjname 
	End
	
	Set @vchCommand = @vchCommand + ' Where ' + @vchNombreCampoLlave + ' = ' + @objname + ' '
	
	--Select 'debug' as dbg, @vchCommand as vchCommand, @vchNombreBaseDatos as vchNombreBaseDatos
	--Select 'debug' as dbg, @vchNombreSquema as vchNombreSquema, @vchNombreObjname as vchNombreObjname
	--Obtener el agrupador
	If @vchNombreCampoAgrupador is not null and @vchNombreCampoAgrupador <> ''
	Begin
		Set @nvchParametros = N'@siAgrupador int output '     
   
		Set @nvchCommand2 = N'Select @siAgrupador = ' + @vchNombreCampoAgrupador + N' '
		Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos 
		--Si el nombre de la tabla, ya incluye el esquema
		--no concatenar nuevamente el esquema
		If @vchNombreObjname like '%.%'
		Begin
			Set @nvchCommand2 = @nvchCommand2 + N'.' + @vchNombreObjname + N' '
		End
		Else
		Begin
			Set @nvchCommand2 = @nvchCommand2 + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname + N' '
		End
		
		Set @nvchCommand2 = @nvchCommand2 + N'Where ' + @vchNombreCampoLlave + N' = ' + cast(@objname as varchar)  + N' '
		
		Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @siAgrupador = @siAgrupador output
		Set @iErrorSql = @@Error    
		If @iErrorSql > 0     
		Begin    
			Set @iError = @iErrorSql    
			Goto Fin
		End    
		If @iError <> 0     
			Begin    
			Goto Fin
		End    
	End
	
	If not (@siAgrupador is null)
	Begin
		Set @vchCommand = @vchCommand + @nvchEnter + 'Select * From ' + @vchNombreBaseDatos 
		If @vchNombreObjname like '%.%' --El nombre del objeto incluye el esquema
		Begin
			Set @vchCommand = @vchCommand + N'.' + @vchNombreObjname
		End
		Else
		Begin
			Set @vchCommand = @vchCommand + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname
		End
		Set @vchCommand = @vchCommand + N' Where ' + @vchNombreCampoAgrupador + N' = ' + Cast(@siAgrupador as varchar) + N' '		
	End

	If @vchNombre2doCampoAgrupador is not null and @vchNombre2doCampoAgrupador <> ''
	Begin
		if(@objname <> @siAgrupador )
		BEGIN
			DECLARE @vchCommand2Do VARCHAR(MAX)
			Set @vchCommand2Do =  'Select * From ' + @vchNombreBaseDatos 
			If @vchNombreObjname like '%.%' --El nombre del objeto incluye el esquema
			Begin
				Set @vchCommand2Do = @vchCommand2Do + N'.' + @vchNombreObjname
			End
			Else
			Begin
				Set @vchCommand2Do = @vchCommand2Do + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname
			End
			Set @vchCommand2Do = @vchCommand2Do + N' Where ' + @vchNombreCampoAgrupador  + N' = ' + cast(@objname as varchar)  + N' '	

			Set @vchCommand = @vchCommand2Do + @nvchEnter+ @vchCommand
		END
	End

End

--Revisar si el nombre correspone a una base de datos
If exists ( Select * From master..sysdatabases Where name = @vchNombreObjname )
Begin
	Set @vchCommand = 'Use ' + @vchNombreObjname
End

If @filtro <> ''		--Se proporcion� un filtro
Begin
	if @campo = ''	--pero No se indic� n�mero ni nombre de campo
	Begin
		--Adquiero el primer nombre de campo con llave primaria (Si y s�lo si Si es tabla):
		If @sysobj_type in ('S ','U ')		-- Tabla
		Begin
			--Tomo el primer campo de la llave principal			
			If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
			Begin
				Insert into @helpIndex
				Exec ('sp_helpindex ''' + @vchNombreObjname + '''')
			End
			Else
			Begin
				Insert into @helpIndex
				Exec ('sp_helpindex ''' + @vchNombreBaseDatos + '..' + @vchNombreObjname + '''')
			End
			-- Adquiero el primer campo de la llave primaria (si existe)
			Select @columna = Case CharIndex(',', index_keys) When 0 Then index_keys Else Left(index_keys, CharIndex(',', index_keys) - 1) End
			From @helpIndex
			Where index_name like 'PK_%'

			If @columna IS NULL	-- Si no existe, tomo el primer campo de la tabla
			Begin
				If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
				Begin
					-- Tomo el nombre del campo n�mero 1 de la tabla @objname
					Select @columna = c.name
					From SysObjects o, SysColumns c
					Where o.id = object_id(@objname)
					And o.id = c.id
					And c.colid = 1
				End
				Else--Si se esta enviando la base de datos
				Begin
					Set @vchQuery = 'Select c.name ' + char(13)
					Set @vchQuery = @vchQuery + 'From ' + @vchNombreBaseDatos + '..' + 'SysObjects o, ' + @vchNombreBaseDatos + '..' + 'SysColumns c ' + char(13)
					Set @vchQuery = @vchQuery + 'Where o.id = object_id(' + char(39) + @vchNombreBaseDatos + '..' + @vchNombreObjname + char(39) + ') ' + char(13)
					Set @vchQuery = @vchQuery + 'And o.id = c.id ' + char(13)
					Set @vchQuery = @vchQuery + 'And c.colid = 1 ' + char(13)
					
					Insert into @tmpColumna ( vchColumna )
					exec ( @vchQuery )
					If exists ( Select * From @tmpColumna )
					Begin
						Select @columna = vchColumna From @tmpColumna
					End
				End
			End
		End
		Else If @sysobj_type in ('V ')		-- Vista (no hay llave primaria)
		Begin
			If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
			Begin
				-- Tomo el nombre del campo n�mero 1 de la tabla @objname
				Select @columna = c.name
				From SysObjects o, SysColumns c
				Where o.id = object_id(@objname)
				And o.id = c.id
				And c.colid = 1
			End
			Else
			Begin
				Set @vchQuery = 'Select c.name ' + char(13)
				Set @vchQuery = @vchQuery + 'From ' + @vchNombreBaseDatos + '..' + 'SysObjects o, ' + @vchNombreBaseDatos + '..' + 'SysColumns c ' + char(13)
				Set @vchQuery = @vchQuery + 'Where o.id = object_id(' + char(39) + @vchNombreBaseDatos + '..' + @vchNombreObjname + char(39) + ') ' + char(13)
				Set @vchQuery = @vchQuery + 'And o.id = c.id ' + char(13)
				Set @vchQuery = @vchQuery + 'And c.colid = 1 ' + char(13)
				
				Insert into @tmpColumna ( vchColumna )
				exec ( @vchQuery )
				If exists ( Select * From @tmpColumna )
				Begin
					Select @columna = vchColumna From @tmpColumna
				End
			End
		End
	End	
	Else	-- Se indic� un campo para filtrar
	Begin
		-- Comprobar si existe columna
		If IsNumeric(@campo) = 1			-- Se indic� NUMERO de campo
			-- Necesito el nombre del campo n�mero @campo
			Select @columna = c.name
			From SysObjects o, SysColumns c
			Where o.id = object_id(@objname)
			And o.id = c.id
			And c.colid = Convert(SmallInt, @campo)
		Else							-- Se indic� Nombre de campo
			Set @columna = @campo
	End
	Set @vchCommand = @vchCommand + 'Where ' + @columna + ' '
	If IsNumeric(@filtro) = 0				-- No es un n�mero, poner LIKE comillas
		Set @vchCommand = @vchCommand + 'LIKE ''%' + @filtro + '%'' '
	Else
		Set @vchCommand = @vchCommand + '= ''' + @filtro + ''' ' 
End

Fin:
Print @vchCommand + char(13) + char(13)

If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
	Set nocount off  
End
EXECUTE (@vchCommand)


GO

--[/RConstante 30/Abril/2020 se agrega filtro para agrupador de INGRES para el Cotrol + 4]

--[RCONSTANTE 15/Octubre/2020 correccion manejo de transacciones]
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante
** Revisi�n:				5
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''<%NomSP%>'' ) '
	Print '			DROP PROCEDURE <%NomSP%>'
	Print '	GO'	
	Print 'CREATE PROCEDURE <%NomSP%> (
			@bResultado BIT OUTPUT,
			@vchMensaje VARCHAR(MAX) OUTPUT
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @ID INT'
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(Max)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print '	IF @@TRANCOUNT > 0 '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	IF(<ERROR>)'
	Print '	BEGIN'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)'
	Print '		SELECT 0,<ERROR>'
	Print '		GOTO _RollBack'
	Print '	END'
	Print '	'
	Print '	'
	Print 'INSERT INTO @Respuesta (bResultado,vchMensaje)'
	Print 'SELECT 1,'''' '
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF @@TRANCOUNT = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION <%NomTran%>;  	'
	Print ''
	Print '_FinTran:	'
	Print '		IF @@TRANCOUNT = 0'
	Print '			COMMIT TRANSACTION;  	'
	Print '	'
	Print '	'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '	SELECT 0,''El siguiente error no dejo terminar la transaccion: ''+ ERROR_MESSAGE()'
	Print '	'
    Print '	'
    Print '	'
	Print '	IF @@TRANCOUNT = 0'
	Print '		ROLLBACK TRANSACTION'
	Print ' ELSE'
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION <%NomTran%>;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print '		SELECT bResultado,vchMensaje'
	Print '		From @Respuesta'
	Print 'GO'
End
GO

--[RCONSTANTE 15/Octubre/2020 correccion manejo de transacciones]



--[RCONSTANTE 15/Octubre/2020 correccion manejo de transacciones]
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante
** Revisi�n:				5
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''<%NomSP%>'' ) '
	Print '			DROP PROCEDURE <%NomSP%>'
	Print '	GO'	
	Print 'CREATE PROCEDURE <%NomSP%> (
			@bResultado BIT OUTPUT,
			@vchMensaje VARCHAR(MAX) OUTPUT
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @trancount INT, @ID INT'
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(Max)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print 'SET @trancount = @@TRANCOUNT'	
	Print '	IF @trancount > 0 '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	IF(<ERROR>)'
	Print '	BEGIN'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)'
	Print '		SELECT 0,<ERROR>'
	Print '		GOTO _RollBack'
	Print '	END'
	Print '	'
	Print '	'
	Print 'INSERT INTO @Respuesta (bResultado,vchMensaje)'
	Print 'SELECT 1,'''' '
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF @trancount = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION <%NomTran%>;  	'
	Print '	Goto _Fin'
	Print '_FinTran:	'
	Print '		IF @trancount = 0'
	Print '			COMMIT TRANSACTION;  	'
	Print '	'
	Print '	'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '	SELECT 0,''El siguiente error no dejo terminar la transaccion: ''+ ERROR_MESSAGE()'
	Print '	'
    Print '	'
    Print '	'
	Print '	IF @trancount = 0'
	Print '		ROLLBACK TRANSACTION'
	Print ' ELSE'
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION <%NomTran%>;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print '		SELECT bResultado,vchMensaje'
	Print '		From @Respuesta'
	Print 'GO'
End
GO

--[RCONSTANTE 15/Octubre/2020 correccion manejo de transacciones]

--[RCONSTANTE 15/Octubre/2020 correccion manejo de errores]
USE MASTER
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020,16/Dic/2020,21/Dic/2020
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante,RConstante,RConstante
** Revisi�n:				7
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''<%NomSP%>'' ) '
	Print '		DROP PROCEDURE <%NomSP%>'
	Print 'GO'	
	Print 'CREATE PROCEDURE <%NomSP%> (
			@iIdUsuario INT,
			@bRetornarMensaje BIT = 1
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		'  --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @trancount INT = -1, @ID INT, @iErrorBase INT = ??? , @iError INT = 0'
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(Max)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print '	SET @trancount = @@TRANCOUNT'	
	Print '	IF @trancount > 0 '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	IF(<ERROR>)'
	Print '	BEGIN'
	Print '		SET @iError = @iErrorBase + 1 '
	Print '		GOTO _RollBack'
	Print '	END'
	Print '	'
	Print '	'
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF @trancount = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF  @trancount <> -1 AND XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION <%NomTran%>;  	'
	Print '	Goto _Fin'
	Print '_FinTran:	'
	Print '		IF @trancount = 0'
	Print '			COMMIT TRANSACTION;  	'
	Print '	'
	Print '	'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	SET @iError = @iErrorBase	'
	Print '	PRINT ''<%NomSP%>''+ ERROR_MESSAGE()'
	Print '	'
	Print '	IF @trancount = 0'
	Print '		ROLLBACK TRANSACTION'
	Print '	ELSE IF @trancount <> -1 '
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION <%NomTran%>;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print 'IF(@bRetornarMensaje = 1)'
	Print 'BEGIN	'
	Print '	IF(@iError > 0 )	'
	Print '		BEGIN'
	Print '			INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '			EXEC prcRetornaMensajesBD @iError,@iIdUsuario	'
	Print '		END	'
	Print '	ELSE	'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		SELECT 1, ''''	'
	Print ''
	Print '	SELECT bResultado,vchMensaje, @iError AS iError'
	Print '	From @Respuesta'
	Print 'END'
	Print 'ELSE'
	Print '	RETURN	@iError'
	Print ''
	Print 'GO'
	Print ''
	Print ''
	Print 'IF NOT EXISTS (SELECT * FROM CatGralObjeto WHERE vchNombre = ''<%NomSP%>'')'
	Print '	EXEC prcInsertaRegistroNvoObjetoBD '
	Print '	@vchNombre = ''<%NomSP%>'','
	Print '	@vchColumnPrimary = '''','
	Print '	@vchComentario = '''','
	Print '	@iIdTipoObjeto = 40002,'
	Print '	@bControlarRango = 0,'
	Print '	@iCantidadRegistrosEnRango = -1,'
	Print '	@iValorInicial = -1,'
	Print '	@ivalorFinal = -1,'
	Print '	@bEstablecerIdentity = 0,'
	Print '	@iIdTipoBitacora = 40026'
	Print 'GO'
	Print ''
	Print 'IF NOT EXISTS (SELECT * FROM CatEspObjetoMensajes WHERE iIdObjetoMensaje = ???)'
	Print '	INSERT INTO CatEspObjetoMensajes (iIdObjetoMensaje,iIdGralObjeto,vchMensaje,bRestrictivo)'
	Print '	SELECT ???,iIdGralObjeto,  ''Error no controlado en base de datos.'',1'
	Print '	FROM CatGralObjeto'
	Print '	WHERE vchNombre = ''<%NomSP%>'''
	Print 'GO'
	Print ''
	Print ''
End

GO
--[/RCONSTANTE 15/Octubre/2020 correccion manejo de errores]
--[RConstante 03/Marzo/2021 Correccion errores personalizados previos a rollback]
USE MASTER
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020,16/Dic/2020,21/Dic/2020
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante,RConstante,RConstante
** Revisi�n:				7
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'IF NOT EXISTS (SELECT * FROM CatGralObjeto WHERE vchNombre = ''<%NomSP%>'')'
	Print '	EXEC prcInsertaRegistroNvoObjetoBD '
	Print '	@vchNombre = ''<%NomSP%>'','
	Print '	@vchColumnPrimary = '''','
	Print '	@vchComentario = '''','
	Print '	@iIdTipoObjeto = 40002,'
	Print '	@bControlarRango = 0,'
	Print '	@iCantidadRegistrosEnRango = -1,'
	Print '	@iValorInicial = -1,'
	Print '	@ivalorFinal = -1,'
	Print '	@bEstablecerIdentity = 0,'
	Print '	@iIdTipoBitacora = 40026'
	Print 'GO'
	Print ''
	Print 'IF NOT EXISTS (SELECT * FROM CatEspObjetoMensajes WHERE iIdObjetoMensaje = ???)'
	Print '	INSERT INTO CatEspObjetoMensajes (iIdObjetoMensaje,iIdGralObjeto,vchMensaje,bRestrictivo)'
	Print '	SELECT ???,iIdGralObjeto,  ''Error no controlado en base de datos.'',1'
	Print '	FROM CatGralObjeto'
	Print '	WHERE vchNombre = ''<%NomSP%>'''
	Print 'GO'
	Print ''
	Print ''
	Print ''
	Print ''
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''<%NomSP%>'' ) '
	Print '		DROP PROCEDURE <%NomSP%>'
	Print 'GO'	
	Print 'CREATE PROCEDURE <%NomSP%> (
			@iIdUsuario INT,
			@bRetornarMensaje BIT = 1
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					<%NomSP%>' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		'  --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @trancount INT = -1, @iErrorBase INT = ??? , @iError INT = 0'
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(500)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print '	SET @trancount = @@TRANCOUNT'	
	Print '	IF @trancount > 0 '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	IF(<ERROR>)'
	Print '	BEGIN'
	Print '		SET @iError = @iErrorBase + 1 '
	Print '		GOTO _RollBack'
	Print '	END'
	Print '	'
	Print '	'
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF (@iError<> 0 ) AND NOT EXISTS (SELECT * FROM CatEspObjetoMensajes WHERE iIdObjetoMensaje = @iError)'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		EXEC prcRetornaMensajesBD @iError,@iIdUsuario	'
	Print '	'
	Print '	IF @trancount = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF  @trancount <> -1 AND XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION <%NomTran%>;  	'
	Print '	'
	Print '	IF EXISTS (SELECT * FROM @Respuesta)	'
	Print '	BEGIN	'
	Print '		DELETE @identity	'
	Print '		INSERT INTO tmpObjetoMensajes	'
	Print '		(	'
	Print '			vchMensaje,bRestrictivo,iIdUsuario,	'
	Print '			dtFechaInsercion	'
	Print '		)	'
	Print '		OUTPUT      INSERTED.iIdMensaje	'
	Print '		INTO @identity	'
	Print '		SELECT vchMensaje,bResultado,@iIdUsuario,	'
	Print '				GETDATE()	'
	Print '		FROM @Respuesta 	'
	Print '		'
	Print '		SELECT @iError = ID	'
	Print '		FROM   @identity		'	
	Print '		DELETE @Respuesta	'
	Print '	END 	'
	Print '	'
	Print '	Goto _Fin'
	Print '_FinTran:	'
	Print '		IF @trancount = 0'
	Print '			COMMIT TRANSACTION;  	'	
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	SET @iError = @iErrorBase	'
	Print '	PRINT ''<%NomSP%>: ''+ ERROR_MESSAGE()'
	Print '	'
	Print '	IF @trancount = 0'
	Print '		ROLLBACK TRANSACTION'
	Print '	ELSE IF @trancount <> -1 '
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION <%NomTran%>;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print 'IF(@bRetornarMensaje = 1)'
	Print 'BEGIN	'
	Print '	IF(@iError > 0 )	'
	Print '		BEGIN'
	Print '			INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '			EXEC prcRetornaMensajesBD @iError,@iIdUsuario	'
	Print '		END	'
	Print '	ELSE	'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		SELECT 1, ''''	'
	Print ''
	Print '	SELECT bResultado,vchMensaje, @iError AS iError'
	Print '	From @Respuesta'
	Print 'END'
	Print 'ELSE'
	Print '	RETURN	@iError'
	Print ''
	Print 'GO'	
End

GO
--[/RConstante 03/Marzo/2021 Correccion errores personalizados previos a rollback]
--[RConstante 03/Marzo/2021 Correccion errores personalizados previos a rollback]
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020,16/Oct/2020,16/Dic/2020,21/Dic/2020,31/Marzo/2021
** Autor modificaci�n: 		RConstante,RConstante,RConstante,RConstante,RConstante,RConstante,RConstante
** Revisi�n:				8
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'IF NOT EXISTS (SELECT * FROM CatGralObjeto WHERE vchNombre = ''<%NomSP%>'')'
	Print '	EXEC prcInsertaRegistroNvoObjetoBD '
	Print '	@vchNombre = ''<%NomSP%>'','
	Print '	@vchColumnPrimary = '''','
	Print '	@vchComentario = '''','
	Print '	@iIdTipoObjeto = 40002,'
	Print '	@bControlarRango = 0,'
	Print '	@iCantidadRegistrosEnRango = -1,'
	Print '	@iValorInicial = -1,'
	Print '	@ivalorFinal = -1,'
	Print '	@bEstablecerIdentity = 0,'
	Print '	@iIdTipoBitacora = 40026'
	Print 'GO'
	Print ''
	Print 'IF NOT EXISTS (SELECT * FROM CatEspObjetoMensajes WHERE iIdObjetoMensaje = ???)'
	Print '	INSERT INTO CatEspObjetoMensajes (iIdObjetoMensaje,iIdGralObjeto,vchMensaje,bRestrictivo)'
	Print '	SELECT ???,iIdGralObjeto,  ''Error no controlado en base de datos.'',1'
	Print '	FROM CatGralObjeto'
	Print '	WHERE vchNombre = ''<%NomSP%>'''
	Print 'GO'
	Print ''
	Print ''
	Print ''
	Print ''
	Print 'IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = ''<%NomSP%>'' ) '
	Print '		DROP PROCEDURE <%NomSP%>'
	Print 'GO'	
	Print 'CREATE PROCEDURE <%NomSP%> (
			@iIdUsuario INT,
			@bRetornarMensaje BIT = 1
			)'	
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					<%NomSP%>' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + FORMAT(GETDATE() , 'dd/MM/yyyy')--+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		'  --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @trancount INT = -1, @iErrorBase INT = ??? , @iError INT = 0'
	Print 'DECLARE @identity AS TABLE (ID INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(500)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'BEGIN TRY'	 
	Print '	SET @trancount = @@TRANCOUNT'	
	Print '	IF @trancount > 0 '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	ELSE '
	Print '	  BEGIN TRANSACTION '	
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 ID '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print '	'
	Print '	IF(<ERROR>)'
	Print '	BEGIN'
	Print '		SET @iError = @iErrorBase + 1 '
	Print '		GOTO _RollBack'
	Print '	END'
	Print '	'
	Print '	'
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	IF (@iError<> 0 ) AND NOT EXISTS (SELECT * FROM CatEspObjetoMensajes WHERE iIdObjetoMensaje = @iError)'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		EXEC prcRetornaMensajesBD @iError,@iIdUsuario	'
	Print '	'
	Print '	IF @trancount = 0'
	Print '	 ROLLBACK TRANSACTION;'
	Print '	Else '
	Print '	 IF  @trancount <> -1 AND XACT_STATE() <> -1'
	Print '		ROLLBACK TRANSACTION <%NomTran%>;  	'
	Print '	'
	Print '	IF EXISTS (SELECT * FROM @Respuesta)	'
	Print '	BEGIN	'
	Print '		DELETE @identity	'
	Print '		INSERT INTO tmpObjetoMensajes	'
	Print '		(	'
	Print '			vchMensaje,bRestrictivo,iIdUsuario,	'
	Print '			dtFechaInsercion	'
	Print '		)	'
	Print '		OUTPUT      INSERTED.iIdMensaje	'
	Print '		INTO @identity	'
	Print '		SELECT vchMensaje,bResultado,@iIdUsuario,	'
	Print '				GETDATE()	'
	Print '		FROM @Respuesta 	'
	Print '		'
	Print '		SELECT @iError = ID	'
	Print '		FROM   @identity		'	
	Print '		DELETE @Respuesta	'
	Print '	END 	'
	Print '	'
	Print '	Goto _Fin'
	Print '_FinTran:	'
	Print '		IF @trancount = 0'
	Print '			COMMIT TRANSACTION;  	'	
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	SET @iError = @iErrorBase	'
	Print '	PRINT Concat(''<%NomSP%>: '', ERROR_MESSAGE(),'' '', ERROR_PROCEDURE(),'' '', ERROR_LINE() )'
	Print '	'
	Print '	IF @trancount = 0'
	Print '		ROLLBACK TRANSACTION'
	Print '	ELSE IF @trancount <> -1 '
	Print '		IF XACT_STATE() <> -1'
	Print '			ROLLBACK TRANSACTION <%NomTran%>;'
    Print ''
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print 'IF(@bRetornarMensaje = 1)'
	Print 'BEGIN	'
	Print '	IF(@iError > 0 )	'
	Print '		BEGIN'
	Print '			INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '			EXEC prcRetornaMensajesBD @iError,@iIdUsuario	'
	Print '		END	'
	Print '	ELSE	'
	Print '		INSERT INTO @Respuesta (bResultado,vchMensaje)	'
	Print '		SELECT 1, ''''	'
	Print ''
	Print '	SELECT bResultado,vchMensaje, @iError AS iError'
	Print '	From @Respuesta'
	Print 'END'
	Print 'ELSE'
	Print '	RETURN	@iError'
	Print ''
	Print 'GO'	
End

GO
--[/RConstante 03/Marzo/2021 Correccion errores personalizados previos a rollback]