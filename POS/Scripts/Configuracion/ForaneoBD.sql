
USE POSForaneo

IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'CatCategorias')
BEGIN
	Create Table CatCategorias
	  (
  		iIdCatCategoria INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdCatCategoria
			PRIMARY KEY (iIdCatCategoria),
		vchNombre VARCHAR(50) NOT NULL,
		vchDescripcion VARCHAR(500) NOT NULL,
		iIdCatCategoriaPadre INT NOT NULL,
			Constraint FK_CatCategorias_iIdCatCategoriaPadre Foreign Key (iIdCatCategoria)
			References CatCategorias (iIdCatCategoria),  
		bActivo BIT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_CatCategorias_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'CatProductos')
BEGIN
	Create Table CatProductos
	  (
  		iIdCatProducto INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_CatProductos
			PRIMARY KEY (iIdCatProducto),
		vchNombre VARCHAR(50) NOT NULL,
		vchDescripcion VARCHAR(500) NOT NULL,
		iIdCatCategoria  INT NOT NULL
			Constraint FK_CatProductos_iIdCatCategoria Foreign Key (iIdCatCategoria)
			References CatCategorias (iIdCatCategoria),  
		bActivo BIT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_CatProductos_dtFechaUltModificacion Default (GETDATE())

	  )
END
 GO


IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'EspProductosPrecios')
BEGIN
	Create Table EspProductosPrecios
	  (
  		iIdEspProductosPrecio INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdEspProductosPrecio
			PRIMARY KEY (iIdEspProductosPrecio),
		iIdCatProducto INT NOT NULL,
			Constraint FK_EspProductosPrecios_iIdCatProducto Foreign Key (iIdCatProducto)
			References CatProductos (iIdCatProducto),  
		mPrecio MONEY NOT NULL,
		vchDescripcion VARCHAR(500) NOT NULL,
		bActivo BIT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_EspProductosPrecios_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


	IF NOT EXISTS( SELECT * FROM CatCategorias WHERE iIdCatCategoria = -1)
	BEGIN
		SET IDENTITY_INSERT CatCategorias ON

		INSERT INTO CatCategorias
		(iIdCatCategoria,	vchNombre,	vchDescripcion,	iIdCatCategoriaPadre,	bActivo,	dtFechaUltModificacion)
		VALUES(-1,'','',-1,0,GETDATE())

		SET IDENTITY_INSERT CatCategorias OFF
	END
	GO




IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'CatConstantes')
BEGIN
	Create Table CatConstantes
	  (
  		iIdCatConstante INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdCatConstante
			PRIMARY KEY (iIdCatConstante),
  		iIdSubAgrupador INT NOT NULL,
		vchNombre VARCHAR(50) NOT NULL,
		vchDescripcion VARCHAR(500) NOT NULL,
		bActivo BIT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_CatConstantes_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'GralComanda')
BEGIN
	Create Table GralComanda
	  (
  		iIdGralComanda INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdGralComanda
			PRIMARY KEY (iIdGralComanda),
		dtFechaAlta DATETIME NOT NULL,
		vchNombre VARCHAR(50) NOT NULL,
		vchComentarioGeneral VARCHAR(500) NOT NULL,
		iIdTipoVenta  INT NOT NULL
			Constraint FK_GralComanda_iIdTipoVenta Foreign Key (iIdTipoVenta)
			References CatConstantes (iIdCatConstante),  --1
		iIdEstatus  INT NOT NULL
			Constraint FK_GralComanda_iIdEstatus Foreign Key (iIdEstatus)
			References CatConstantes (iIdCatConstante),  --2
		mTotalCobrado MONEY NOT NULL,
		smDescuento SMALLINT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_GralComanda_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'EspComanda')
BEGIN
	Create Table EspComanda
	  (
  		iIdEspComanda INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdEspComanda
			PRIMARY KEY (iIdEspComanda),
		iIdEstatus  INT NOT NULL
			Constraint FK_EspComanda_iIdEstatus Foreign Key (iIdEstatus)
			References CatConstantes (iIdCatConstante),  --3
		iIdCatProducto  INT NOT NULL
			Constraint FK_EspComanda_iIdCatProducto Foreign Key (iIdCatProducto)
			References CatProductos (iIdCatProducto), 
		iIdEspProductosPrecio  INT NOT NULL
			Constraint FK_EspComanda_iIdEspProductosPrecio Foreign Key (iIdEspProductosPrecio)
			References EspProductosPrecios (iIdEspProductosPrecio),  
		vchDescripcion VARCHAR(500) NOT NULL,
		vchComentario VARCHAR(500) NOT NULL,
		smCantidad SMALLINT NOT NULL,
		smDescuento SMALLINT NOT NULL,
		mTotal MONEY NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_EspComanda_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdSubAgrupador = 1 ) 
BEGIN 
	SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	( 1,1,UPPER('LOCAL'),UPPER('Tipo de venta'), 1,GETDATE()),
	( 2,1,UPPER('LLEVAR'),UPPER('Tipo de venta'), 1,GETDATE()),
	( 3,1,UPPER('VENTA LINEA'),UPPER('Tipo de venta'), 1,GETDATE());

	SET IDENTITY_INSERT CatConstantes OFF

END


IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdSubAgrupador = 2 ) 
BEGIN 
	SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	( 4, 2,UPPER('ORDEN'),UPPER('Estatus general Comanda'), 1,GETDATE()),
	( 5, 2,UPPER('ENTREGADA'),UPPER('Estatus general Comanda'), 1,GETDATE()),
	( 6, 2,UPPER('PAGADA'),UPPER('Estatus general Comanda'), 1,GETDATE()),
	( 7, 2,UPPER('CANCELADA'),UPPER('Estatus general Comanda'), 1,GETDATE())
	SET IDENTITY_INSERT CatConstantes OFF

END


IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdSubAgrupador = 3 ) 
BEGIN 
	
	SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	(8, 3,UPPER('ACTIVO'),UPPER('Estatus platillo en comanda'), 1,GETDATE()),
	(9, 3,UPPER('CANCELADO'),UPPER('Estatus platillo en comanda'), 1,GETDATE()),
	(10, 3,UPPER('DEVELTO'),UPPER('Estatus platillo en comanda'), 1,GETDATE()),
	(11, 3,UPPER('CORTESIA'),UPPER('Estatus platillo en comanda'), 1,GETDATE())
	SET IDENTITY_INSERT CatConstantes OFF

END
GO




IF NOT EXISTS (SELECT * FROM SYSOBJECTS O , SYSCOLUMNS C WHERE O.ID = C.ID AND O.NAME = 'EspProductosPrecios' AND C.NAME = 'bDefault')
BEGIN
	ALTER TABLE EspProductosPrecios
	ADD bDefault BIT NOT NULL
	CONSTRAINT DF_EspProductosPrecios_bDefault DEFAULT(0);
END
GO

IF NOT EXISTS (SELECT * FROM SYSOBJECTS O , SYSCOLUMNS C WHERE O.ID = C.ID AND O.NAME = 'EspComanda' AND C.NAME = 'iIdGralComanda')
BEGIN
	ALTER TABLE EspComanda
	ADD iIdGralComanda INT NOT NULL
	CONSTRAINT FK_EspComanda_iIdGralComanda DEFAULT(-1);
END
GO

IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdSubAgrupador = 4 ) 
BEGIN 
		SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	(12, 4,UPPER('EFECTIVO'),('forma de pago'), 1,GETDATE()),
	(13, 4,UPPER('TARGETA'),('forma de pago'), 1,GETDATE()),
	(14, 4,UPPER('OTRO'),('forma de pago'), 1,GETDATE())

	SET IDENTITY_INSERT CatConstantes OFF

END
GO

/*
UPDATE CatConstantes
SET vchDescripcion = 'Estatus platillo en comanda'
WHERE iIdSubAgrupador = 3

UPDATE CatConstantes
SET vchDescripcion = 'Estatus general Comanda'
WHERE iIdSubAgrupador = 2

UPDATE CatConstantes
SET vchDescripcion = 'Tipo de venta'
WHERE iIdSubAgrupador = 1

GO
*/
--29/11/2021

IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'GralCorteCaja')
BEGIN
	Create Table GralCorteCaja
	  (
  		iIdGralCorteCaja INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdGralCorteCaja
			PRIMARY KEY (iIdGralCorteCaja),
		dtFechaIncicio DATETIME NOT NULL,
		dtFechaFin DATETIME NOT NULL,
		vchDescripcion VARCHAR(500) NOT NULL,
		mTotal MONEY NOT NULL,
		bActivo BIT NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_GralCorteCaja_dtFechaUltModificacion Default (GETDATE())
	)
END
GO


IF NOT EXISTS (SELECT * FROM sysobjects o WHERE o.NAME = 'EspCorteCaja')
BEGIN
	Create Table EspCorteCaja
	  (
  		iIdEspCorteCaja INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_iIdEspCorteCaja
			PRIMARY KEY (iIdEspCorteCaja),
		iIdGralCorteCaja  INT NOT NULL
			Constraint FK_EspCorteCaja_iIdGralCorteCaja Foreign Key (iIdGralCorteCaja)
			References GralCorteCaja (iIdGralCorteCaja),  
		iIdGralComanda  INT NOT NULL
			Constraint FK_EspCorteCaja_iIdGralComanda Foreign Key (iIdGralComanda)
			References GralComanda (iIdGralComanda),  
		iIdTipoPago  INT NOT NULL
			Constraint FK_EspCorteCaja_iIdTipoPago Foreign Key (iIdTipoPago)
			References CatConstantes (iIdCatConstante),  --4
		mTotalRecaudado MONEY NOT NULL,
		dtFechaUltModificacion DATETIME NOT NULL
		  Constraint DF_EspCorteCaja_dtFechaUltModificacion Default (GETDATE())
	)
END
GO



IF NOT EXISTS( SELECT * FROM GralCorteCaja WHERE iIdGralCorteCaja = -1)
BEGIN
	SET IDENTITY_INSERT GralCorteCaja ON

	INSERT INTO GralCorteCaja
	(iIdGralCorteCaja, dtFechaIncicio, dtFechaFin, vchDescripcion, mTotal, bActivo)
	VALUES(-1,'1900-01-01', '1900-01-01', '',0,0)

	SET IDENTITY_INSERT GralCorteCaja OFF
END
GO



IF NOT EXISTS( SELECT * FROM GralCorteCaja WHERE iIdGralCorteCaja >= 0)
BEGIN

	INSERT INTO GralCorteCaja
	( dtFechaIncicio, dtFechaFin, vchDescripcion, mTotal, bActivo)
	VALUES(GETDATE(), '1900-01-01', '',0,1)

END

-- 23/01/2022

IF NOT EXISTS (SELECT * FROM SYSOBJECTS O , SYSCOLUMNS C WHERE O.ID = C.ID AND O.NAME = 'GralComanda' AND C.NAME = 'mSubTotal')
BEGIN
	ALTER TABLE GralComanda
	ADD mSubTotal MONEY  NOT NULL
	CONSTRAINT DF_GralComanda_mSubTotal DEFAULT(0);
END
GO


-- 05/02/2022

IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdSubAgrupador = 5 ) 
BEGIN 
	SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	(15, 5,UPPER('COMANDA'),('Tipo de movimiento'), 1,GETDATE()),
	(16, 5,UPPER('DEPOSITO'),('Tipo de movimiento'), 1,GETDATE()),
	(17, 5,UPPER('RETIRO'),('Tipo de movimiento'), 1,GETDATE())

	SET IDENTITY_INSERT CatConstantes OFF

END
GO




IF NOT EXISTS (SELECT * FROM SYSOBJECTS O , SYSCOLUMNS C WHERE O.ID = C.ID AND O.NAME = 'EspCorteCaja' AND C.NAME IN( 'iIdTipoMovimiento','vchComentario'))
BEGIN
	ALTER TABLE EspCorteCaja
	ADD iIdTipoMovimiento INT NOT NULL
	CONSTRAINT DF_EspCorteCaja_iIdTipoMovimiento DEFAULT(15)
	CONSTRAINT FK_EspCorteCaja_iIdTipoMovimiento FOREIGN KEY (iIdTipoMovimiento)
    References CatConstantes (iIdCatConstante),  
     vchComentario VARCHAR(500) NOT NULL
	CONSTRAINT DF_EspCorteCaja_vchComentario DEFAULT('')
END
GO



IF NOT EXISTS (SELECT * FROM SYSOBJECTS O , SYSCOLUMNS C WHERE O.ID = C.ID AND O.NAME = 'GralCorteCaja' AND C.NAME IN( 'mTotalSistema'))
BEGIN
	ALTER TABLE GralCorteCaja
	ADD mTotalSistema MONEY NOT NULL
	CONSTRAINT DF_GralCorteCaja_mTotalSistema DEFAULT(0)
END
GO


-- 07/02/2022 


IF NOT EXISTS (SELECT * FROM CatConstantes WHERE iIdCatConstante = -1 ) 
BEGIN 
	SET IDENTITY_INSERT CatConstantes ON

	INSERT INTO CatConstantes 
	(iIdCatConstante,iIdSubAgrupador,	vchNombre,	vchDescripcion,	bActivo,	dtFechaUltModificacion)
	VALUES
	(-1, -1,'','', 1,GETDATE())

	SET IDENTITY_INSERT CatConstantes OFF

END
GO

IF NOT EXISTS( SELECT * FROM GralComanda WHERE iIdGralComanda = -1)
BEGIN
	SET IDENTITY_INSERT GralComanda ON

	INSERT INTO GralComanda
		(iIdGralComanda, dtFechaAlta, vchNombre,
		vchComentarioGeneral, iIdTipoVenta, iIdEstatus,
		mTotalCobrado, smDescuento, dtFechaUltModificacion,
		mSubTotal)
	VALUES(-1, '1900-01-01', '',
			'', -1, -1, 0, 0, '1900-01-01', 0 )
	SET IDENTITY_INSERT GralComanda OFF
END
GO



