Use master
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'VpTdCatObjetoBD_SQL')
	DROP VIEW dbo.VpTdCatObjetoBD_SQL
GO
CREATE   view dbo.VpTdCatObjetoBD_SQL
(              
 iIdObjeto, vchNombre, vchNombreSquema, vchType,  
 iIdEsquema  
)              
AS              
/*              
** Nombre:     VpTdCatObjetoBD_SQL              
** Prop�sito:    Retornar el esquema al que pertence el objeto              
** Campos:                   
** Dependencias:                   
** Error Base:                  
** Retorna:                   
**                     
** Fecha creaci�n:       
** Autor creaci�n:                 
** Csd creaci�n:                  
** Fecha modificaci�n:  
** Autor modificaci�n: 
** Compatibilidad:   1.75              
** Revisi�n:     0  
*/              
SELECT  O.object_id, cast( O.name as nvarchar(50)), cast(S.name as nvarchar),           
cast( O.type as nvarchar) COLLATE database_default, --SQL_Latin1_General_CP850_CI_AI,            
case S.name      
when 'dbo' then 155031    
else 155030            
end as iIdEsquema            
FROM sys.objects  O              
INNER JOIN sys.schemas S ON O.schema_id = S.schema_id  
GO
If exists ( Select * From sysobjects Where name = 'sp_ppinCamposIndice' ) 
	Drop function dbo.sp_ppinCamposIndice
GO
Create Function sp_ppinCamposIndice
(
	@vchDbName varchar(128),
	@vchTabla sysname,
	@indid int
)
Returns Varchar(512)
As 
Begin
	-- Funci�n que partir de una tabla y un ID de indice de la misma
	-- devuelve todos los campos involucrados entre par�ntesis y separados por coma
	Declare @ret varchar(512), @nv nvarchar(128), @i int
	Select @ret = '(', @i = 1
	
	Set @nv = index_col(@vchDbName + '..' + @vchTabla, @indid, @i)	
	While not @nv is null
	Begin
		Set @ret = @ret + @nv + ', '

		Set @i = @i + 1
		Set @nv = index_col(@vchDbName + '..' + @vchTabla, @indid, @i)	
	End

	Set @ret = Left(@ret, len(@ret) - 1) + ')'
	return @ret
End 
GO

If exists ( Select * From sysobjects Where name = 'SP_PpInOrderByPrimaryKey' ) 
	Drop procedure dbo.SP_PpInOrderByPrimaryKey
GO

Create Procedure dbo.SP_PpInOrderByPrimaryKey
@vchTabla nvarchar(30),
@vchOrderByPrimaryKey nvarchar(256) OUTPUT
As
/*
** Nombre:					SP_PpInOrderByPrimaryKey		
** Prop�sito:				Retornar el orden by de una tabla, por su llave primaria
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			8/Junio/2012
** Autor creaci�n:			CJFU
** Csd creaci�n:				csd173
** Fecha modificaci�n: 		
** Autor modificaci�n: 		
** Csd modificaci�n:			
** Compatibilidad:			1.75
** Revisi�n:					0
*/

Set nocount on

--Set @vchTabla = 'histdistribucioncuenta'
select @vchOrderByPrimaryKey = master.dbo.sp_ppinCamposIndice (db_name(), @vchTabla, i.indid) 
from sysobjects o, sysindexes i
where o.parent_obj = object_id(@vchTabla)
and o.xtype in ('PK')
and i.id = o.parent_obj
and o.name = i.name

If @vchOrderByPrimaryKey is null
Begin
	Set @vchOrderByPrimaryKey = ''
End

If @vchOrderByPrimaryKey <> ''
Begin	
	Set @vchOrderByPrimaryKey = 'Order by ' + @vchOrderByPrimaryKey
	Set @vchOrderByPrimaryKey = REPLACE(@vchOrderByPrimaryKey,'(','')
	Set @vchOrderByPrimaryKey = REPLACE(@vchOrderByPrimaryKey,')',' desc')
	Set @vchOrderByPrimaryKey = REPLACE(@vchOrderByPrimaryKey,',',' desc,')
End
--Select @vchOrderByPrimaryKey as vchOrderByPrimaryKey
If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
 Set nocount off
End
Return(0)
GO

If exists ( Select * From sysobjects Where name = 'SP_SELECT_ALL' ) 
	Drop procedure dbo.SP_SELECT_ALL
GO

CREATE procedure dbo.SP_SELECT_ALL(
@objname varchar(500),
@filtro varchar(50) = '',
@campo varchar(50) = ''
)
as
/*
** Nombre:					SP_SELECT_ALL
** Prop�sito:				Facilitar los selects
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			fcolombo
** Csd creaci�n:				?
** Fecha modificaci�n: 		3/Marzo/2011, 14/Mayo/2012, 8/Junio/2012, 13/Julio/2012, 20/Julio/2012
** Autor modificaci�n: 		CJFU, CJFU, CJFU, CJFU, CJFU
** Csd modificaci�n:			csd173, csd173, csd173, csd173, csd173
** Compatibilidad:			1.75
** Revisi�n:					5
*/
Declare @sysobj_type char(2),
@columna varchar(100)
Declare @vchCommand varchar(700)
Declare @nvchCommand2 nVarchar(512)
Declare @nvchParametros nVarchar(128)
Declare @nvchEnter nvarchar(2)
Declare @vchNombreObjname nvarchar(128)
Declare @vchAliasObjeto nvarchar(128)
Declare @vchNombreSquema	varchar(80)
Declare @iIdBaseDatos int --Select * From bd_sicyproh..CatGralObjeto
Declare @vchNombreBaseDatos varchar(256)
Declare @vchQuery varchar(512)
Declare @siAgrupador int
Declare @iIdIdentificador int
Declare @iIdGralObjeto int
Declare @vchNombreCampoLlave varchar(50)
Declare @vchNombreCampoAgrupador varchar(50)
Declare @iError int
Declare @iErrorSql int
Declare @iRegistros int
Declare @vchOrderByPrimaryKey nvarchar(256)

Set nocount on

Set @iError = 0
Set @vchNombreBaseDatos = ''
Set @vchNombreCampoLlave = ''
Set @vchNombreCampoAgrupador = ''
Set @nvchEnter = char(13) + char(10)

Declare @tmpColumna table 
(
	vchColumna varchar(256) null
)

Declare @helpIndex table 
(
	index_name			sysname collate database_default NOT NULL,
	index_description	varchar(210),
	index_keys			nvarchar(2126) collate database_default NOT NULL
)

If isnumeric(@objname) = 0 --No es un numero
Begin
	If @objname like '%..%'
	Begin		
		Select @vchNombreBaseDatos = substring(@objname, 1, CHARINDEX('.',@objname) - 1)
		Select @vchNombreObjname = substring(@objname, CHARINDEX('.',@objname) + 2, len(@objname))
	End
	Else
	Begin
		Set @vchNombreObjname = @objname
	End
End
Else
Begin
	Set @vchNombreObjname = @objname
End

Select @sysobj_type = xtype
From sysobjects
where id = object_id(@vchNombreObjname)

If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
Begin
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	
	Set @vchCommand = 'Select'
	If @filtro = '' or @filtro is null --No hay un filtro
	Begin --Retornar unicamente 20 registros
		Set @vchCommand = @vchCommand + ' Top 20 * From ' + @vchNombreSquema + '.' + @vchNombreObjname + ' ' + @nvchEnter
		
		--Hacer un order by por la llave primaria de la tabla
		Exec SP_PpInOrderByPrimaryKey
		@vchTabla = @vchNombreObjname,
		@vchOrderByPrimaryKey = @vchOrderByPrimaryKey Output
		
		Set @vchCommand = @vchCommand + @vchOrderByPrimaryKey		
	End
	Else
	Begin
		Set @vchCommand = @vchCommand + ' * From ' + @vchNombreSquema + '.' + @vchNombreObjname + ' '
	End
End
Else
Begin
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + '..VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	
	Set @vchCommand = 'Select'
	If @filtro = '' or @filtro is null --No hay un filtro
	Begin --Retornar unicamente 20 registros
		Set @vchCommand = @vchCommand + ' top 20 * From ' + @vchNombreBaseDatos + '.' + @vchNombreSquema + '.' + @vchNombreObjname + ' ' + @nvchEnter
		
		--Hacer un order by por la llave primaria de la tabla
		Exec SP_PpInOrderByPrimaryKey
		@vchTabla = @vchNombreObjname,
		@vchOrderByPrimaryKey = @vchOrderByPrimaryKey Output
		
		Set @vchCommand = @vchCommand + @vchOrderByPrimaryKey		
	end
	Else
	Begin
		Set @vchCommand = @vchCommand + ' * From ' + @vchNombreBaseDatos + '.' + @vchNombreSquema + '.' + @vchNombreObjname + ' '
	End
End

SET NOCOUNT ON

-- Si s�lo pasan un n�mero este n�mero se busca en las listas
If (@filtro = '' And @campo = '' And IsNumeric(@vchNombreObjname) = 1)
Begin

	Set @iIdIdentificador = cast( @vchNombreObjname as int )
	
	Select Top 1 @iIdGralObjeto = iIdGralObjeto
	From bd_sicyproh..CatEspObjetoRangoIdentificador
	Where iRangoInicial <= @iIdIdentificador
	And iRangoFinal >= @iIdIdentificador

	If @iIdGralObjeto is null or @iIdGralObjeto <= 0
	Begin
		Set @vchCommand = 'El identificar no corresponde a un valor valido (no existe en CatEspObjetoRangoIdentificador)'
		Goto fin
	End
	
	Select @vchNombreObjname = vchNombre,
	@iIdBaseDatos = iIdBaseDatos,
	@vchAliasObjeto = vchAlias
	From Bd_sicyproh..CatGralObjeto
	Where iIdGralObjeto = @iIdGralObjeto
	And iIdTipoObjeto = 5220 --Tabla
	
	If @vchNombreObjname is null or @vchNombreObjname = ''
	Begin
		Set @vchCommand = 'El id del objeto no corresponde a una tabla (CatGralObjeto.iIdGralObjeto--> ' + cast(@iIdGralObjeto as varchar) + ')'
		Goto fin
	End	
	
	--recuperar la base de datos
	--la que este relacionada al objeto
	Select @vchNombreBaseDatos = vchDescripcion
	From bd_sicyproh..sysValorLista
	Where siAgrupador = 73280
	And siConsecutivo = @iIdBaseDatos
	
	If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
	Begin
		Set @vchNombreBaseDatos = 'bd_sicyproh'
	End
		
	Set @nvchParametros = N'@vchNombreSquema varchar(80) output '
    
    --Buscar el esquema al que pertence la tabla
	Set @nvchCommand2 = N'Select Top 1 @vchNombreSquema = vchNombreSquema '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + '..VpTdCatObjetoBD_SQL '
	Set @nvchCommand2 = @nvchCommand2 + N'Where vchNombre = ' + char(39) + @vchNombreObjname + char(39) + N' '
   	
   	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @vchNombreSquema = @vchNombreSquema output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
		Begin    
		Goto Fin
	End    			
	--Select 'debug' as dbg, @nvchCommand2 as nvchCommand2, @vchNombreSquema as vchNombreSquema
	
	If @vchNombreSquema is null or @vchNombreSquema = ''
	Begin
		Set @vchNombreSquema = 'dbo'
	End
	

	--Buscar el nombre del campo que es el campo llave o
	--Por el campo que se haria la busqueda
	Select @vchNombreCampoLlave = CEO.vchNombre
	From Bd_sicyproh..TranParametroValorDinamicoObj TPVD, Bd_sicyproh..CatEspObjeto CEO
	Where TPVD.iIdEspDicDatos = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	And TPVD.iIdParametroDinamico = 66020 --Campo llave de la tabla
	And CEO.iIdGralObjeto = @iIdGralObjeto
	
	
	--Buscar el nombre del campo agrupador
	--Para ejecutar la segunda consulta, por el agrupador
	Select @vchNombreCampoAgrupador = CEO.vchNombre
	From Bd_sicyproh..TranParametroValorDinamicoObj TPVD, Bd_sicyproh..CatEspObjeto CEO
	Where TPVD.iIdEspDicDatos = CEO.iIdEspObjeto --siconsecutivo Select * From CatEspObjeto Where iIdEspObjeto = 11316	
	And TPVD.iIdParametroDinamico = 66029 --Campo agrupador de la tabla
	And CEO.iIdGralObjeto = @iIdGralObjeto
	
	If @vchNombreCampoLlave is null or @vchNombreCampoLlave = ''
		or @vchNombreCampoAgrupador is null or @vchNombreCampoAgrupador = ''
	Begin
		Print 'Hace falta configurar el campo llave y el agrupador en la tabla Bd_sicyproh..TranParametroValorDinamicoObj'
	End		
	
	---Revisar si existen registros en tabla
	Set @nvchParametros = N'@iRegistros int output '     
   
	Set @nvchCommand2 = N'Select @iRegistros = count(*) '
	Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname + N' '
	Set @nvchCommand2 = @nvchCommand2 + N'Where ' + @vchNombreCampoLlave + N' = ' + cast(@objname as varchar)  + N' '
	
	Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @iRegistros = @iRegistros output
	Set @iErrorSql = @@Error    
	If @iErrorSql > 0     
	Begin    
		Set @iError = @iErrorSql    
		Goto Fin
	End    
	If @iError <> 0     
	Begin    
		Goto Fin
	End
	
	--Si el query anterior no arrojo registros
	--Intentar a hacer los querys con el Alias
	If @iRegistros = 0
	Begin
		Set @vchNombreObjname = @vchAliasObjeto
	End
	
	Set @vchCommand = 'Select * From ' + @vchNombreBaseDatos 
	--Si el nombre de la tabla, ya incluye el esquema
	--no concatenar nuevamente el esquema
	if @vchNombreObjname like '%.%' 
	Begin
		Set @vchCommand = @vchCommand + '.' + @vchNombreObjname 
	End
	Else
	Begin
		Set @vchCommand = @vchCommand + '.' + @vchNombreSquema + '.' + @vchNombreObjname 
	End
	
	Set @vchCommand = @vchCommand + ' Where ' + @vchNombreCampoLlave + ' = ' + @objname + ' '
	
	--Select 'debug' as dbg, @vchCommand as vchCommand, @vchNombreBaseDatos as vchNombreBaseDatos
	--Select 'debug' as dbg, @vchNombreSquema as vchNombreSquema, @vchNombreObjname as vchNombreObjname
	--Obtener el agrupador
	If @vchNombreCampoAgrupador is not null and @vchNombreCampoAgrupador <> ''
	Begin
	--ppadUnificaPaciente
			
    
		Set @nvchParametros = N'@siAgrupador int output '     
   
		Set @nvchCommand2 = N'Select @siAgrupador = ' + @vchNombreCampoAgrupador + N' '
		Set @nvchCommand2 = @nvchCommand2 + N'From ' + @vchNombreBaseDatos 
		--Si el nombre de la tabla, ya incluye el esquema
		--no concatenar nuevamente el esquema
		If @vchNombreObjname like '%.%'
		Begin
			Set @nvchCommand2 = @nvchCommand2 + N'.' + @vchNombreObjname + N' '
		End
		Else
		Begin
			Set @nvchCommand2 = @nvchCommand2 + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname + N' '
		End
		
		Set @nvchCommand2 = @nvchCommand2 + N'Where ' + @vchNombreCampoLlave + N' = ' + cast(@objname as varchar)  + N' '
		
		Exec @iError = sp_executesql @nvchCommand2, @nvchParametros, @siAgrupador = @siAgrupador output
		Set @iErrorSql = @@Error    
		If @iErrorSql > 0     
		Begin    
			Set @iError = @iErrorSql    
			Goto Fin
		End    
		If @iError <> 0     
			Begin    
			Goto Fin
		End    
	End
	
	If not (@siAgrupador is null)
	Begin
		Set @vchCommand = @vchCommand + @nvchEnter + 'Select * From ' + @vchNombreBaseDatos 
		If @vchNombreObjname like '%.%' --El nombre del objeto incluye el esquema
		Begin
			Set @vchCommand = @vchCommand + N'.' + @vchNombreObjname
		End
		Else
		Begin
			Set @vchCommand = @vchCommand + N'.' + @vchNombreSquema + N'.' + @vchNombreObjname
		End
		Set @vchCommand = @vchCommand + N' Where ' + @vchNombreCampoAgrupador + N' = ' + Cast(@siAgrupador as varchar) + N' '		
	End

End

--Revisar si el nombre correspone a una base de datos
If exists ( Select * From master..sysdatabases Where name = @vchNombreObjname )
Begin
	Set @vchCommand = 'Use ' + @vchNombreObjname
End

If @filtro <> ''		--Se proporcion� un filtro
Begin
	if @campo = ''	--pero No se indic� n�mero ni nombre de campo
	Begin
		--Adquiero el primer nombre de campo con llave primaria (Si y s�lo si Si es tabla):
		If @sysobj_type in ('S ','U ')		-- Tabla
		Begin
			--Tomo el primer campo de la llave principal			
			If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
			Begin
				Insert into @helpIndex
				Exec ('sp_helpindex ''' + @vchNombreObjname + '''')
			End
			Else
			Begin
				Insert into @helpIndex
				Exec ('sp_helpindex ''' + @vchNombreBaseDatos + '..' + @vchNombreObjname + '''')
			End
			-- Adquiero el primer campo de la llave primaria (si existe)
			Select @columna = Case CharIndex(',', index_keys) When 0 Then index_keys Else Left(index_keys, CharIndex(',', index_keys) - 1) End
			From @helpIndex
			Where index_name like 'PK_%'

			If @columna IS NULL	-- Si no existe, tomo el primer campo de la tabla
			Begin
				If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
				Begin
					-- Tomo el nombre del campo n�mero 1 de la tabla @objname
					Select @columna = c.name
					From SysObjects o, SysColumns c
					Where o.id = object_id(@objname)
					And o.id = c.id
					And c.colid = 1
				End
				Else--Si se esta enviando la base de datos
				Begin
					Set @vchQuery = 'Select c.name ' + char(13)
					Set @vchQuery = @vchQuery + 'From ' + @vchNombreBaseDatos + '..' + 'SysObjects o, ' + @vchNombreBaseDatos + '..' + 'SysColumns c ' + char(13)
					Set @vchQuery = @vchQuery + 'Where o.id = object_id(' + char(39) + @vchNombreBaseDatos + '..' + @vchNombreObjname + char(39) + ') ' + char(13)
					Set @vchQuery = @vchQuery + 'And o.id = c.id ' + char(13)
					Set @vchQuery = @vchQuery + 'And c.colid = 1 ' + char(13)
					
					Insert into @tmpColumna ( vchColumna )
					exec ( @vchQuery )
					If exists ( Select * From @tmpColumna )
					Begin
						Select @columna = vchColumna From @tmpColumna
					End
				End
			End
		End
		Else If @sysobj_type in ('V ')		-- Vista (no hay llave primaria)
		Begin
			If @vchNombreBaseDatos is null or @vchNombreBaseDatos = ''
			Begin
				-- Tomo el nombre del campo n�mero 1 de la tabla @objname
				Select @columna = c.name
				From SysObjects o, SysColumns c
				Where o.id = object_id(@objname)
				And o.id = c.id
				And c.colid = 1
			End
			Else
			Begin
				Set @vchQuery = 'Select c.name ' + char(13)
				Set @vchQuery = @vchQuery + 'From ' + @vchNombreBaseDatos + '..' + 'SysObjects o, ' + @vchNombreBaseDatos + '..' + 'SysColumns c ' + char(13)
				Set @vchQuery = @vchQuery + 'Where o.id = object_id(' + char(39) + @vchNombreBaseDatos + '..' + @vchNombreObjname + char(39) + ') ' + char(13)
				Set @vchQuery = @vchQuery + 'And o.id = c.id ' + char(13)
				Set @vchQuery = @vchQuery + 'And c.colid = 1 ' + char(13)
				
				Insert into @tmpColumna ( vchColumna )
				exec ( @vchQuery )
				If exists ( Select * From @tmpColumna )
				Begin
					Select @columna = vchColumna From @tmpColumna
				End
			End
		End
	End	
	Else	-- Se indic� un campo para filtrar
	Begin
		-- Comprobar si existe columna
		If IsNumeric(@campo) = 1			-- Se indic� NUMERO de campo
			-- Necesito el nombre del campo n�mero @campo
			Select @columna = c.name
			From SysObjects o, SysColumns c
			Where o.id = object_id(@objname)
			And o.id = c.id
			And c.colid = Convert(SmallInt, @campo)
		Else							-- Se indic� Nombre de campo
			Set @columna = @campo
	End
	Set @vchCommand = @vchCommand + 'Where ' + @columna + ' '
	If IsNumeric(@filtro) = 0				-- No es un n�mero, poner LIKE comillas
		Set @vchCommand = @vchCommand + 'LIKE ''%' + @filtro + '%'' '
	Else
		Set @vchCommand = @vchCommand + '= ''' + @filtro + ''' ' 
End

Fin:
Print @vchCommand + char(13) + char(13)

If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
	Set nocount off  
End
EXECUTE (@vchCommand)
GO

If exists ( Select * From sysobjects Where name = 'SP_ppinAddDrop_ForeignKeys' ) 
	Drop procedure dbo.SP_ppinAddDrop_ForeignKeys
GO
CREATE Procedure dbo.SP_ppinAddDrop_ForeignKeys (
	@vchTabla varchar(800),
	@tiTipo tinyint = 0
)
AS
/*
** Nombre:					SP_CreateDrop_ForeignKeys
** Prop�sito:				Genera autom�ticamente un script con los alter table (drop o add) de los constraints
**						que hacen referencia (for�nea) a la tabla especificada
** Par�metros:				@vchTabla		Tabla a la que HACEN referencia los constraints
**						@tiTipo		0: Hace el ADD
**									1: Hace el DROP
**
** Dependencias:				
**
** Fecha creaci�n:			23/11/2005
** Autor creaci�n:			FDCG
** csd creaci�n: 				?
** Fecha modificaci�n:	21/Septiembre/2006, 7/Marzo/2011, 13/Septiembre/2011
** Autor modificacion:	CJFU, CJFU, CJFU
** csd modificaci�n:		csd169, c
sd173, csd173
** Compatibilidad:			1.75
** Revision:				3
*/
Set nocount on
Create Table #tempConstraints		--Tabla que contiene todos los IDs de los constraints (FK) que referencian a alg�n campo de @vchTabla
(
	constid int not null,
	tiUsado tinyint not null
)

Create Table #tempColumnas		--Tabla que contiene (una vez por cada constID) las tabla y los campos a los que referencia
(
	TablaDesde sysname not null,
	TablaHasta sysname not null,
	ColumnaDesde sysname not null,
	ColumnaHasta sysname not null,
	tiUsado tinyint not null
)

Declare @nvchEnter nvarchar(2)
Declare @iTotalTablasReferenciadas int
Set @nvchEnter = CHAR(13) + CHAR(10)

--Declare @vchTabla varchar(200)
--Set @vchTabla = 'ACFDEPE1'

Declare @constid int, @vchCreate varchar(4000), @vchDrop varchar(1000)
Declare @TablaF varchar(100), @TablaR varchar(100)
Declare @ColF varchar(100), @ColR varchar(100)
Declare @CamposIzquierda varchar(500), @CamposDerecha varchar(500)

-- Lleno la temporal de constraints 
Insert #tempConstraints
Select r.constid, 0
from sysreferences r, sysobjects o
where o.name = @vchTabla
And rkeyid = o.id
order by 1

If not exists (select * from #tempConstraints)
	If object_id(@vchTabla) Is Null 
		Print 'La tabla [' + @vchTabla + '] no existe en [' + db_name() + ']'
	Else
		Print 'La tabla [' + @vchTabla + '] no tiene referencias for�neas'
Else
	While exists (Select * From #tempConstraints Where tiUsado = 0)
	Begin
		Select Top 1 @constid = constid From #tempConstraints Where tiUsado = 0
	
		Set @CamposIzquierda = ''
		Set @CamposDerecha = ''
		Set @vchCreate = ''
		Set @vchDrop = ''
	
		-- Lleno la temporal de columnas	
		Insert #tempColumnas (TablaDesde, TablaHasta,  ColumnaDesde, ColumnaHasta, tiUsado)
		Select TablaF.name, TablaR.name, ColF.name, ColR.name, 0
		From sysforeignkeys fk, sysobjects ofk, sysobjects TablaF, sysobjects TablaR, 
		syscolumns ColF, syscolumns ColR
		Where ofk.name = object_name(@constid)
		And ofk.id = fk.constid
		And TablaF.id = fk.fkeyid
		And TablaR.id = fk.rkeyid
		And ColF.id = TablaF.id And ColF.colid = fk.fkey
		And ColR.id = TablaR.id And ColR.colid = fk.rkey

--		Select * from #tempColumnas

		While Exists (Select * From #tempColumnas Where tiUsado = 0)
		Begin
			Select Top 1 @ColF = ColumnaDesde, @ColR = ColumnaHasta
			From #tempColumnas
			Where tiUsado = 0
			
			Set @CamposIzquierda = @CamposIzquierda + @ColF + ', '
			Set @CamposDerecha = @CamposDerecha + @ColR + ', '
		
			Update #tempColumnas
			Set tiUsado = 1
			Where ColumnaDesde = @ColF And ColumnaHasta = @ColR
		End	

		Set @CamposIzquierda = LEFT(@CamposIzquierda, LEN(@CamposIzquierda) - 1)
		Set @CamposDerecha = LEFT(@CamposDerecha, LEN(@CamposDerecha) - 1)

		Select Top 1 @TablaF = TablaDesde, @TablaR = TablaHasta From #tempColumnas

		Set @vchCreate = 'If Not Exists (Select * From SysObjects Where Name = ''' + object_name(@constid) + ''') '
		Set @vchCreate = @vchCreate + 'And object_id(''' + @TablaF + ''') is not null ' + @nvchEnter
		Set @vchCreate = @vchCreate + char(9) + 'Alter Table ' + @TablaF + @nvchEnter
		Set @vchCreate = @vchCreate + char(9) + 'Add Constraint ' + object_name(@constid) + ' Foreign Key' 
		Set @vchCreate = @vchCreate + '(' + @CamposIzquierda + ') ' + @nvchEnter
		Set @vchCreate = @vchCreate + char(9) + 'References ' + @TablaR 
		Set @vchCreate = @vchCreate + ' (' + @CamposDerecha + ')' + @nvchEnter
		Set @vchCreate = @vchCreate + 'GO' 

		Set @vchDrop = 'If Exists (Select * From SysObjects Where Name = ''' + object_name(@constid) + ''') ' + @nvchEnter
		Set @vchDrop = @vchDrop + char(9) + 'Alter Table ' + @TablaF + @nvchEnter
		Set @vchDrop = @vchDrop + char(9) + 'Drop Constraint ' + object_name(@constid) + @nvchEnter
		Set @vchDrop = @vchDrop + 'GO'

		If @tiTipo = 1
			Print @vchDrop
		else 
			Print @vchCreate			

		Update #tempConstraints
		Set tiUsado = 1
		Where constid = @constid
	
		Delete #tempColumnas
		Where tiUsado = 1
	End

Set @iTotalTablasReferenciadas = 0
Select @iTotalTablasReferenciadas = count(*)
From #tempConstraints

Print N'La tabla esta referenciada por ' + cast(@iTotalTablasReferenciadas as nvarchar) + ' llaves foraneas.'
If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
	Set nocount off
End
GO

If exists ( Select * From sysobjects Where name = 'SP_ppinGenera_Insert' ) 
	Drop procedure dbo.SP_ppinGenera_Insert
GO

Create Procedure dbo.SP_ppinGenera_Insert 
(
@vchTabla varchar(200),
@vchWhere varchar(8000) = '',
@tiTipo tinyint = 0,
@tiTipoResultado tinyint = 0,
@iCantCam_NvaLinea_CampoInsert int = 0
)
AS
/*
** Nombre:					sp_Genera_Insert
** Prop�sito:				Genera el script que inserta los datos de una tabla
** Par�metros:				@vchTabla	Nombre de la tabla
**						@vchWhere	(opcional) filtro para el where
**						@tiTipo	0: Genera el if exists DELETE->INSERT
**								1: Genera el if not exists INSERT
**								2: Genera el if exists UPDATE else INSERT
**						@tiTipoResultado Tipo de resultado
**								0:	El resultado se arroja con prints, se informa al usuario en caso de errores o llaves primarias
**								1:	El resultado se arrojo con select para que se posible atrapar el resultado en insert a alguna tabla
**										el resultado siempre tendra un unico campo, se eliminan los mensajes informativos
**						@iCantCam_NvaLinea_CampoInsert valor numerico que indica cada cuantos campos
**							se har� un saldo de linea para los campos en el script de insert.
**
** Dependencias:				
**
** Fecha creaci�n:			23/11/05
** Autor creaci�n:			FDCG
** csd creaci�n: 				
** Fecha modificaci�n:		Septiembre 2006, 26/Agosto/2008, 27/Septiembre/2011, 14/Noviembre/2013
** Autor modificacion:		FDCG, CJFU, CJFU, CJFU
** csd modificaci�n:			?, csd172, csd173, csd173
**							
** Compatibilidad:			1.75
** Revisi�n:				4
*/
Declare @tiEsCampoText tinyint
Declare @vchBaseDatos varchar(100)
Declare @vchBaseDatosDosPuntos varchar(100) --Si se concene la base de datos se le concatena dos puntos, si no se conoce debe estar vacia
Declare @vchBaseDatosUnPunto varchar(100)	--Si se concene la base de datos se le concatena un punto, si no se conoce debe estar vacia
Declare @vchBaseDatosConTabla varchar(100)
Declare @vchTemporal varchar(100)
Declare @vchEsquemaTabla varchar(30)
Declare @iPosicionNombreTabla int
Declare @nvchQuery nvarchar(4000)
Declare @nvchParametros nvarchar(512)
Declare @iCantRegistro int
Declare @vchBaseDatosActual varchar(100)
Declare @vchNvaLn char(2)
Declare @vchEnter nvarchar(20)
Declare @iCamposFaltantes int
Declare @iContadorTmp int

Declare @vchReturn varchar(8000), @vchWhere1 varchar(1000), @vchInsert varchar(1000), @vchValues varchar(8000)
Declare @vchCamposComa varchar(1000)

Declare @vchUpdate1 varchar(8000), @vchUpdate2 varchar(8000), @vchTemp varchar(8000)
Declare @esFecha tinyint
Declare @Columna nvarchar(128), @esCadena tinyint, @tiEsTimeStamp tinyint

Set Nocount On

Select @vchBaseDatosActual = db_name()
Select @vchNvaLn = char(13) + char(10)
Set @vchEnter = 'char(13) + char(10)' --Donde se quiere que el script retornado haga enter's

create table #tmpIndex
(
	TABLE_QUALIFIER	sysname NOT NULL,
	TABLE_OWNER		sysname NOT NULL,
	TABLE_NAME		sysname NOT NULL,
	COLUMN_NAME		sysname NOT NULL,
	KEY_SEQ		smallint NOT NULL,
	PK_NAME		sysname  NOT NULL
)

Create Table #Campos 
(
	vchNombre varchar(60) not null,		-- Nombre de la columna
	tiLlavePrimaria tinyint not null,		-- Indica si es llave primaria
	tiEsCadena tinyint not null,			-- Indica si el campo es una cadena (varchar, nvarchar, etc)
	tiUsado tinyint not null,			-- Campo para hacer ciclos while
	tiEsTimeStamp tinyint not null,		-- Indica si es del tipo TimeStamp (para poner la palabra NULL en el values)
	tiAdmiteNulos tinyint not null,		-- Indica si el campo admite nulos
	tiEsFecha tinyint not null,
	tiEsCampoText tinyint not null --Indica que es un campo tipo text, para no utilizar el replace

)

Create table #tmpTexto
(
	vchTexto varchar(8000)
)

--Revisar si en el nombre de la table trae concatenada la base de datos
--Pero no tra el esquema
If @vchTabla like '%..%'
Begin
	Select @vchBaseDatosConTabla= @vchTabla
	Select @iPosicionNombreTabla = PATINDEX('%..%', @vchTabla)

	Set @vchBaseDatosDosPuntos = substring( @vchBaseDatosConTabla, 1 , @iPosicionNombreTabla + 1) --Base de datos con puntos
	Set @vchBaseDatosUnPunto = substring( @vchBaseDatosConTabla, 1 , @iPosicionNombreTabla) 
	Select @vchBaseDatos = substring( @vchBaseDatosConTabla, 1 , @iPosicionNombreTabla - 1) 
	
	Select @vchTabla = substring( @vchBaseDatosConTabla, @iPosicionNombreTabla + 2, 200)
	Set @vchEsquemaTabla = ''
End
Else If @vchTabla like '%.%' --Se envio el esquema y posiblemente la base de datos
Begin
	Set @iPosicionNombreTabla = PATINDEX('%.%', @vchTabla)

	--Revisar si hay un segundo punto
	Set @vchTemporal = substring( @vchTabla, @iPosicionNombreTabla + 1, 100) 

	If PATINDEX('%.%', @vchTemporal) > 0
	Begin
		--Eliminar la tabla
		--dejar base de datos y esquema
		Set @vchTemporal = substring( @vchTabla, 1, @iPosicionNombreTabla ) + substring( @vchTemporal, 1, PATINDEX('%.%', @vchTemporal) -1)
		Set @vchTabla = substring( @vchTabla, LEN(@vchTemporal) + 2, 100)
	End
	Else
	Begin
		--Eliminar la tabla en otra variable temporal
		Set @vchTemporal = substring( @vchTabla, 1, @iPosicionNombreTabla -1)
		Set @vchTabla = substring( @vchTabla, @iPosicionNombreTabla + 1, 100)
	End

	--Revisar si en la temporal hay otro punto
	If PATINDEX('%.%', @vchTemporal) > 0
	Begin
		Set @vchEsquemaTabla = substring( @vchTemporal, PATINDEX('%.%', @vchTemporal) + 1, 30)
		Set @vchBaseDatosDosPuntos = LEFT(@vchTemporal, PATINDEX('%.%', @vchTemporal) -1) + '..'
		Set @vchBaseDatosUnPunto = LEFT(@vchTemporal, PATINDEX('%.%', @vchTemporal) -1) + '.'
		Set @vchBaseDatos = LEFT(@vchTemporal, PATINDEX('%.%', @vchTemporal) - 1)
	End
	Else
	Begin
		Set @vchEsquemaTabla = @vchTemporal
		Set @vchBaseDatosDosPuntos = '' --No se envio la base de datos 
		Set @vchBaseDatosUnPunto = '' --No se envio la base de datos 
		Set @vchBaseDatos = '' --No se envio la base de datos 
	End

	Set @vchBaseDatosConTabla = ''
End
Else
Begin
	Set @vchBaseDatosConTabla = @vchTabla
	Set @vchBaseDatos = ''
	Set @vchBaseDatosDosPuntos = '' --No se envio la base de datos
	Set @vchBaseDatosUnPunto = '' --No se envio la base de datos
	Set @vchEsquemaTabla = ''
End

Set @nvchQuery = N'Select @iCantRegistro = count(*) From ' + @vchBaseDatosDosPuntos + 'sysobjects o ' 
Set @nvchQuery = @nvchQuery + N'Where o.name = ''' + @vchTabla + ''' and xtype = ''' + 'u' + ''''

Set @nvchParametros = N'@iCantRegistro int output ' 
Exec sp_executesql @nvchQuery, @nvchParametros, @iCantRegistro = @iCantRegistro output

If @iCantRegistro <= 0
Begin
	if @tiTipoResultado = 0
	Begin
		Print 'La tabla ' + @vchTabla + ' no existe'
	End
	Goto _Fin
End

--Revisar si no se tiene el nombre del esquema
If @vchEsquemaTabla = '' or @vchEsquemaTabla is null
Begin
	If @vchBaseDatosActual <> @vchBaseDatos And @vchBaseDatos <> ''
	Begin
		Set @nvchQuery = N'Select @vchEsquemaTabla = vchNombreSquema From ' + @vchBaseDatosDosPuntos + 'VpTdCatObjetoBD_SQL o ' 
		Set @nvchQuery = @nvchQuery + N'Where o.vchNombre = ''' + @vchTabla + ''' and vchType = ''' + 'u' + ''''

		Set @nvchParametros = N'@vchEsquemaTabla varchar(30) output ' 
		Exec sp_executesql @nvchQuery, @nvchParametros, @vchEsquemaTabla = @vchEsquemaTabla output				
	End
	Else
	Begin
		Set @nvchQuery = N'Select @vchEsquemaTabla = vchNombreSquema From VpTdCatObjetoBD_SQL o ' 
		Set @nvchQuery = @nvchQuery + N'Where o.vchNombre = ''' + @vchTabla + ''' and vchType = ''' + 'u' + ''''

		Set @nvchParametros = N'@vchEsquemaTabla varchar(30) output ' 
		Exec sp_executesql @nvchQuery, @nvchParametros, @vchEsquemaTabla = @vchEsquemaTabla output
	End
End

If @vchBaseDatosActual <> @vchBaseDatos And @vchBaseDatos <> ''
Begin
	Set @nvchQuery = N'' + @vchBaseDatosDosPuntos
	Set @nvchQuery = @nvchQuery + N'sp_pkeys ''' + @vchTabla + ''','
	Set @nvchQuery = @nvchQuery + @vchEsquemaTabla --enviar el esquema en el que se encuentra la tabla
	
	Insert into #tmpIndex
	Exec (@nvchQuery)
End
Else
Begin
	Set @nvchQuery = N'' 
	Set @nvchQuery = @nvchQuery + N'sp_pkeys ''' + @vchTabla + ''','
	Set @nvchQuery = @nvchQuery + @vchEsquemaTabla --enviar el esquema en el que se encuentra la tabla
	
	Insert into #tmpIndex
	Exec (@nvchQuery) --enviar el esquema en el que se encuentra la tabla
End

--Inserto todos los campos de la tabla en la temporal
Set @nvchQuery = N'Select c.name, 0, ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'Case When c.type IN (35, 47,39 ) Then ' + @vchNvaLn --35->(ntext, text), 47->(char, nchar), 39(nvarchar, varchar, sysname, sql_variant) 
Set @nvchQuery = @nvchQuery + N'	1 Else 0 End, ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'0, Case When c.xtype = 189 Then 1 Else 0 End, ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'c.isnullable, ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'Case When c.type IN (58,61) Then 1 Else 0 End, ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'Case When c.type IN (35 ) Then ' + @vchNvaLn --35->(ntext, text)
Set @nvchQuery = @nvchQuery + N'	1 Else 0 End ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'From ' + @vchBaseDatosDosPuntos + 'sysobjects o, ' + @vchBaseDatosDosPuntos + 'syscolumns c ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'Where o.id = c.id ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'And o.name = ''' + @vchTabla + ''' ' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'And o.type = ''U''' + @vchNvaLn
Set @nvchQuery = @nvchQuery + N'Order by c.colid ' + @vchNvaLn
--Select @nvchQuery as nvchQuery

Insert #Campos (vchNombre, tiLlavePrimaria,
tiEsCadena,
tiUsado, tiEsTimeStamp,
tiAdmiteNulos,
tiEsFecha,
tiEsCampoText )
execute (@nvchQuery)

--Actualizo el valor de tiLlavePrimaria, para los que pertenecen al PK
Update #Campos
Set tiLlavePrimaria = 1
From #tmpIndex tmp
Where tmp.COLUMN_NAME = vchNombre


Select @vchUpdate1 = '', @vchUpdate2 = ''


Set @vchReturn = ''
Set @vchCamposComa = ''

If @iCantCam_NvaLinea_CampoInsert is null or @iCantCam_NvaLinea_CampoInsert = 0
Begin
	Set @iCantCam_NvaLinea_CampoInsert = 4 --Cada cuatro campos
End

If not exists (Select * from #tmpIndex)
-- No hay ninguna llave primaria, las marco a todas como primarias (las que no aceptan nulos y son distintas de timestamp
Begin
	if @tiTipoResultado = 0
	Begin
		Print 'Tabla sin llave primaria, se usar�n como llave primaria todos los campos que no admitan nulos' + @vchNvaLn + @vchNvaLn 
	End
	Update #Campos
	Set tiLlavePrimaria = 1
	Where tiLlavePrimaria = 0 And tiAdmiteNulos = 0 And tiEsTimeStamp = 0
End

-- Muestro la llave primaria
While exists (Select * From #Campos Where tiUsado = 0 And tiLlavePrimaria = 1)
Begin
	Select Top 1 @Columna = vchNombre From #Campos Where tiUsado = 0 And tiLlavePrimaria = 1
	
	Set @vchReturn = @vchReturn + @Columna + ', '

	Update #Campos
	Set tiUsado = 1
	Where vchNombre = @Columna
	And tiLlavePrimaria = 1
End
Set @vchReturn = LEFT(@vchReturn, LEN(@vchReturn) - 1)	--Quito la �ltima coma
if @tiTipoResultado = 0
Begin
	Print 'Llave primaria de la tabla [' + @vchTabla + ']: ' + @vchReturn
End

Set @vchReturn = ''
Update #Campos
Set tiUsado = 0

--Select * From #Campos

----------LLENO EL WHERE
Set @vchWhere1 = 'Where '
While exists (Select * from #Campos where tiLlavePrimaria = 1 And tiUsado = 0 And tiEsTimeStamp = 0)
Begin
	Select Top 1 @Columna = vchNombre, @esCadena = tiEsCadena, @tiEsCampoText = tiEsCampoText
	From #Campos where tiLlavePrimaria = 1 And tiUsado = 0 And tiEsTimeStamp = 0

	Set @vchWhere1 = @vchWhere1 + @Columna + ' = '
	If @esCadena = 1
		-- Hay que poner comillas
		Begin
			--Set @vchWhere1 = @vchWhere1 + ''''''' + ' + char(13) + 'Convert(VarChar(8000),' + @Columna + ') + '''''' '
			If @tiEsCampoText = 1
			Begin
				Set @vchWhere1 = @vchWhere1 + ''''''' + ' +  @vchNvaLn  + 'Convert(VarChar(8000), '+ @Columna + ') + '''''' '
			End
			Else
			Begin
				Set @vchWhere1 = @vchWhere1 + ''''''' + ' + @vchNvaLn + 'Convert(VarChar(8000), Replace(' + @Columna + ', '''''''', '''''''''''')) + '''''' '
			End

			Set @vchWhere1 = @vchWhere1 + ' AND '		
		End
	Else
		-- Sin comillas
		Begin
			--Set @vchWhere1 = @vchWhere1 + ''' + Convert(VarChar(8000),' + @Columna + ') + '
			Set @vchWhere1 = @vchWhere1 + ''' + ' + @vchNvaLn + 'Convert(VarChar(8000),' + @Columna + ') + '
			Set @vchWhere1 = @vchWhere1 + ''' AND '
		End

	Update #Campos
	Set tiUsado = 1
	Where vchNombre = @Columna
End

-- Quito el �ltimo AND 
If @esCadena = 1		--Si el �ltimo fue cadena
	Set @vchWhere1 = LEFT(@vchWhere1, LEN(@vchWhere1) - 5) + ''''
Else
	Set @vchWhere1 = LEFT(@vchWhere1, LEN(@vchWhere1) - 7)

-------------- LLENO EL INSERT
-- Lleno la variable @vchCamposComa con los campos de la tabla separados por coma
Update #Campos
Set tiUsado = 0

Set @iContadorTmp = 0--Contar los campos procesados

While exists (Select * from #Campos Where tiUsado = 0)
Begin
	Select Top 1 @Columna = vchNombre
	From #Campos
	Where tiUsado = 0

	Set @iContadorTmp = @iContadorTmp + 1--incrementar los campos procesados
	
	Set @vchCamposComa = @vchCamposComa + @Columna

	Update #Campos
	Set tiUsado = 1
	Where vchNombre = @Columna
	
	If exists ( Select * From #Campos 
		Where tiUsado = 0 )--Existen mas campos pendientes
	Begin
		--concatenar una coma
		Set @vchCamposComa = @vchCamposComa + ', '
	End
	
	If (@iContadorTmp % @iCantCam_NvaLinea_CampoInsert) = 0 --Es multiplo, se debe hacer un salto de linea
	Begin
		Set @vchCamposComa = @vchCamposComa + @vchNvaLn + CHAR(9)
	End
End

Set @vchInsert = 'Insert Into ' + @vchEsquemaTabla + '.' + @vchTabla + '(' + @vchNvaLn + CHAR(9) + @vchCamposComa + ')'

-------------- LLENO EL VALUES
Update #Campos
Set tiUsado = 0

Set @vchValues = 'Values ('''
While exists (Select * from #Campos Where tiUsado = 0)
Begin
	Select Top 1 @Columna = vchNombre, @esCadena = tiEsCadena, @tiEsTimeStamp = tiEsTimeStamp, @esFecha = tiEsFecha,
	@tiEsCampoText = tiEsCampoText
	From #Campos
	Where tiUsado = 0

	Select @iCamposFaltantes = COUNT(*)
	From #Campos
	Where tiUsado = 0
	
	If @esCadena = 1 Or @esFecha = 1
		--Poner comillas
		Begin
			If @tiEsCampoText = 1
			Begin
				Set @vchTemp = ' + ' + @vchNvaLn + 'Case When ' + @Columna + ' Is Null Then ''Null'' Else '''''''' + Convert(VarChar(8000), ' + @Columna + ') + '''''''' End + '
			End
			Else --No campo tipo text
			Begin				
				Set @vchTemp = ' + ' + @vchNvaLn + 'Case When ' + @Columna + ' Is Null Then ''Null'' Else '''''''' + Convert(VarChar(8000), Replace(' + @Columna + ','''''''','''''''''''')) + '''''''' End + '				
			End
						
			-- Este IF va porque a veces el update es m�s grande que 8000 caraceteres
			If len(@vchUpdate1) + len(@vchTemp) <= 3500 
				Set @vchUpdate1 = @vchUpdate1 + '''' + @Columna + ' = ''' + @vchTemp 
			Else 
				Set @vchUpdate2 = @vchUpdate2 + '''' + @Columna + ' = ''' + @vchTemp 
			End
	Else
	Begin
		If @tiEsTimeStamp = 1 
		Begin
			Set @vchTemp = ' + ' + @vchNvaLn + '''Null' 
			-- De los timestamps no hago update
		End
		Else
		Begin			
			Set @vchTemp = ' + ' + @vchNvaLn + 'Case When ' + @Columna + ' Is Null Then ''Null'' Else Convert(VarChar(8000), ' + @Columna + ') End + '
			
			-- Este IF va porque a veces el update es m�s grande que 8000 caraceteres
			If len(@vchUpdate1) + len(@vchTemp) <= 3500 
				Set @vchUpdate1 = @vchUpdate1 + '''' + @Columna + ' = ''' + @vchTemp 
			Else 
				Set @vchUpdate2 = @vchUpdate2 + '''' + @Columna + ' = ''' + @vchTemp 
		End
	End
	
	--Si hay mas campos pendientes, agregar una coma
	If @iCamposFaltantes > 1--no es el ultimo campo, agregar una coma
	Begin
		Set @vchValues = @vchValues + @vchTemp + ''', ''' + ' + ' + @vchEnter + ' + char(9) '
		
		If len(@vchUpdate1) + len(@vchTemp) <= 3500 
			Set @vchUpdate1 = @vchUpdate1 + ''', ''' + ' + ' + @vchEnter + ' + char(9) + '
		Else 
			Set @vchUpdate2 = @vchUpdate2 + ''', ''' + ' + ' + @vchEnter + ' + char(9) + '
	End
	Else--Es el ultimo campo
	Begin
		Set @vchValues = @vchValues + @vchTemp + ''') ''' + ' + ' + @vchEnter + ' + char(9) '		
	End		
	Update #Campos
	Set tiUsado = 1
	Where vchNombre = @Columna
End

--FINAL
if @tiTipo = 0			-- Generar el if exists->delete->Insert
	Begin
		Set @vchReturn = 'Select ''If Exists (Select * From ' + @vchEsquemaTabla + '.' + @vchTabla + ' ' + @vchWhere1 + '+ '')'' + ' + @vchEnter + ' + Char(9) + ' + @vchNvaLn + '''Delete ' + @vchEsquemaTabla + '.' + @vchTabla
		Set @vchReturn = @vchReturn + ' ' + @vchWhere1 + ' + ' + @vchNvaLn + '' + @vchEnter + ' + ''GO'' + ' + @vchEnter + ' + '
		Set @vchReturn = @vchReturn + @vchNvaLn + '''' + @vchInsert + ''' + ' + @vchEnter + ' + ' + @vchNvaLn + '''' 
		Set @vchReturn = @vchReturn + @vchValues + ' + ' + @vchNvaLn + '' + @vchEnter + ' + ''GO'' + ' + @vchEnter + ' ' 
		Set @vchReturn = @vchReturn + @vchNvaLn + 'From ' + @vchBaseDatosUnPunto + @vchEsquemaTabla + '.' + @vchTabla 
	End
else if @tiTipo = 1		-- Generar el if not exists->Insert
	Begin
	 	Set @vchReturn = 'Select ''If Not Exists (Select * From ' + @vchEsquemaTabla + '.' + @vchTabla + ' '
	 	Set @vchReturn = @vchReturn + @vchWhere1 + '+ '')'' + Char(9) + ' + @vchNvaLn 
		Set @vchReturn = @vchReturn + ' + ' +  @vchEnter + ' + char(9) + '
		Set @vchReturn = @vchReturn + @vchNvaLn + '''' + @vchInsert + ''' + ' + @vchEnter + ' + char(9) + '
		Set @vchReturn = @vchReturn + @vchNvaLn + '''' + @vchValues + ' + ' + @vchNvaLn + '' + @vchEnter
		Set @vchReturn = @vchReturn + ' + ''GO'' + ' + @vchEnter + ' ' + @vchNvaLn + 'From ' + @vchBaseDatosUnPunto
		Set @vchReturn = @vchReturn + @vchEsquemaTabla + '.' + @vchTabla 
	End

else if @tiTipo = 2		-- Generar el if exists Insert else Update
	Begin
		-- En este caso, no alcanzan los 8000 caracteres que permite el query analizer como respuesta de un select, entonces hago prints parciales
		if @tiTipoResultado = 0 --resultado con prints
		Begin
			Print ''
			Print 'Select ''If Exists (Select * From ' + @vchEsquemaTabla + '.' + @vchTabla + ' ' + @vchWhere1 + '+ '')'' + ' + @vchEnter + ' + Char(9) + ' 
			Print '''Update ' + @vchEsquemaTabla + '.' + @vchTabla + @vchNvaLn + char(9) + 'Set ' + ''' + ' 
			Print @vchUpdate1 
			Print @vchUpdate2 + ' + ' + @vchEnter + ' + char(9) + ''' + @vchWhere1
			Print ' + ' + @vchEnter + ' + ''Else'' + ''' + @vchNvaLn + char(9) + @vchInsert + ''' + ' + @vchEnter + ' + char(9) + ''' + @vchValues 
			Print ' + ' + @vchNvaLn + '' + @vchEnter + ' + ''GO'' + ' + @vchEnter + ' + ' + @vchEnter + ' ' + @vchNvaLn + 'From ' + @vchBaseDatosUnPunto + @vchEsquemaTabla + '.' + @vchTabla 
			If @vchWhere <> ''
				Print 'Where ' + @vchWhere
		End
		Else if @tiTipoResultado = 1 --resultado con selects
		Begin
			Insert into #tmpTexto
			Select ''

			Insert into #tmpTexto
			Select 'Select ''If Exists (Select * From ' + @vchEsquemaTabla + '.' + @vchTabla + ' ' + @vchWhere1 + '+ '')'' + ' + @vchEnter + ' + Char(9) + '

			Insert into #tmpTexto
			Select '''Update ' + @vchEsquemaTabla + '.' + @vchTabla + @vchNvaLn + char(9) + 'Set ' + ''' + '

			Insert into #tmpTexto
			Select @vchUpdate1  + @vchNvaLn

			Insert into #tmpTexto
			Select @vchUpdate2 + ' + ' + @vchEnter + ' + char(9) + ''' + @vchWhere1

			Insert into #tmpTexto
			Select ' + ' + @vchEnter + ' + ''Else'' + ''' + @vchNvaLn + char(9) + @vchInsert + ''' + ' + @vchEnter + ' + char(9) + ''' + @vchValues

			Insert into #tmpTexto
			Select ' + ' + @vchNvaLn + '' + @vchEnter + ' + ''GO'' + ' + @vchEnter + ' + ' + @vchEnter + ' ' + @vchNvaLn + 'From ' + @vchBaseDatosUnPunto + @vchEsquemaTabla + '.' + @vchTabla

			If @vchWhere <> ''
			Begin
				Insert into #tmpTexto
				Select 'Where ' + @vchWhere + @vchNvaLn
			End

			Select *
			From #tmpTexto
		End
	End

If @vchWhere <> ''
	Set @vchReturn = @vchReturn + @vchNvaLn + 'Where ' + @vchWhere

if @tiTipo <> 2
	if @tiTipoResultado = 0 --resultado con prints
	Begin
		Print @vchReturn
	End
	Else if @tiTipoResultado = 1 --resultado con selects
	Begin
		Insert into #tmpTexto
		Select @vchReturn

		Select * from #tmpTexto
	End
_Fin:
If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
 Set nocount off
End
GO

If exists ( Select * From sysobjects Where name = 'sp_longitud' ) 
	Drop procedure dbo.sp_longitud
GO
Create procedure dbo.sp_longitud(
@vchValor varchar(8000)
)
AS
/*
** Nombre:		sp_longitud
** Prop�sito:	Retornar la longitud de una cadena
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			FDCG
** Csd creaci�n:				?
** Fecha modificaci�n: 		29/Mayo/2012
** Autor modificaci�n: 		CJFU
** Csd modificaci�n:			csd173
** Compatibilidad:			1.75
** Revisi�n:					1
*/
Print len(@vchValor)
GO

If exists ( Select * From sysobjects Where name = 'sp_comentario_nuevo' ) 
	Drop procedure dbo.sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:				?
** Fecha modificaci�n: 		28/Junio/2011, 8/Mayo/2012,06/Junio/2014
** Autor modificaci�n: 		CJFU, CJFU, RConstante
** Csd modificaci�n:			csd173, csd173,Csd173
** Compatibilidad:			1.75
** Revisi�n:					3
*/

Declare @iCsd int
Declare @vchFecha varchar(50)

	Select @iCsd = iRevision
	from bd_sicyproh..vptUltimaActualizacion
	
	Select @vchFecha = cast(datepart(dd,getdate()) as varchar) + '/' + vchDescripcion + '/' + cast(datepart(year,getdate()) as varchar)
	From bd_sicyproh..sysvalorlista
	where siagrupador = 75220
	And vchMnemonico = cast(datepart(mm,getdate())  as varchar)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' ' + 'Csd' + cast(@iCsd as varchar) + ' ' + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' ' + 'Csd' + cast(@iCsd as varchar) + ' ' + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- [Mantis XXX - ' + @vchMnemonicoUsuario + ']'
	print '-- [/Mantis XXX - ' + @vchMnemonicoUsuario + ']' + char(13)  + char(10) + char(13) + char(10) 
	print '-- [Guias Arboledas ' + cast(@iCsd as varchar) + ' MODULO id = XX ' + @vchMnemonicoUsuario + ']'
	print '-- [/Guias Arboledas ' + cast(@iCsd as varchar) + ' MODULO id = XX ' + @vchMnemonicoUsuario + ']'

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			' + @vchFecha --+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Csd creaci�n:				' + 'Csd' + cast(@iCsd as varchar) --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Csd modificaci�n:			' --+ char(13) + char(10) 
	Print '** Compatibilidad:			1.75' --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print ''
	Print 'Declare @iError int'
	Print 'Declare @iErrorBase int'
	Print 'Declare @iErrorSql int'
	Print 'Set nocount on'

	Print 'Set @iErrorSql = @@ERROR'
	Print 'If @iErrorSql <> 0'
	Print 'Begin'
	Print '	Set @iError = @iErrorSql'
	Print 'End'
	Print 'If @iError <> 0'
	Print 'Begin'
	Print '	Goto _Fin'
	Print 'End'
	Print ''
	Print 'COMMIT TRANSACTION <%NomTran%>'
	Print 'COMMIT TRANSACTION <%NomTran%>'
	Print 'Goto _Fin'
	Print ''
	Print '_RollBack:'
	Print ''
	Print 'If @@trancount = 1'
	Print ' ROLLBACK TRAN'
	Print 'Else If @@trancount > 1'
	Print ' COMMIT TRAN'
	Print '_Fin:'
	Print ''
	Print 'If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este'
	Print 'Begin'
	Print ' Set nocount off'
	Print 'End'
	Print ''
	Print 'Return @iError'
End
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinNombreTablaTemp' ) 
	Drop procedure dbo.sp_ppinNombreTablaTemp
GO

Create Procedure dbo.sp_ppinNombreTablaTemp
(
	@vchTabla varchar(512)
)
AS
/*
** Nombre:				sp_ppinNombreTablaTemp
** Prop�sito:				Dado un nombre de tabla temporal (ej. #tmp) devuelve el nombre real de la tabla en tempdb
** Campos/Par�metros:		@vchTabla			Nombre de la tabla
**
** Dependencias:			?
**
** Fecha creaci�n:			03-01-06
** Autor creaci�n:			FDCG
**
** Fecha modificaci�n:
** Autor modificacion:
** csd modificaci�n:
** Compatibilidad:			1.75
** Revision:				1
*/
Declare @vchReal varchar(512)
If LEFT(@vchTabla, 1) <> '#'
	Begin
		Print @vchTabla
		Return
	End
Select @vchReal = convert(varchar(512),name)
From tempdb..sysobjects
where name like @vchTabla + '_%'
Print @vchReal
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinGeneraScriptObjeto' ) 
	Drop procedure dbo.sp_ppinGeneraScriptObjeto
GO

CREATE  procedure dbo.sp_ppinGeneraScriptObjeto
(
@iAccion int = 0,
@vchValor1 varchar(512) = '',
@vchValor2 varchar(512) = '',
@vchValor3 varchar(512) = '',
@vchValor4 varchar(512) = '',
@vchValor5 varchar(512) = '',
@vchValor6 varchar(512) = NULL
)
AS
/*
** Nombre:		sp_ppinGeneraScriptObjeto
** Prop�sito:	Procedimiento almacenado para facilir el manejo o manipulacion de la base de datos
** Campos:			@iAccion -->0		ejecuta un sp_helptext de este procedimiento almacenado
**							
**					@iAccion -->1		Arma el query para eliminar o crear un objeto de la base de datos. El crear o eliminar depende del tipo de objeto
**									@vchValor1		Nombre del objeto (Requerido)
**
**					@iAccion -->2		Retorna los objecto que cumplen los criterios proporcionado (Nombre o parte del nombre)
**									@vchValor1		Nombre del objeto (Requerido)
**									@vchValor2		Tipo de objeto (opcional)
**					@iAccion -->3		Retorna los objetos que contienen en el cuerpo del mismo un texto (procedimientos, vistas y triggers)
**									@vchValor1		Texto a busca (Requerido)
**									@vchValor2		Tipo de objeto (opcional)
**					@iAccion -->4		Retorna las columnas de una tabla o vista o par�metros de un SP separados por coma
**									@vchValor1		Nombre de la Tabla, vista o SP
**									@vchValor2		Cantidad de columnas por cada fila devuelta
**									@vchValor3		Alias de la tabla
**									@vchValor4		Enviar el numéro uno si se quiere que lugar de separar los campos
**																por espacios se desea separarlos por tabuladores.
**					@iAccion -->5		Retorna el script CREATE TABLE de la tabla existente proporcionada
**									@vchValor1		Nombre de la Tabla
**					@iAccion -->6		Retorna los comentarios que deben llevar los objetos de la base datos
**					@iAccion -->7		Agrega una columna a la tabla especificada
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		Nombre de la nueva columna
**									@vchValor3		Tipo de dato de la nueva columna
**									@vchValor4		Longitud del tipo de datos (si aplica)
**									@vchValor5		Permite nulos (0:no  1:si)
**									@vchValor6		Valor por defecto (o NULL si no aplica)
**					@iAccion -->8		Genera autom�ticamente un script con los alter table (drop o add) de los constraints
**									que hacen referencia (for�nea) a la tabla especificada
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		0: Genera los Add Constraint (por defecto)
**												1: Genera los Drop Constraint
**					@iAccion -->9		Genera el script que inserta los datos de una tabla
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		Cl�usula where a usar (opcional)
**									@vchValor3		Tipo de resultado: 0: Genera el If Exists->Delete->Insert (dafault)
**																	 1: Genera el If Not Exists->Insert
**					@iAccion -->10		Muestra la consulta al diccionario de datos para un objeto dado
**									@vchValor1		Nombre del objeto.
**					@iAccion -->11		Retorna las consultas a las listas y constantes mas usuales
**									@vchValor1 Nombre de la tabla, si se envia un vacio se retorna todas las tablas ( catconstante, sysvalorlista, catvaloreslista, sysparametro, configuracion)
**									@vchValor2 Valor para el filtro, si se envia un vacio no se coloca ningun filtro
**									@vchValor3 Campo por que se quiere filtrar(nombre) o enviar un uno para filtrar por el consecutivo y un 2
**									para filtrar por el agrupador, si se envia un vacio se filtra por el consecutivo (llave de la tabla).
**					@iAccion -->12		Retorna los objectos que hacen un insert a x tabla de la base de datos
**									@vchValor1 Nombre de la tabla, debe especificarse una sola tabla
**									@vchValor2 Tipo de objeto (<p>-->Procedimientos almacenados, <TR>--Trigrer, <vac�o>-->Sin filtrar el tipo de objeto.
**									@vchValor4 varchar(512) = '',
**									@vchValor5 varchar(512) = '',
**									@vchValor6		Filtro
**					@iAccion -->13		Dada una 

tabla y un alias para la misma, Retorna un SELECT con FROM y WHERE seg�n sus llaves for�neas
**									@vchValor1 Nombre de la tabla
**									@vchValor2 Alias que tendr� la tabla en el select devuelto
**
**					@iAccion -->14		Dado un comando del shell lo ejecuta y devuelve el output
**									@vchValor1: Comando. por ejemplo: 'dir *.exe'
**
**					@iAccion -->15		Devuelve los inserts a CatGralObjeto y CatEspObjeto para una tabla que existe en el diccionario de datos
**									@vchValor1: Nombre de la tabla. por ejemplo: 'ACFMATE1'
**
**					@iAccion -->16		Devuelve las dependencias de un SP (execs anidados a otros SPs)
**									@vchValor1: Nombre del procedimiento almacenado
**									@vchValor2: Cantidad m�xima de niveles de anidamiento
**					@iAccion -->17		Devuelve el script para la creaccion de los objetos de estas herramientes de sql para el desarrollador 
**										(no se requieren parametros). Los objetos tomados son los que est�n asociados a el m�dulo "aplicaci�n interna",
**										est� asociados a el subsistema "herramientas para el sql,...", es un procedimiento almacenado y pertence a la base
**										de datos master.
**
** Dependencias: 	
**
** Fecha creaci�n: 	12/Noviembre/2005
** Autor creaci�n: 	CJFU
** Csd creaci�n:				?
**					
** Fecha modificaci�n: 	24/11/05, 03-03-07, 05-07-06, 01-08-07, 30-04-08, 12-01-09, 26/Febrero/2010,
**						7/Marzo/2011, 29/Agosto/2013, 20/Septiembre/2013, 9/Mayo/2014
** Autor modificacion: 	FDCG, FDCG, FDCG, FDCG, FDCG, FDCG, CJFU,
**						CJFU, CJFU, CJFU, CJFU
** Csd modificaci�n:		
**						?, ? , ?, csd173
** Compatibilidad:	1.75
** Revision:		13
*/
Declare @vchQuery nvarchar(4000)
Declare @vchNvaLn nVarchar(2)

Set nocount on

set @vchNvaLn = CHAR(13) + CHAR(10)

If @iAccion = 0 --Ejecutar sp_helptext de este procedimiento almacenado
	Begin
		Select c.text
		From master..sysobjects o, master..syscomments c
		Where o.id = c.id
		And o.name = 'sp_ppinGeneraScriptObjeto'
	End

If @iAccion = 1	--Drop
	Begin
		If rtrim(ltrim(@vchValor1)) = ''
		Begin
			Print 'Esta accion es para la validaci�n de la creaccion/eliminacion de un objeto '
			Print '(creacion para tablas, reportes o campos de tablas y eliminacion para el resto de objetos'
			Print 'Favor de proporcionar el nombre del objeto'
			Print '@vchValor1		-->Nombre del objeto'
			Print '@vchValor2		-->Nombre del campo (siempre y cuando sea una tabla)'
		End
		Else
		If Substring(@vchValor1, 1, 1) = '#' 	-- Si es una temporal
			Begin				
				Select 'If exists ( Select * From tempdb..sysobjects Where name like ''' + @vchValor1 + '[_]%'' ) ' + @vchNvaLn +
					char(9) + 'Drop Table ' + @vchValor1 + @vchNvaLn + 'GO' + @vchNvaLn
				From tempdb..sysobjects o
				Where o.name like @vchValor1 + '[_]%'
			End
		Else If substring(@vchValor1, len(@vchValor1) -3, 4) = '.rpt'--Es un reporte
		Begin
			Exec SP_PpInGenera_ScriptRpt
			@vchNombreReporte = @vchValor1
		End
		Else
			Begin
				
				Set @vchValor2 = ltrim(rtrim(@vchValor2))
				--Revisar si es una tabla, si es as� para cambiar que en lugar del exists sea un not exists
				If exists ( Select * From sysobjects o
					Where o.name = @vchValor1 And o.xtype = 'U' ) --Tabla
				Begin
					--Si no se est� enviando el segundo parametro, quiere decir que unicamente se quiere el if not exists
					If @vchValor2 = '' or @vchValor2 is null
					Begin
						Select 'If not exists ( Select * From sysobjects Where name = ''' + name + ''' ) '
						From sysobjects o
						Where o.name = @vchValor1						
					End
					Else
					Begin
						Select 'If not exists ( Select * From sysobjects o, syscolumns c Where o.id = c.id' + @vchNvaLn + char(9) +
							'And o.name = ''' + o.name + ''' And c.name = ''' + @vchValor2 + ''' ) '
						From sysobjects o
						Where o.name = @vchValor1
					End
				End
				Else
				Begin
					--Select db_name() as Xdb_name
					
					Set @vchQuery = N'SELECT ''IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = '' + char(39) + O.vchNombre + char(39) + '' ) ''' + 
									' + char(13) + char(10) + char(9) + ' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'''DROP '' + CASE WHEN O.vchType = ''U'' THEN ''TABLE''
									WHEN O.vchType = ''P'' THEN ''PROCEDURE''
									WHEN O.vchType = ''TR'' THEN ''TRIGGER''
									WHEN O.vchType = ''V'' THEN ''VIEW''
									WHEN O.vchType = ''FN'' THEN ''FUNCTION''
									WHEN O.vchType = ''TF'' THEN ''FUNCTION''
									End 					
							 + '' '' + O.vchNombreSquema + ''.'' + O.vchNombre + char(13) + char(10) + ''GO'' + char(13) + char(10) ' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'From ' + db_name() + '..VpTdCatObjetoBD_SQL O' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'Where O.vchNombre =  ''' + @vchValor1 + '''' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'And O.vchType in ( ''U'',  --TABLE
					''P'', --PROCEDURE
					''TR'', --TRIGGER
					''V'', --View
					''FN'', --Function
					''TF'') --Function'
					
					--Select @vchQuery as vchQuery
					
					EXECUTE sp_executesql @vchQuery
				End
			End
	End
If @iAccion = 2	--Nombres like...
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el filtro'
		Else
		Begin
			Set @vchQuery  = 'Select Distinct o.name '
			Set @vchQuery  = @vchQuery + 'From sysobjects o '
			Set @vchQuery  = @vchQuery + 'Where o.name like ' + char(39) + '%' + @vchValor1 + '%' + char(39) + ' '
			If @vchValor2 <> ''
				Set @vchQuery  = @vchQuery + 'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
			Exec ( @vchQuery  )

			Select @vchQuery as vchQuery
		End
If @iAccion = 3	--Comentarios like
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el filtro'
		Else
		Begin
			
			Set @vchQuery  = 'Select Distinct o.name '
			Set @vchQuery  = @vchQuery + 'From sysobjects o, syscomments c '
			Set @vchQuery  = @vchQuery + 'Where o.id = c.id '
			Set @vchQuery  = @vchQuery + 'And c.text like ' + char(39) + '%' + @vchValor1 + '%' + char(39) + ' '
			If @vchValor2 <> ''
				Set @vchQuery  = @vchQuery + 'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
			Set @vchQuery  = @vchQuery + ' And o.name <> ''' + @vchValor1 + ''' '
			Exec ( @vchQuery  )

			Select @vchQuery as vchQuery
		End

If @iAccion = 4	-- Campos de una tabla o vista � par�metros de un SP separados por coma
Begin
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el nombre del objeto'
		Else
		Begin
			Declare @ColName varchar(70), @cont int

			Create Table #tmpColumnas (
				ya smallint default(0),
				colName varchar(70),
			)
			Set nocount on
			
			If @vchValor3 is null
			Begin
					Set @vchValor3 = ''
			End
			Else
			Begin--eliminar los espacios
				Set @vchValor3 = rtrim(ltrim(@vchValor3))
			End
	
			If @vchValor3 <> ''
			Begin
				Set @vchValor3 = @vchValor3 + '.'
			End

			If exists (Select * from sysobjects where name = @vchValor1 and type IN ('U', 'S', 'V', 'P'))
			BEGIN
				Insert #tmpColumnas
				Select 0, @vchValor3 + c.name
				From sysobjects o, syscolumns c
				Where o.id = c.id
				AND o.name = @vchValor1
				Order by c.colid 
			END
			ELSE
			BEGIN
				If exists ( Select * from tempdb..sysobjects where name like @vchValor1 + '[_]%' )
				Begin
					-- Es una tabla temporal
					Insert #tmpColumnas
					Select 0, @vchValor3 + c.name
					From tempdb..sysobjects o, tempdb..syscolumns c
					Where o.id = c.id
					AND o.name = (Select Top 1 name From tempdb..sysobjects Where name like @vchValor1 + '[_]%')
					Order by c.colid 					
				End
				Else
				Begin
					-- No existe o no es un objeto v�lido
					If object_id(@vchValor1) Is Null
						Print 'El objeto [' + @vchValor1 + '] no existe en [' + db_name() + ']'
					else
						Print 'El objeto [' + @vchValor1 + '] no es un objeto v�lido'
					Return -1
				End
			END

			Set @vchQuery = ''
			If (@vchValor2 = '')
				Set @vchValor2 = '0'
			Set @cont = 0
			while exists (Select * From #tmpColumnas Where ya=0)
				Begin
					Select TOP 1 @ColName = colName From #tmpColumnas Where ya=0
					Update #tmpColumnas Set ya = 1 Where colName = @colName
					Set @vchQuery = @vchQuery + @ColName 

					If @vchValor4 = 1
					Begin
						Set @vchQuery = @vchQuery + char(9)
					End			
					Else If exists (Select * From #tmpColumnas Where ya=0)
					Begin
						Set @vchQuery = @vchQuery + ', '
					End

					Set @cont = @cont + 1
					If @cont = Convert(int, @vchValor2)
						BEGIN
							Set @vchQuery = @vchQuery + @vchNvaLn
							Set @cont = 0
						END
				End
			Set nocount off
			Select @vchQuery

		End
	End

If @iAccion = 5 	-- Create Table con los campos, tipo, longitud y nullable
	Begin
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el nombre del objeto'
		Else
		Begin 
			Exec sp_ppinScriptTabla @vchValor1
		End
	End	

If @iAccion = 6 	--Retorna los comentarios que deber�n llevar los objetos de la base de datos (triggers, vistas y procedimientos almacenados)
	Begin
		Declare @vchTmp varchar(8000)
		Set @vchTmp = @vchNvaLn + Char(10) + '/*' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Nombre:			?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Prop�sito:			?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Campos/Par�metros:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '**' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Dependencias:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '**' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Fecha creaci�n:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Autor creaci�n:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** csd creaci�n: 		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Fecha modificaci�n:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Autor modificacion:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** csd modificaci�n:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Compatibilidad:		1.75' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Revision:			0' + @vchNvaLn
		Set @vchTmp = @vchTmp + '*/'
		Print @vchTmp
	End

If @iAccion = 7	-- Agregar columna a tabla
	Begin
	-- Variable que indica si el tipo de datos elegido EXIGE una longitud
	Declare @tiAplicaLongitud tinyint
	Set @tiAplicaLongitud = 0
	If rtrim(ltrim(@vchValor1)) = '' OR rtrim(ltrim(@vchValor2)) = '' OR rtrim(ltrim(@vchValor3)) = '' 
		BEGIN
			Print 'Par�metros incorrectos'
			Print 'Formato: 7, NOMBRE_TABLA, NOMBRE_COLUMNA, TIPO_DATO, LONGITUD, PERMITE_NULOS, POR_DEFECTO'
			Print '-------------------------------------------------------------------------------------------------'
			Print 'NOMBRE_TABLA: Nombre de la tabla						ej: ACFDEPE1'
			Print 'NOMBRE_COLUMNA: Nombre de la columna a insertar					ej: vchCodigoABC'
			Print 'TIPO_DATO: Nombre completo del tipo de datos					ej: varchar'
			Print 'LONGITUD: Longitud del tipo de datos (si aplica)				ej: 10'
			Print 'PERMITE_NULOS: 0->No se permiten nulos  1->Se permiten nulos			ej: 0'
			Print 'POR_DEFECTO: Valor por defecto o NULL si no aplica				ej: ''ABC'''
		END
	Else
		BEGIN
			-- Es una tabla no existente ?
			If not exists (Select * from sysobjects where name = @vchValor1 and type IN ('U', 'S'))
			BEGIN

				If object_id(@vchValor1) Is Null
					Print 'El objeto [' + @vchValor1 + '] no existe en [' + db_name() + ']'
				else
					Print 'El objeto [' + @vchValor1 + '] no es un objeto v�lido'
				Return -1
			END
			-- Existe la comuna en la tabla?
			If exists ( Select * from SysObjects o, syscolumns c Where o.id = c.id And o.name = @vchValor1 And c.name = @vchValor2)
			BEGIN
				Print 'Advertencia: La columna [' + @vchValor1 + '.' + @vchValor2 + '] ya existe'
			END
			-- Es un tipo de datos no v�lido?
			If Not exists (Select * From SysTypes Where Name = @vchValor3)
			BEGIN
				Print 'El tipo de datos proporcionado [' + @vchValor3 + '] no es v�lido' + @vchNvaLn + @vchNvaLn
				Print 'Los tipos de datos v�lidos son: '
				Select Name AS ' ' From SysTypes
				Return -1
			END
			-- El tipo de datos exige una longitud y fue proporcionada incorrectamente?
			if Exists (Select * from SysTypes Where name = @vchValor3 AND status = 2)
				Set @tiAplicaLongitud = 1
			If (@tiAplicaLongitud = 1) and (isnumeric(@vchValor4) = 0 Or @vchValor4 = '' Or @vchValor4 = '0')
			BEGIN
				Print 'El tipo de datos [' + @vchValor3 + '] exige una longitud v�lida'
				Return -1
			END
						
			Set @vchQuery = 'ALTER TABLE ' + @vchValor1 + @vchNvaLn 					-- Alter Table TABLA
			Set @vchQuery = @vchQuery + char(9) + 'ADD ' + @vchValor2 + ' '			-- Alter Table TABLA Add COLUMNA
			Set @vchQuery = @vchQuery + @vchValor3 + ' '							-- Alter Table TABLA Add COLUMNA TIPO
			If @tiAplicaLongitud = 1					
				Set @vchQuery = @vchQuery + '(' + @vchValor4 + ') '				-- Alter Table TABLA Add COLUMNA TIPO (XX)
			If @vchValor5 = '0'
				Set @vchQuery = @vchQuery + 'NOT '								-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT
			Set @vchQuery = @vchQuery + 'NULL'									-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT NULL
			If @vchValor6 IS NOT NULL
				Set @vchQuery = @vchQuery + ' CONSTRAINT DF_' + upper(@vchValor1) + '_' + upper(@vchValor2) + ' DEFAULT (''' + @vchValor6 + ''')'		-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT NULL DEFAULT (YY)
			Select @vchQuery
		END
	End
If @iAccion = 8	--SP_AddDrop_ForeignKeys
	Exec ('SP_ppinAddDrop_ForeignKeys ''' + @vchValor1 + ''',''' + @vchValor2 + '''')
If @iAccion = 9	--SP_Genera_Insert 
	Exec ('SP_ppinGenera_Insert ''' + @vchValor1 + ''',''' + @vchValor2 + ''', ' + @vchValor3)
If @iAccion = 10 --Consulta al diccionario de datos
Begin
	Set Nocount On
	Select CC2.vchdescripcion AS Base, G.vchNombreObjeto, CC1.vchdescripcion AS TipoObjeto, 
	CC3.vchdescripcion AS Modulo,	vchDescripcionObjeto, E.vchNombreCampo, E.vchDescripcionCampo, G.siTipoObjeto,
	G.iIdObjeto
	Into #tmpAccion10
	From Bd_Req.Dic.vpinGralDiccionarioDatos G, bd_sicyproh..vpinEspDiccionarioDatos E,
	bd_sicyproh..CatConstanteDiccionarioDatos CC1, bd_sicyproh..CatConstanteDiccionarioDatos CC2,
	bd_sicyproh..CatConstanteDiccionarioDatos CC3
	Where G.iIdObjeto=E.iIdObjeto
	And CC1.siconsecutivo = G.siTipoObjeto
	And CC2.siconsecutivo = G.siBaseDatos
	And CC3.siconsecutivo = G.siIdModulo
	And vchNombreObjeto = @vchValor1
	And CC2.vchdescripcion = db_name()
	

	If Not Exists ( Select * From #tmpAccion10 )
	Begin
		Print 'El objeto [' + @vchValor1 + '] no existe en el diccionario de datos para [' + db_name() + ']'
		Return -1
	End
	
	Declare @tmpBase varchar(512), @tmpObjeto varchar(512), @tmpTipoObjeto varchar(512), @tmpDescripcionObjeto varchar(1000)
	Declare @tmpTipo int, @tmpModulo varchar(512), @iIdObjeto int
	Declare @tmpCampo varchar(512), @tmpDesc varchar(1000)
	Select Top 1 @tmpBase = Base, @tmpObjeto = vchNombreObjeto, @tmpTipoObjeto = TipoObjeto, 
	@tmpDescripcionObjeto = vchDescripcionObjeto, @tmpTipo = siTipoObjeto, @tmpModulo = Modulo, @iIdObjeto = iIdObjeto
	From #tmpAccion10

	Print '---------------- CONSULTA AL DICCIONARIO DE DATOS ----------------'
	Print 'Objeto: [' + @tmpTipoObjeto + '] ' + @tmpBase + '..' + @tmpObjeto + ' (' + @tmpDescripcionObjeto + ')' + @vchNvaLn + 'ID Objeto: ' + convert(varchar, @iIdObjeto) + @vchNvaLn
	Print 'M�dulo: ' + @tmpModulo + @vchNvaLn + @vchNvaLn
	if @tmpTipo = 1		-- Store Procedure
		Select '@' + vchNombreCampo AS Parametro, Replace(vchDescripcionCampo, @vchNvaLn,' ') AS Descripcion From #tmpAccion10
	Else
		Select vchNombreCampo AS Campo, Replace(vchDescripcionCampo,@vchNvaLn,' ') AS Descripcion From #tmpAccion10
	Set Nocount Off
End

If @iAccion = 11 --Retornar las consultas de tablas de listas m�s usuales
Begin
	If @vchValor1 = ''
	Begin
		Select @vchQuery = 'Select * From catconstante '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'siconsecutivo = ' + @vchValor2
													when '1' then 'siconsecutivo = ' + @vchValor2
													when '2' then 'siagrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn  --Un enter
		Select @vchQuery = @vchQuery + 'Select * From sysvalorlista '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'siConsecutivo = ' + @vchValor2
													when '1' then 'siConsecutivo = ' + @vchValor2
													when '2' then 'siAgrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn --Un enter
		Select @vchQuery = @vchQuery + 'Select * From catvaloreslista '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iconsecutivo = ' + @vchValor2
													when '1' then 'iconsecutivo = ' + @vchValor2
													when '2' then 'sicodpal = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
		Select @vchQuery = @vchQuery + 'Select * From sysparametro '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iCodParametro = ' + @vchValor2
				
									when '1' then 'iCodParametro = ' + @vchValor2
													when '2' then 'siAgrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
		Select @vchQuery = @vchQuery + 'Select * From configuracion '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iCodConfiguracion = ' + @vchValor2
													when '1' then 'iCodConfiguracion = ' + @vchValor2													
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
	End
	Else
	Begin
		Select @vchQuery + 'Select * From ' + @vchValor1 + ' '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iConsecutivo = ' + @vchValor2
													when '1' then 'iConsecutivo = ' + @vchValor2
													when '2' then 'iAgrupador = ' + @vchValor2													
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
	End
	Select @vchQuery as vchQuery
End

If @iAccion = 12 --Retornar las consultas de tablas de listas m�s usuales
Begin
	Set @vchValor1 = ltrim(rtrim(@vchValor1))
	Set @vchValor2 = ltrim(rtrim(@vchValor2))
	If @vchValor1 <> '' And @vchValor1 is not null
	Begin
		--Insert con un espacio
		Set @vchQuery = 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Insert con dos espacios
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert  ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Insert con tabulador
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert	' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con un espacio
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%into ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con dos espacios
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%Into  ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con tabulador
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%Into	' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '

		End
		Execute ( @vchQuery )
		Select @vchQuery as vchQuery
	End
	Else
	Begin
		Select 'Favor de proporcionar el nombre la de la tabla'
		Select '@iAccion -->12		Retorna los objectos que hacen un insert a x tabla de la base de datos'
		Select '@vchValor1 Nombre de la tabla, debe especificarse una sola tabla'
		Select '@vchValor2 Tipo de objeto (<p>-->Procedimientos almacenados, <TR>--Trigrer, <vac�o>-->Sin filtrar el tipo de objeto.'
	End
End

If @iAccion = 13 -- Retornar SELECT de una tabla seg�n sus llaves for�neas
Begin
	If @vchValor2 = '' Set @vchValor2 = 'A'
	Exec ('sp_ppinSelectFromWhereForaneo ''' + @vchValor1 + ''',''' + @vchValor2 + '''')
End

If @iAccion = 14 -- cmdShell
Begin
	Exec ('master..xp_cmdshell ''' + @vchValor1 + '''')
End

If @iAccion = 15 -- Inserts a CatGralObjeto y CatEspObjeto
Begin
	Exec ('sp_ppinTablaCatObjeto ''' + @vchValor1 + '''')
End

If @iAccion = 16 -- Dependencias por execs anidados
Begin
	If len(ltrim(rtrim(@vchValor2))) = 0
		Set @vchValor2 = '10'
	Exec ('sp_ppinDependenciaSP ''' + @vchValor1 + ''', ' + @vchValor2)
End

-- Recuperar los objetos de todas las herramientas de sql para que se ejecuten en otro servidor
If @iAccion = 17 
Begin
	Exec sp_ppinScriptHerramientaSql
End
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinTablaCatObjeto' ) 
	Drop procedure dbo.sp_ppinTablaCatObjeto
GO

CREATE   Procedure Dbo.sp_ppinTablaCatObjeto
(
	@nvchTabla nvarchar(1000)
)
AS
/*
** Nombre:				dbo.sp_ppinTablaCatObjeto
** Prop�sito:				Armar el insert a CatGralObjeto y CatEspObjeto para una TABLA dada
** Campos:				@vchTabla: Nombre de la tabla (debe existir en el diccionario de datos)
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			30/Abril/2008
** Autor creaci�n:			FDCG
** Csd creaci�n:			csd172
** Fecha modificaci�n: 		20/Julio/2009, 9/Marzo/2011, 11/Abril/2011, 11/Agosto/2011, 31/Agosto/2011, 27/Noviembre/2012,
**							7/Enero/2014, 19/Mayo/2014, 8/Octubre/2014
** Autor modificaci�n: 		CJFU, CJFU, CJFU, CJFU, CJFU, CGUTIERREZ,
**							CJFU, CJFU, CJFU
** Csd modificaci�n:		csd172, csd173, csd173, csd173, csd173,
**							csd173, csd173, csd173
** Compatibilidad:			1.75
** Revisi�n:					9
*/
Declare @iError int
Declare @iIdObjeto int
Declare @nvchQueryG nvarchar(max)
Declare @nvchQueryE nvarchar(max)
Declare @nvchEnter nvarchar(2)
Declare @nvchTab nvarchar(1)
Declare @vchEsquema nvarchar(30)
Declare @vchEsquema_Nombre nvarchar(1000)
Declare @iExiste int

SET @iExiste = 0

Declare @tmpCamposEspObjeto table
(
	NombreCampo nvarchar(80) not null,
	iIdEspDicDatos int not null,
	vchDescripcion nvarchar(100),
	iIdTipoBitacoraModificacion int not null,
	iIdTipoValor int not null,
	iIdEstatus int not null,
	iIdTipoFiltroMultiSucursal int not null,
	iOrden  int not null
)

Set nocount on

Set @nvchEnter = char(13) + char(10)
Set @nvchTab = char(9)

Set @iError = 0

Set @vchEsquema = ''

Set @vchEsquema_Nombre = @nvchTabla
--Revisar si en el nombre del objeto viene incluido el esquema
If @nvchTabla like '%.%'
BEGIN
	Select @vchEsquema = substring( @nvchTabla, 1, PATINDEX ( '%.%', @nvchTabla)-1  )
	
	Select @nvchTabla = substring( @nvchTabla, len(@vchEsquema) + 2, 1000 )
END

IF @vchEsquema is null
BEGIN
	SET @vchEsquema = ''
END

IF @vchEsquema = ''
AND NOT EXISTS ( Select * From bd_sicyproh..trangraldiccionariodatos
	Where siTipoObjeto in (  153005, 153006 )
	AND vchEsquema_Nombre = @vchEsquema_Nombre )
BEGIN
	Print N'Favor de proporcionar el esquema donde se encuentra el objeto '  + @vchEsquema_Nombre
	Set @iError = -1
	Goto _Fin
END

-- Si no existe la tabla en el diccionario de datos, env�o error
If not exists ( Select * From bd_sicyproh..trangraldiccionariodatos
	Where vchEsquema_Nombre = @vchEsquema_Nombre 
	And siTipoObjeto in ( 153001, 153003, 153005, 153006 ) ) --procedimiento almacenado, Tabla, reporte, Forma
Begin
	Print N'ERROR: El objeto ' + @vchEsquema_Nombre + N' no existe en el diccionario de datos, o no es del tipo tabla, forma (vb6) o procedimiento almacenado'
	Set @iError = -1
	Goto _Fin
End

Select @iIdObjeto = Convert(varchar, iIdObjeto)
From bd_sicyproh..trangraldiccionariodatos GD
Where GD.vchEsquema_Nombre = @vchEsquema_Nombre

If @iIdObjeto Is null
Begin
	Print 'ERROR: al obtener el ID del objeto'
	Set @iError = -1
	Goto _Fin
End

If exists ( Select *
	From bd_sicyproh..trangraldiccionariodatos GD
	Where iIdObjeto = @iIdObjeto
	And siTipoObjeto <> 153005 ) --Diferente a reporte
Begin
	If exists ( Select * From bd_sicyproh..CatGralObjeto Where iIdGralObjeto = @iIdObjeto )
	Begin
		Print '--El objeto ya existe previamente en CatGralObjeto'
	End
End
Else
Begin
	If exists ( Select * From bd_sicyproh..CatReporte
		Where iCodReporte = @iIdObjeto )
	Begin
		Print '--El objeto ya existe previamente en CatReporte'
		SET @iExiste = 1
	End
End

If exists ( Select *
	From bd_sicyproh..trangraldiccionariodatos GD
	Where iIdObjeto = @iIdObjeto
	And siTipoObjeto <> 153005 ) --Diferente a reporte
Begin
	Select @nvchQueryG = N'If not exists ( Select * From dbo.CatGralObjeto Where iIdGralObjeto = ' + cast( TGDD.iIdObjeto as varchar) + N' )' + @nvchEnter +
	@nvchTab + N'Insert Into CatGralObjeto (iIdGralObjeto, vchNombre, iIdTipoObjeto, tiActivo,' + @nvchEnter +
	@nvchTab + N'vchComentario, tiBitacora, iIdAplicacion, vchAlias,' + @nvchEnter +
	@nvchTab + N'iIdBaseDatos, vchVersion ) ' + @nvchEnter +
	@nvchTab + N'Values (' + cast(TGDD.iIdObjeto as varchar) + N', ''' + TGDD.vchNombre + N''', ' +
	case TGDD.siTipoObjeto when 153001 then N'5221'	--procedimiento almacenado
		when 153003 then N'5220' --tabla
		when 153006 then N'5223' else N'5220' end + N', 1, ' + @nvchEnter + --Forma (vb6)
	@nvchTab + N'''' + cast(TGDD.vchDescripcion as varchar(100)) + N''', 0, 5940, ' + N'''' + TGDD.vchEsquema_Nombre + N''', ' + @nvchEnter +
	@nvchTab + cast(SVL.siConsecutivo as varchar) + N', ''' + TGDD.vchVersion + N''') ' + @nvchEnter +
	N'GO' + @nvchEnter
	From bd_sicyproh..trangraldiccionariodatos TGDD, bd_sicyproh..sysvalorlista SVL
	Where TGDD.siBaseDatos = SVL.iReferencia
	And SVL.siAgrupador = 73280
	And TGDD.iIdObjeto = @iIdObjeto
End
Else
Begin
	Select @nvchQueryG = N'If not exists ( Select * From CatReporte Where iCodReporte = ' + cast( TGDD.iIdObjeto as varchar) + N' )' + @nvchEnter +
	@nvchTab + N'Insert Into CatReporte (iCodReporte, vchNombreReporte, vchDescripcion, tiActivo, ' + @nvchEnter +
	@nvchTab + N'vchRuta, siAgrupador, vchComentario, iIdOrigenReporte, ' + @nvchEnter +
	@nvchTab + N'iIdOrientacionReporte, vchVersionRPT, iIdBaseDatos, iIdTipoConexion ) ' + @nvchEnter +
	@nvchTab + N'Values (' + cast(TGDD.iIdObjeto as varchar) + N', ''' + TGDD.vchNombre + N''', ' +	
	N'''' + cast(TGDD.vchDescripcion as varchar(100)) + N''', 1, ' + @nvchEnter +
	@nvchTab + N'''' + cast(TGDD.vchRutaArchivoFisico as varchar(100)) + N''', 0, ' + N'''''' + N', 10600, ' + @nvchEnter +
	@nvchTab + N'11571, ''' + TGDD.vchVersion + ''', 73280, 73660) ' + @nvchEnter
	From bd_sicyproh..trangraldiccionariodatos TGDD
	Where TGDD.iIdObjeto = @iIdObjeto
	If @iExiste = 1
	Begin
		Select @nvchQueryG = @nvchQueryG + N'Else ' + @nvchEnter + 
		@nvchTab + N'Update CatReporte  ' + @nvchEnter +
		@nvchTab + N'Set vchNombreReporte = ''' + CR.vchNombreReporte + N''', ' + @nvchEnter +
		@nvchTab + N'vchDescripcion = ''' + CR.vchDescripcion + N''', ' + @nvchEnter +
		@nvchTab + N'tiActivo = 1, ' + @nvchEnter +
		@nvchTab + N'vchRuta = ''' + CR.vchRuta + N''', ' + @nvchEnter +
		@nvchTab + N'siAgrupador = ' + cast( CR.siAgrupador as varchar) + N', ' + @nvchEnter +
		@nvchTab + N'vchComentario = ''' + CR.vchComentario + N''', ' + @nvchEnter +
		@nvchTab + N'iIdOrigenReporte = ' + cast( CR.iIdOrigenReporte as varchar) + N', ' + @nvchEnter +
		@nvchTab + N'iIdOrientacionReporte = ' + cast( CR.iIdOrientacionReporte as varchar) + N', ' + @nvchEnter +
		@nvchTab + N'vchVersionRPT = ''' + CR.vchVersionRPT + N''', ' + @nvchEnter +
		@nvchTab + N'iIdBaseDatos = ' + cast( CR.iIdBaseDatos as varchar) + N', ' + @nvchEnter +
		@nvchTab + N'iIdTipoConexion = ' + cast( CR.iIdTipoConexion as varchar) + @nvchEnter +
		@nvchTab + N'Where iCodReporte = ' + cast( @iIdObjeto as varchar) + @nvchEnter
		From bd_sicyproh..CatReporte CR
		Where iCodReporte = @iIdObjeto
	End
	Select @nvchQueryG = @nvchQueryG + N'GO' + @nvchEnter 
	
End

Set @nvchQueryE = N''

-- Obtengo los nombres de los campos
Insert Into @tmpCamposEspObjeto (NombreCampo,iIdEspDicDatos, vchDescripcion,
iIdTipoBitacoraModificacion, iIdTipoValor, iIdEstatus, 
iIdTipoFiltroMultiSucursal, iOrden)
Select TEDD.vchNombre As NombreCampo, TEDD.iIdEspDicDatos, cast( TEDD.vchDescripcion as varchar(100)),
6260, 10119, 3343,
10750, TEDD.iOrden
From bd_sicyproh..tranespdiccionariodatos TEDD, bd_sicyproh..tranGralDiccionarioDatos TGDD
Where TEDD.iIdObjeto = TGDD.iIdObjeto
And TGDD.siTipoObjeto = 153003 --Tabla
And TEDD.iIdObjeto = convert(int, @iIdObjeto)

-- Muestro los inserts
If exists ( Select * From @tmpCamposEspObjeto ) --Tiene registro la variable. Hay campos asociados a la tabla
Begin
	Select @nvchQueryG + @nvchEnter + @nvchEnter as N'Inserts generados: '
	Union all
	Select N'If not exists ( Select * From CatEspObjeto Where iIdEspObjeto = ' + convert(nvarchar, iIdEspDicDatos ) + N' )' + @nvchEnter +
	@nvchTab + N'Insert Into CatEspObjeto (iIdEspObjeto, iIdGralObjeto, vchNombre, vchComentario, ' + @nvchEnter +
	@nvchTab + N'tiActivo, iIdTipoBitacoraModificacion, iIdTipoValor, iIdEstatus, ' + @nvchEnter +
	@nvchTab + N'iIdTipoFiltroMultiSucursal, iOrden) ' + @nvchEnter +
	@nvchTab + N'Values (' + convert(nvarchar, iIdEspDicDatos ) + N', ' + convert(nvarchar,@iIdObjeto) + N', ''' + NombreCampo + N''', ''' + cast(vchDescripcion as nvarchar(100)) + N''', ' + @nvchEnter +
	@nvchTab + N'1, ' + convert(nvarchar, iIdTipoBitacoraModificacion ) + N', ' + convert(nvarchar, iIdTipoValor ) + N', ' + convert(nvarchar,iIdEstatus) + N', ' + @nvchEnter +
	@nvchTab + convert(nvarchar, iIdTipoFiltroMultiSucursal ) + N', ' + convert(nvarchar, iOrden) +
	N')' + @nvchEnter + 
	N'GO' + @nvchEnter 
	From @tmpCamposEspObjeto
End
Else --No hay campos, el objeto no es una tabla
Begin
	Select @nvchQueryG + @nvchEnter + @nvchEnter
End

_Fin:
If @@NESTLEVEL <= 1 -- solo cuando no exista anidamiento de procedimientos apagamos el Set nocount   
Begin    
	Set nocount off     
End 
Return (@iError)
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinScriptLlavesForaneas' ) 
	Drop procedure dbo.sp_ppinScriptLlavesForaneas
GO

Create Procedure dbo.sp_ppinScriptLlavesForaneas
(
	@vchTabla sysname,
	@vchResultado varchar(8000) output
)
AS 
/*
** Nombre:					sp_ppinScriptLlavesForaneas
** Prop�sito:				Retornar el script de construccion o destruccion de las
**							llaves foraneas de una tabla
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			FDCG
** Csd creaci�n:				?
** Fecha modificaci�n: 		29/Mayo/2012
** Autor modificaci�n: 		CJFU
** Csd modificaci�n:			173
** Compatibilidad:			1.75
** Revisi�n:					1
*/
Begin

	DECLARE @tmpFK table(
		TablaF sysname,
		TablaR sysname,
		ColF sysname,
		ColR sysname,
		FKName sysname)

	-- obtengo las llaves foraneas en @vchForeign
	Declare @vchForeign varchar(8000), @FKName sysname, @vchColumnasF varchar(4000), @vchColumnasR varchar(4000), @ColF sysname, @ColR sysname
	Declare @vchTemp varchar(1000), @TablaR sysname

	Insert into @tmpFK
	Select TablaF.name AS TablaF, TablaR.name AS TablaR, ColF.name AS ColF, ColR.name AS ColR, ofk.name AS FKName
	From sysforeignkeys fk, sysobjects ofk, sysobjects TablaF, sysobjects TablaR, 
	syscolumns ColF, syscolumns ColR
	Where TablaF.name = @vchTabla
	And ofk.id = fk.constid
	And TablaF.id = fk.fkeyid
	And TablaR.id = fk.rkeyid
	And ColF.id = TablaF.id And ColF.colid = fk.fkey
	And ColR.id = TablaR.id And ColR.colid = fk.rkey
	order by FKName

	Set @vchForeign = ''
	While Exists ( Select * From @tmpFK )
	Begin
		Select Top 1 @FKName = FKName From @tmpFK
		Set @vchColumnasF = ''
		Set @vchColumnasR = ''
		While Exists ( Select * From @tmpFK Where FKName = @FKName )
		Begin
			Select Top 1 @ColF = ColF, @ColR = ColR, @TablaR = TablaR From @tmpFK Where FKName = @FKName
			Delete From @tmpFK Where ColF = @ColF And ColR = @ColR And TablaR = @TablaR And FKName = @FKName
			Set @vchColumnasF = @vchColumnasF + @ColF + ', '
			Set @vchColumnasR = @vchColumnasR + @ColR + ', '
		End
		
		Set @vchColumnasF = LEFT(@vchColumnasF, LEN(@vchColumnasF) - 1)
		Set @vchColumnasR = LEFT(@vchColumnasR, LEN(@vchColumnasR) - 1)
		Set @vchTemp = 'Constraint ' + @FKName + ' Foreign Key (' + @vchColumnasF + ') '
		Set @vchTemp = @vchTemp + 'References ' + @TablaR + ' (' + @vchColumnasR + ')'
		Set @vchForeign = @vchForeign + char(9) + @vchTemp + ',' + char(13) 
	End

	Select @vchResultado = Case When Len(@vchForeign) >=2 Then Left(@vchForeign, Len(@vchForeign) - 2) Else @vchForeign End
End
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinScriptTabla' ) 
	Drop procedure dbo.sp_ppinScriptTabla
GO

Create Procedure sp_ppinScriptTabla
(
	@vchTabla sysname,
	@tiSoloUnRegistro tinyint = 0

)
AS
/*
** Nombre:					sp_ppinScriptTabla
** Prop�sito:				
** Campos:					@vchTabla 		Nombre de la tabla
**						@tiSoloUnRegistro	Indica si el script de creacci�n se retorna uno o varios registros:
**									0-->Se crea un registro por cada fila.
**									1-->Se crea un solo registro para todo el script de creacci�n de la tabla
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?	
** Fecha modificaci�n: 		10/Mayo/2010
** Autor modificaci�n: 		CJFU
** Csd modificaci�n:		csd173
** Compatibilidad:			1.75
** Revisi�n:					1
*/
Set nocount on

Declare @tmpTablaVariasLineas as table (
	iIdLinea int not null identity(1,1),
	vchLinea nvarchar(MAX)
)

Declare @tmpTablaUnRegistro as table (
	vchLinea nvarchar(MAX)
)

Declare @iContador int
Declare @nvchNuevaLinea nvarchar(2)

Set @nvchNuevaLinea = char(13) + char(10)
-- Obtengo las foreign keys
Declare @foreign nvarchar(MAX)
Exec sp_ppinScriptLlavesForaneas @vchTabla, @foreign output

--Create table
Insert Into @tmpTablaVariasLineas (vchLinea)
Select 'Create ' + 
Case o.xtype When 'U' Then 'Table' When 'P' Then 'Procedure' Else '??' End + ' ' +
@vchTabla + char(13) + '('
From sysobjects o
Where o.name = @vchTabla

-- Obtengo campos
Insert Into @tmpTablaVariasLineas (vchLinea)
Select char(9) + c.name + ' ' +									-- Nombre
dbo.sp_ppinTipoLongitud(t.xtype, c.length, c.isnullable) +			-- Tipo(longitud)
Case When c.colstat & 1 = 1										-- Identity (si aplica)
	Then ' Identity(' + convert(varchar, ident_seed(@vchTabla)) + ',' + Convert(varchar, ident_incr(@vchTabla)) + ')' 
	Else '' 
End + 
Case When not od.name is null									-- Defaults (si aplica)
	Then ' Constraint ' + od.name + ' Default ' + replace(replace(cd.text, '((', '('), '))', ')')
	Else ''
End + ', '
from sysobjects o, syscolumns c
LEFT OUTER JOIN sysobjects od On od.id = c.cdefault LEFT OUTER join syscomments cd On cd.id = od.id, 
systypes t
where o.id = object_id(@vchTabla)
and o.id = c.id
and c.xtype = t.xtype
order by c.colid


-- Obtengo PKs y UKs
Insert Into @tmpTablaVariasLineas (vchLinea)
select char(9) + 'Constraint ' + o.name + ' ' +
Case o.xtype When 'PK' Then 'Primary Key' Else 'Unique' End + ' ' +
dbo.sp_ppinCamposIndice (db_name(), @vchTabla, i.indid) + ', '
from sysobjects o, sysindexes i
where o.parent_obj = object_id(@vchTabla)
and o.xtype in ('PK','UQ')
and i.id = o.parent_obj
and o.name = i.name

-- Obtengo Check constraints
Insert Into @tmpTablaVariasLineas (vchLinea)
select char(9) + 'Constraint ' + o.name + ' Check ' + c.text + ', '
from sysobjects o, syscomments c
where o.parent_obj = object_id(@vchTabla)
and o.xtype in ('C')
and o.id = c.id

Insert Into @tmpTablaVariasLineas (vchLinea)
Select @foreign

Insert Into @tmpTablaVariasLineas (vchLinea)
Select ')'


If @tiSoloUnRegistro = 0
Begin
	Select vchLinea
	From @tmpTablaVariasLineas
End
Else
Begin
	--Insertar un registro
	--el cual va a ser el �nico registro que tendr� la tabla
	Insert into @tmpTablaUnRegistro (vchLinea)
	Values('')

	Set @iContador = -1
	--Hacer un ciclo para concatenar todos los registro en uno solo
	While exists ( Select *	From @tmpTablaVariasLineas
		Where iIdLinea > @iContador )
	Begin
		--Obtener el id de la siguiente linea a concatenar
		Select top 1 @iContador = iIdLinea
		From @tmpTablaVariasLineas
		Where iIdLinea > @iContador
		Order by iIdLinea

		Update @tmpTablaUnRegistro
		Set vchLinea = tmpUR.vchLinea + @nvchNuevaLinea + tmpTVL.vchLinea
		From @tmpTablaUnRegistro tmpUR, @tmpTablaVariasLineas tmpTVL
		Where tmpTVL.iIdLinea = @iContador		
	End

	--Retornar el resultado a crystal
	Select vchLinea
	From @tmpTablaUnRegistro

End

Set nocount off
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinSelectFromWhereForaneo' ) 
	Drop procedure dbo.sp_ppinSelectFromWhereForaneo
GO
Create Procedure sp_ppinSelectFromWhereForaneo
(
	@vchTabla varchar(100),
	@vchAliasTabla varchar(5) = 'A'
)
AS
/*
** Nombre:					sp_ppinSelectFromWhereForaneo
** Prop�sito:					SP que genera un Select * a partir de una tabla.
**							El select se genera con el From y el Where seg�n las llaves for�neas de la tabla
** Campos:					@vchTabla: Tabla a la que se har� el select
**							@vchAliasTabla: Alias que tendr� la tabla en el select
** Dependencias: 				
**												
** Fecha creaci�n:				17/Mayo/2007
** Autor creaci�n:				FDCG
** Csd creaci�n:				csd171
** Fecha modificaci�n: 	
** Autor modificaci�n: 	
** Csd modificaci�n:			
** Compatibilidad:				1.75
** Revisi�n:					0
*/
Set nocount On
Declare @TablaR sysname, @ColF sysname, @ColR sysname, @vchAlias varchar(5)
Declare @vchFrom varchar(4000), @vchWhere varchar(4000), @iCont int, @iHayMas int

If not exists ( Select * From sysobjects where name = @vchTabla And xtype in ('U') )
Begin
	Set nocount Off
	Print 'La tabla ' + @vchTabla + ' no existe en [' + db_name() + ']'
	Return
End

If exists ( Select * From tempdb..sysobjects Where name like '#tmpForeign[_]%' ) 
	Drop table #tmpForeign

Create Table #tmpForeign
(
	iIdentity int not null identity (1,1),
	TablaF sysname,
	TablaR sysname,
	ColF sysname,
	ColR sysname,
	FKName sysname,
	vchAlias varchar(5),
	tiProceso tinyint 
)

Insert Into #tmpForeign (TablaF, TablaR, ColF, ColR, FKName, 
vchAlias, tiProceso)
Select TablaF.name, TablaR.name, ColF.name, ColR.name, ofk.name,
substring(TablaR.name, 1, 1) + substring(TablaR.name, 4, 1) , 0
From sysforeignkeys fk, sysobjects ofk, sysobjects TablaF, sysobjects TablaR, 
syscolumns ColF, syscolumns ColR
Where TablaF.name = @vchTabla
And ofk.id = fk.constid
And TablaF.id = fk.fkeyid
And TablaR.id = fk.rkeyid
And ColF.id = TablaF.id And ColF.colid = fk.fkey
And ColR.id = TablaR.id And ColR.colid = fk.rkey
order by Left(TablaR.name, 2)

Update t
Set vchAlias = Upper(vchAlias) + Cast(iIdentity As varchar)
From #tmpForeign t


If exists ( Select * From #tmpForeign Where tiProceso = 0 )
Begin
	Set @vchFrom = 'From ' + @vchTabla + ' ' + @vchAliasTabla + ', ' 
	Set @vchWhere = 'Where '
End
Else
Begin
	Print 'La tabla ' + @vchTabla + ' no tiene llaves for�neas'
	Return
End

Set @iCont = 0
While exists ( Select * From #tmpForeign Where tiProceso = 0 )
Begin
	Set @iCont = @iCont + 1
	Set @iHayMas = Case When (Select count(*) From #tmpForeign Where tiProceso = 0) > 1 Then 1 Else 0 End

	Select Top 1 @TablaR = TablaR, @ColF = ColF, @ColR = ColR, @vchAlias = vchAlias From #tmpForeign Where tiProceso = 0

	-- Armo el From 
	Select @vchFrom = @vchFrom + @TablaR + ' ' + @vchAlias  

	If @iHayMas = 1 
		Set @vchFrom = @vchFrom + ', '

	If @iCont = 3 And @iHayMas = 1
		Select @iCont = 0, @vchFrom = @vchFrom + char(13) + char(10)

	-- Armo el Where:	A.Campo = B.Campo
	Select @vchWhere = @vchWhere + @vchAliasTabla + '.' + @ColF + ' = ' + @vchAlias + '.' + @ColR

	If @iHayMas = 1 
		Set @vchWhere = @vchWhere + char(13) + char(10) + 'And '

	Update tmp Set tiProceso = 1 From #tmpForeign tmp Where TablaR = @TablaR And ColF = @ColF And ColR = @ColR And tiProceso = 0
End

If exists ( Select * From #tmpForeign Where tiProceso = 1 )
	Select 'Select * ' + char(13) + @vchFrom + ' ' + char(13) + @vchWhere
Else
	Select ''
Set nocount Off
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinInsertaObjetoError' ) 
	Drop procedure dbo.sp_ppinInsertaObjetoError
GO

Create Procedure dbo.sp_ppinInsertaObjetoError
(
	@vchNombreObjeto varchar(100) = '',
	@vchComentarioObjeto varchar(1024) = '',
	@iIdMensajeError int = 0,
	@vchMensajeError varchar(255) = '',
	@iIdTipoMensaje int = 5361,
	@tiEjecutaScript tinyint = 0
)
AS
/*
** Nombre:				sp_ppinInsertaObjetoError
** Prop�sito:				Insertar y devolver script de inserci�n de un error determinado a la tabla CatMensaje_ProcAlmacenado
** Campos:				@vchNombreObjeto: Nombre del objeto al que se le insertar� el error
**						@vchComentarioObjeto: Comentario que tendr� el objeto en caso de no existir en CatGralObjeto
**						@iIdMensajeError: ID del mensaje de error (el ID que devuelve el objeto cuando ocurre el error)
**						@vchMensajeError: La descripci�n del mensaje de error (ej: 'la factura ya existe')
**						@iIdTipoMensaje: Tipo de mensaje (catconstante, 5360, 4), por defecto, Cr�tico
**						@tiEjecutaScript: Para indicar si adem�s de generar el script, lo ejecuta (insert a catgralobjeto y/o CatMensaje_ProcAlmacenado)
** Fecha creaci�n:			2/Agosto/2007
** Autor creaci�n:			FDCG
** Csd creaci�n:			csd172
** Fecha modificaci�n: 		
** Autor modificaci�n: 		
** Csd modificaci�n:			
** Compatibilidad:			1.75
** Revisi�n:				0
*/
Declare @iError int, @iIdObjeto int, @siTipoObjeto int, @iIdTipoObjeto_CC int
Declare @vchScript varchar(4000)
Set @iError = 0

Set nocount on

If len(ltrim(rtrim(@vchNombreObjeto))) = 0 Or @iIdMensajeError = 0 Or @vchMensajeError = ''
Begin
	Print 'Favor de proporcionar los siguientes par�metros: ' + char(13) + char(10) + char(13) + char(10)
	Print '@vchNombreObjeto: Nombre del objeto al que se le insertar� el error'
	Print '@vchComentarioObjeto: Comentario que tendr� el objeto en caso de no existir en CatGralObjeto'
	Print '@iIdMensajeError: ID del mensaje de error (el ID que devuelve el objeto cuando ocurre el error)'
	Print '@iIdTipoMensaje: Tipo de mensaje (catconstante, 5360, 4), por defecto, Cr�tico'
	Print '@tiEjecutaScript: Para indicar si adem�s de generar el script, lo ejecuta (insert a catgralobjeto y/o CatMensaje_ProcAlmacenado)'
	Set @iError = -1
	Goto _Fin
End

-- Si existe el mensaje, env�o error
If exists ( Select * From bd_sicyproh..CatMensaje_ProcAlmacenado Where iIdMensaje = @iIdMensajeError )
Begin
	Print 'WARNING: El mensaje con ID ' + convert(varchar, @iIdMensajeError) + ' ya existe en la tabla bd_sicyproh..CatMensaje_ProcAlmacenado' + char(13) + char(10) + char(13) + char(10)
	Set @tiEjecutaScript = 0
End

-- Si no existe el objeto en el diccionario de datos, env�o error
If not exists ( Select * From bd_sicyproh..trangraldiccionariodatos Where vchNombre = @vchNombreObjeto )
Begin
	Print 'ERROR: El objeto ' + @vchNombreObjeto + ' no existe en el diccionario de datos'
	Set @iError = -1
	Goto _Fin
End

-- Si no existe el objeto en CatGralObjeto, lo inserto
Select @iIdObjeto = iIdObjeto, @siTipoObjeto = siTipoObjeto, @iIdTipoObjeto_CC = DD.iReferencia 
From bd_sicyproh..trangraldiccionariodatos GD, bd_sicyproh..CatConstanteDiccionarioDatos DD
Where GD.vchNombre = @vchNombreObjeto
And GD.siTipoObjeto = DD.siconsecutivo

If @iIdObjeto Is null
Begin
	Print 'ERROR: al obtener el ID del objeto'
	Set @iError = -1
	Goto _Fin
End

if not exists ( Select * From bd_sicyproh..catgralobjeto where iIdGralObjeto = @iIdObjeto )
Begin
	-- Armo la cadena del Insert a catgralobjeto y lo imprimo
	Set @vchScript = 'If not exists ( Select * From CatGralObjeto Where iIdGralObjeto = ' + convert(varchar, @iIdObjeto) + ' )' + char(13) + char(10) 
	Set @vchScript = @vchScript + char(9) + 'Insert Into CatGralObjeto ( iIdGralObjeto, vchNombre, iIdTipoObjeto, tiActivo, vchComentario, tiBitacora )' + char(13) + char(10)
	Set @vchScript = @vchScript + char(9) + 'Values (' + convert(varchar, @iIdObjeto) + ', ''' + @vchNombreObjeto + ''', ' + convert(varchar, @iIdTipoObjeto_CC) + ', 1, ''' + @vchComentarioObjeto + ''', 0)' + char(13) + char(10)
	Set @vchScript = @vchScript + 'GO' + char(13) + char(10) + char(13) + char(10)

	Print @vchScript
	Set @vchScript = ''

	If @tiEjecutaScript <> 0
	Begin
		Insert Into bd_sicyproh..CatGralObjeto ( iIdGralObjeto, vchNombre, iIdTipoObjeto, tiActivo, vchComentario, tiBitacora )
		Values (@iIdObjeto, @vchNombreObjeto, @iIdTipoObjeto_CC, 1, @vchComentarioObjeto, 0)
	End
End


-- Insertar y devolver script de inserci�n a CatMensaje_ProcAlmacenado
Set @vchScript = 'If not exists ( Select * From CatMensaje_ProcAlmacenado Where iIdMensaje = ' + convert(varchar, @iIdMensajeError) + ' )' + char(13) + char(10)
Set @vchScript = @vchScript + char(9) + 'Insert Into CatMensaje_ProcAlmacenado (iIdMensaje, vchDescripcion, iIdGralObjeto, tiActivo, iIdTipoMensage)' + char(13) + char(10)
Set @vchScript = @vchScript + char(9) + 'Values (' + convert(varchar, @iIdMensajeError) + ', ''' + @vchMensajeError + ''', ' + convert(varchar, @iIdObjeto) + ', 1, ' + convert(varchar, @iIdTipoMensaje) + ')' + char(13) + char(10)
Set @vchScript = @vchScript + 'GO' + char(13) + char(10) + char(13) + char(10)

Print @vchScript
Set @vchScript = ''

If @tiEjecutaScript <> 0
Begin
	Insert Into bd_sicyproh..CatMensaje_ProcAlmacenado (iIdMensaje, vchDescripcion, iIdGralObjeto, tiActivo, iIdTipoMensage)
	Values (@iIdMensajeError, @vchMensajeError, @iIdObjeto, 1, @iIdTipoMensaje )
End


_Fin:
Set nocount off
Return @iError
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinSeparaParametro_ScrObj' ) 
	Drop procedure dbo.sp_ppinSeparaParametro_ScrObj
GO

Create procedure dbo.sp_ppinSeparaParametro_ScrObj
@vchParametro varchar(1024)
AS
/*
** Nombre:							sp_ppinSeparaParametro_ScrObj
** Prop�sito:						Procedimiento almacenado que separa los parametros para el procedimiento sp_ppinGeneraScriptObjeto
**											Por que en algunas instalaciones del 2008 no separa los parametros por cada coma.
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			19/Junio/2008
** Autor creaci�n:			CJFU
** Csd creaci�n:				csd172
** Fecha modificaci�n: 		
** Autor modificaci�n: 		
** Csd modificaci�n:			
** Compatibilidad:			1.75
** Revisi�n:					0
*/
Declare @tiHayComa tinyint
Set @tiHayComa = 0

Declare @vchParametro1 varchar(50)
Declare @vchParametro2 varchar(50)
Declare @vchParametro3 varchar(50)
Declare @vchParametro4 varchar(50)
Declare @vchParametro5 varchar(50)
Declare @vchParametro6 varchar(50)
Declare @vchParametro7 varchar(50)
Declare @vchParametro8 varchar(50)
Declare @vchParametro9 varchar(50)
Declare @vchParametro10 varchar(50)

Declare @iParametroNum1 int
Declare @iParametroNum2 int

Set @vchParametro1 = ''
Set @vchParametro2  = ''
Set @vchParametro3  = ''
Set @vchParametro4  = ''
Set @vchParametro5  = ''
Set @vchParametro6  = ''
Set @vchParametro7  = ''
Set @vchParametro8  = ''
Set @vchParametro9  = ''
Set @vchParametro10  = ''

Set @iParametroNum1 = 0
Set @iParametroNum2  = 0

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el primer parametro
Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
If @tiHayComa >= 1
Begin
	Select @vchParametro1 = substring ( @vchParametro, 1, @tiHayComa - 1 )
	--Eliminar de la cadena el parametro obtenido
	Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
End
Else --si no hay coma
Begin
	Select @vchParametro1 = @vchParametro
	--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
	Select @vchParametro = ''
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el segundo parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro2 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro2 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el tercer parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro3 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro3 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el cuarto parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro4 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro4 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el quinto parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro5 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro5 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el sexto parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro6 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro6 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el septimo parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro7 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro7 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el octavo parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro8 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro8 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el noveno parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro9 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro9 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final
Set @vchParametro = ltrim(rtrim(@vchParametro))

--Obtener el decimo parametro, siempre y cuando la variable <<@vchParametro>> tenga datos
If @vchParametro <> ''
Begin
	Select @tiHayComa = PATINDEX ( '%,%' , @vchParametro )
	If @tiHayComa >= 1
	Begin
		Select @vchParametro10 = substring ( @vchParametro, 1, @tiHayComa - 1 )
		--Eliminar de la cadena el parametro obtenido
		Select @vchParametro = substring ( @vchParametro, @tiHayComa + 1, 1000 )
	End
	Else --si no hay coma
	Begin
		Select @vchParametro10 = @vchParametro
		--Eliminar de la cadena el parametro obtenido, vacio por que ya no existen m�s parametros
		Select @vchParametro = ''
	End
End

--eliminar los espacios al inicio y al final de todos los parametros
Set @vchParametro1 = ltrim(rtrim(@vchParametro1))
Set @vchParametro2 = ltrim(rtrim(@vchParametro2))
Set @vchParametro3 = ltrim(rtrim(@vchParametro3))
Set @vchParametro4 = ltrim(rtrim(@vchParametro4))
Set @vchParametro5 = ltrim(rtrim(@vchParametro5))
Set @vchParametro6 = ltrim(rtrim(@vchParametro6))
Set @vchParametro7 = ltrim(rtrim(@vchParametro7))
Set @vchParametro8 = ltrim(rtrim(@vchParametro8))
Set @vchParametro9 = ltrim(rtrim(@vchParametro9))
Set @vchParametro10 = ltrim(rtrim(@vchParametro10))

--Executa el procemiento almacenado dependiendo del parametro enviado
If isnumeric(@vchParametro1) = 1
Begin
	Select @iParametroNum1 = cast(@vchParametro1 as int)

	Exec sp_ppinGeneraScriptObjeto
	@iAccion = @iParametroNum1,
	@vchValor1 = @vchParametro2,
	@vchValor2 = @vchParametro3,
	@vchValor3 = @vchParametro4,
	@vchValor4 = @vchParametro5,
	@vchValor5 = @vchParametro6,
	@vchValor6 = @vchParametro7
End
Else
Begin
	Exec sp_ppinGeneraScriptObjeto
	@iAccion = 0,
	@vchValor1 = '',
	@vchValor2 = '',
	@vchValor3 = '',
	@vchValor4 = '',
	@vchValor5 = '',
	@vchValor6 = ''
End
GO

If exists ( Select * From sysobjects Where name = 'sp_UpdateCVS' ) 
	Drop procedure dbo.sp_UpdateCVS
GO

CREATE Procedure dbo.sp_UpdateCVS
As
	Exec xp_CMDShell 'D:\Sicyproh\Sicyproh\bin\UpdateReportesCVS.bat'
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinDependenciaSP' ) 
	Drop procedure sp_ppinDependenciaSP
GO

Create procedure sp_ppinDependenciaSP
(
	@vchSP varchar(255), 
	@niveles int
)
AS
/*
** Nombre:				sp_ppinDependenciaSP
** Prop�sito:				Devuelve las dependencias por exec dentro de un SP seg�n la cantidad de niveles m�ximos indicados.
**						O sea, si el procedimiento p01 llama al procedimiento p02 y �ste a su vez al p03,
**						se devolver�n los tres procedimientos llamando a este SP con los par�metros: sp_ppinDependenciaSP 'p01', 3
** Campos:				@vchSP: Nombre del procedimiento (nivel 0)
**						@niveles: cantidad m�xima de niveles a escanear
** Fecha creaci�n:			12/Enero/2009
** Autor creaci�n:			FDCG
** Revisi�n:				0
*/
Set nocount on

Declare @iNivel int, @id int
Set @iNivel = 1

If object_id(@vchSP) Is null
Begin
	print 'El objeto [' + @vchSP + '] no existe en [' + db_name() + ']'
	Return 1
End

Create Table #tmpDepends
(
	id int,
	name sysname,
	nivel int
)

Insert Into #tmpDepends (id, name, nivel)
Select Object_id(@vchSP), Object_name(Object_id(@vchSP)), 0

While (@iNivel <= @niveles)
Begin
	Insert Into #tmpDepends (id, name, nivel)
	Select distinct d.depid, o.name, @iNivel
	From sysdepends d, sysobjects o, #tmpDepends t
	Where d.id = t.id
	and d.depid = o.id
	and o.xtype = 'P'
	and not exists ( Select * From #tmpDepends t2 where t2.id = d.depid )

	Set @iNivel = @iNivel + 1
End

Print 'Dependencias por exec del objeto ' + @vchSP + ':'
Select Convert(varchar(100), Space(nivel * 2) + name) as NombreNivel, nivel, id, name 
From #tmpDepends

Set nocount off
Return 0
GO

If exists ( Select * From sysobjects Where name = 'SP_PpInGenera_ScriptRpt' ) 
	Drop procedure dbo.SP_PpInGenera_ScriptRpt
GO
CREATE Procedure dbo.SP_PpInGenera_ScriptRpt (
	@vchNombreReporte varchar(80)
)
AS
/*
** Nombre:					SP_PpInGenera_ScriptRpt
** Prop�sito:					Genera el script que insert el reporte al sistema
** Par�metros:					@vchNombreReporte	Nombre del archivo f�sico del reporte
**
** Dependencias:				
**
** Fecha creaci�n:			24/Febrero/2010
** Autor creaci�n:			CJFU
** csd creaci�n: 			csd172
** Fecha modificaci�n:			24/Marzo/2010
** Autor modificacion:			CJFU
** csd modificaci�n:			csd173
** Compatibilidad:			1.75
** Revision:				1
*/

Set Nocount On

Declare @Enter char(2)
Declare @vchEnter nvarchar(20)
Declare @iIdReporte int
Declare @vchTituloReporte varchar(100)
Select @Enter = char(13) + char(10)
Set @vchEnter = 'char(13) + char(10)' --Donde se quiere que el script retornado haga enter's
Set @iIdReporte = 0
Set @vchTituloReporte = ''



If exists ( Select * From bd_sicyproh..CatReporte Where vchNombreReporte = @vchNombreReporte )
Begin
	--Almacenar el id del reporte
	Select @iIdReporte = iCodReporte, @vchTituloReporte = cast(vchDescripcion as varchar(100))
	From bd_sicyproh..CatReporte
	Where vchNombreReporte = @vchNombreReporte
	
	Select 'If Exists (Select * From CatReporte Where iCodReporte = ' + 
	Convert(VarChar(8000),iCodReporte) + ')' + @Enter + Char(9) + 
	'Update CatReporte' + @Enter + Char(9) + 
	'Set ' + 
	'vchNombreReporte = ' + 
	Case When TGDD.vchNombre Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchNombre,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'vchDescripcion = ' + 
	Case When TGDD.vchAlias Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchAlias,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'tiActivo = ' + 
	Case When tiActivo Is Null Then 'Null' Else Convert(VarChar(8000), tiActivo) End + ', ' + @Enter + char(9) + 'vchRuta = ' + 
	Case When vchRuta Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchRuta,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'siAgrupador = ' + 
	Case When siAgrupador Is Null Then 'Null' Else Convert(VarChar(8000), siAgrupador) End + ', ' + @Enter + char(9) + 'vchComentario = ' + 
	Case When TGDD.vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchDescripcion,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'iIdOrigenReporte = ' + 
	Case When iIdOrigenReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdOrigenReporte) End + ', ' + @Enter + char(9) + 'iIdOrientacionReporte = ' + 
	Case When iIdOrientacionReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdOrientacionReporte) End + ' '
	 + @Enter + char(9) + 'Where iCodReporte = ' + 
	Convert(VarChar(8000),iCodReporte) 
	 + @Enter + 'Else' + @Enter + char(9) +
	'Insert Into CatReporte(iCodReporte, vchNombreReporte, vchDescripcion, tiActivo, ' + @Enter + char(9) +
	'vchRuta, siAgrupador, vchComentario, iIdOrigenReporte, '+ @Enter + char(9) +
	'iIdOrientacionReporte)' + @Enter + char(9) + 'Values (' + 
	Case When iCodReporte Is Null Then 'Null' Else Convert(VarChar(8000), iCodReporte) End + ', ' + 
	Case When TGDD.vchNombre Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchNombre,'''','''''')) + '''' End + ', ' + 
	Case When TGDD.vchAlias Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchAlias,'''','''''')) + '''' End + ', ' + 
	Case When tiActivo Is Null Then 'Null' Else Convert(VarChar(8000), tiActivo) End + ', ' + @Enter + char(9) + 
	Case When vchRuta Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchRuta,'''','''''')) + '''' End + ', ' + 
	Case When siAgrupador Is Null Then 'Null' Else Convert(VarChar(8000), siAgrupador) End + ', ' + 
	Case When TGDD.vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(TGDD.vchDescripcion,'''','''''')) + '''' End + ', ' + 
	Case When iIdOrigenReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdOrigenReporte) End + ', ' + @Enter + char(9) +
	Case When iIdOrientacionReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdOrientacionReporte) End + ')'
	 + 
	@Enter + 'GO' + @Enter + @Enter 
	From bd_sicyproh..CatReporte R, bd_sicyproh..TranGralDiccionarioDatos TGDD
	Where R.iCodReporte = TGDD.iIdObjeto
	And TGDD.vchNombre = @vchNombreReporte
End

else If exists ( Select * From bd_sicyproh..TranGralDiccionarioDatos Where vchNombre = @vchNombreReporte )
Begin
	--Almacenar el id del reporte
	Select @iIdReporte = iIdObjeto,
	@vchTituloReporte = cast(vchAlias as varchar(100))
	From bd_sicyproh..TranGralDiccionarioDatos
	Where vchNombre = @vchNombreReporte

	Select 'If Exists (Select * From CatReporte Where iCodReporte = ' + 
	Convert(VarChar(8000),iIdObjeto) + ')' + @Enter + Char(9) + 
	'Update CatReporte' + @Enter + Char(9) + 
	'Set ' + 
	'vchNombreReporte = ' + 
	Case When vchNombre Is Null Then 'Null' Else '''' + Convert(VarChar(80), Replace(vchNombre,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'vchDescripcion = ' + 
	Case When vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchDescripcion,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'tiActivo = ' + 
	'1, ' + @Enter + char(9) + 'vchRuta = ' + 
	Case When vchRutaArchivoFisico Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchRutaArchivoFisico,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'siAgrupador = ' + 
	'0, ' + @Enter + char(9) + 'vchComentario = ' + --siAgrupador
	Case When vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchDescripcion,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'iIdOrigenReporte = ' + 
	'10600 '  --iIdOrigenReporte
	 + @Enter + char(9) + 'Where iCodReporte = ' + 
	Convert(VarChar(8000),iIdObjeto) 
	 + @Enter + 'Else' + @Enter + char(9) +
	'Insert Into CatReporte(iCodReporte, vchNombreReporte, vchDescripcion, tiActivo, ' + @Enter + char(9) +
	'vchRuta, siAgrupador, vchComentario, iIdOrigenReporte, ' + @Enter + char(9) +
	'iIdOrientacionReporte )' + @Enter + char(9) + 'Values (' + @Enter + char(9) + 
	Case When iIdObjeto Is Null Then 'Null' Else Convert(VarChar(8000), iIdObjeto) End + ', ' + 
	Case When vchNombre Is Null Then 'Null' Else '''' + Convert(VarChar(80), Replace(vchNombre,'''','''''')) + '''' End + ', ' + 
	Case When vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchDescripcion,'''','''''')) + '''' End + ', ' + 
	'1, ' + @Enter + char(9) +  --tiActivo
	Case When vchRutaArchivoFisico Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchRutaArchivoFisico,'''','''''')) + '''' End + ', ' + 
	'0, ' + --siAgrupador
	Case When vchDescripcion Is Null Then 'Null' Else '''' + Convert(VarChar(512), Replace(vchDescripcion,'''','''''')) + '''' End + ', ' + 
	'10600, ' + @Enter + char(9) + --iIdOrigenReporte
	'11589 )'
	 + 
	@Enter + 'GO' + @Enter + @Enter 
	From bd_sicyproh..TranGralDiccionarioDatos
	Where vchNombre = @vchNombreReporte
End

else 
Begin
	Select 'El reporte debe existir en catreporte o en TranGralDiccionarioDatos, para obtener el script.' as msgError
	Goto Fin
End

--Crear los scripts para los reportes dinamicos
--La parte de los reportes dinamicos se comento, por que se va a dejar un script en los csd que va a insertar
--los valores defaults.
/*
If @iIdReporte > 0 
Begin
	If exists ( Select * From bd_sicyproh..CatGralSubReporteDinamico
		Where iIdReporte = @iIdReporte )--Existe la configuraci�n para el reporte
	Begin
		Select 'Set identity_insert CatGralSubReporteDinamico on' + @Enter + 'GO' + @Enter

		Select 'If Exists (Select * From CatGralSubReporteDinamico Where iIdGralSubReporteDinamico = ' + 
		Convert(VarChar(8000),iIdGralSubReporteDinamico) + ')' + @Enter + Char(9) + 
		'Update CatGralSubReporteDinamico
			Set ' + 
		'iIdSubreporte = ' + 
		Case When iIdSubreporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdSubreporte) End + ', ' + @Enter + char(9) + 'iIdUbicacion = ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + @Enter + char(9) + 'iIdEstatus = ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ', ' +
		 + @Enter + char(9) + 'Where iIdGralSubReporteDinamico = ' + 
		Convert(VarChar(8000),iIdGralSubReporteDinamico) 
		 + @Enter + 'Else' + '
			Insert Into CatGralSubReporteDinamico(iIdGralSubReporteDinamico, iIdReporte, iIdSubreporte, iIdUbicacion, iIdEstatus)' + @Enter + char(9) + 'Values (' + 
		Case When iIdGralSubReporteDinamico Is Null Then 'Null' Else Convert(VarChar(8000), iIdGralSubReporteDinamico) End + ', ' + 
		Case When iIdReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdReporte) End + ', ' + 
		Case When iIdSubreporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdSubreporte) End + ', ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ')'
		 + 
		@Enter + 'GO' + @Enter + @Enter 
		From  bd_sicyproh..CatGralSubReporteDinamico
		Where iIdReporte = @iIdReporte

		Select 'Set identity_insert CatGralSubReporteDinamico off' + @Enter + 'GO' + @Enter
		Select 'Set identity_insert CatEspSubReporteDinamico on' + @Enter + 'GO' + @Enter

		Select 'If Exists (Select * From CatEspSubReporteDinamico Where iIdEspSubReporteDinamico = ' + 
		Convert(VarChar(8000),iIdEspSubReporteDinamico) + ')' + @Enter + Char(9) + 
		'Update CatEspSubReporteDinamico
			Set ' + 
		'iIdTipoValor = ' + 
		Case When iIdTipoValor Is Null Then 'Null' Else Convert(VarChar(8000), iIdTipoValor) End + ', ' + @Enter + char(9) + 'tiNumOrden = ' + 
		Case When tiNumOrden Is Null Then 'Null' Else Convert(VarChar(8000), tiNumOrden) End + ', ' + @Enter + char(9) + 'vchNombreValor = ' + 
		Case When vchNombreValor Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchNombreValor,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'vchValor = ' + 
		Case When vchValor Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchValor,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'iIdEstatus = ' + 
		Case When CERD.iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), CERD.iIdEstatus) End + ', ' + 
		 + @Enter + char(9) + 'Where iIdEspSubReporteDinamico = ' + 
		Convert(VarChar(8000),iIdEspSubReporteDinamico) 
		 + @Enter + 'Else' + '
			Insert Into CatEspSubReporteDinamico(iIdEspSubReporteDinamico, iIdGralSubReporteDinamico, iIdTipoValor, tiNumOrden, vchNombreValor, vchValor, iIdEstatus)' + @Enter + char(9) + 'Values (' + 
		Case When iIdEspSubReporteDinamico Is Null Then 'Null' Else Convert(VarChar(8000), iIdEspSubReporteDinamico) End + ', ' + 
		Case When CERD.iIdGralSubReporteDinamico Is Null Then 'Null' Else Convert(VarChar(8000), CERD.iIdGralSubReporteDinamico) End + ', ' + 
		Case When iIdTipoValor Is Null Then 'Null' Else Convert(VarChar(8000), iIdTipoValor) End + ', ' + 
		Case When tiNumOrden Is Null Then 'Null' Else Convert(VarChar(8000), tiNumOrden) End + ', ' + 
		Case When vchNombreValor Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchNombreValor,'''','''''')) + '''' End + ', ' + 
		Case When vchValor Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchValor,'''','''''')) + '''' End + ', ' + 
		Case When CERD.iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), CERD.iIdEstatus) End + ')'
		 + 
		@Enter + 'GO' + @Enter + @Enter 
		From bd_sicyproh..CatEspSubReporteDinamico CERD, bd_sicyproh..CatGralSubReporteDinamico CGRD
		Where CERD.iIdGralSubReporteDinamico = CGRD.iIdGralSubReporteDinamico
		And CGRD.iIdReporte = @iIdReporte

		Select 'Set identity_insert CatEspSubReporteDinamico off' + @Enter + 'GO' + @Enter

		Select 'Set identity_insert CatReporteFormulaDinamica on' + @Enter + 'GO' + @Enter

		Select 'If Exists (Select * From CatReporteFormulaDinamica Where iIdCatReporteFormulaDinamica = ' + 
		Convert(VarChar(8000),iIdCatReporteFormulaDinamica) + ')' + @Enter + Char(9) + 
		'Update CatReporteFormulaDinamica
			Set ' + 
		'iIdUbicacion = ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + @Enter + char(9) + 'iNumUbicacion = ' + 
		Case When iNumUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iNumUbicacion) End + ', ' + @Enter + char(9) + 'iIdAlineacion = ' + 
		Case When iIdAlineacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdAlineacion) End + ', ' + @Enter + char(9) + 'vchNombreFormula = ' + 
		Case When vchNombreFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchNombreFormula,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'vchFormula = ' + 
		Case When vchFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchFormula,'''','''''')) + '''' End + ', ' + @Enter + char(9) + 'fltTop = ' + 
		Case When fltTop Is Null Then 'Null' Else Convert(VarChar(8000), fltTop) End + ', ' + @Enter + char(9) + 'fltLeft = ' + 
		Case When fltLeft Is Null Then 'Null' Else Convert(VarChar(8000), fltLeft) End + ', ' + @Enter + char(9) + 'fltWidth = ' + 
		Case When fltWidth Is Null Then 'Null' Else Convert(VarChar(8000), fltWidth) End + ', ' + @Enter + char(9) + 'tiTama�oFuente = ' + 
		Case When tiTama�oFuente Is Null Then 'Null' Else Convert(VarChar(8000), tiTama�oFuente) End + ', ' + @Enter + char(9) + 'iIdEstatus = ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ', ' + @Enter + char(9) + 'iIdUsuario = ' + 
		Case When iIdUsuario Is Null Then 'Null' Else Convert(VarChar(8000), iIdUsuario) End + ' ' +
		 + @Enter + char(9) + 'Where iIdCatReporteFormulaDinamica = ' + 
		Convert(VarChar(8000),iIdCatReporteFormulaDinamica) 
		 + @Enter + 'Else' + '
			Insert Into CatReporteFormulaDinamica(iIdCatReporteFormulaDinamica, iIdReporte, iIdUbicacion, iNumUbicacion, iIdAlineacion, vchNombreFormula, vchFormula, fltTop, fltLeft, fltWidth, tiTama�oFuente, iIdEstatus, iIdUsuario)' + @Enter + char(9) + 'Values (

' + 
		Case When iIdCatReporteFormulaDinamica Is Null Then 'Null' Else Convert(VarChar(8000), iIdCatReporteFormulaDinamica) End + ', ' + 
		Case When iIdReporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdReporte) End + ', ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + 
		Case When iNumUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iNumUbicacion) End + ', ' + 
		Case When iIdAlineacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdAlineacion) End + ', ' + 
		Case When vchNombreFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchNombreFormula,'''','''''')) + '''' End + ', ' + 
		Case When vchFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchFormula,'''','''''')) + '''' End + ', ' + 
		Case When fltTop Is Null Then 'Null' Else Convert(VarChar(8000), fltTop) End + ', ' + 
		Case When fltLeft Is Null Then 'Null' Else Convert(VarChar(8000), fltLeft) End + ', ' + 
		Case When fltWidth Is Null Then 'Null' Else Convert(VarChar(8000), fltWidth) End + ', ' + 
		Case When tiTama�oFuente Is Null Then 'Null' Else Convert(VarChar(8000), tiTama�oFuente) End + ', ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ', ' + 
		Case When iIdUsuario Is Null Then 'Null' Else Convert(VarChar(8000), iIdUsuario) End + ')'
		 + 
		@Enter + 'GO' + @Enter + @Enter 
		From bd_sicyproh..CatReporteFormulaDinamica
		Where iIdReporte = @iIdReporte

		Select 'Set identity_insert CatReporteFormulaDinamica off' + @Enter + 'GO' + @Enter
	End
	else If exists ( Select * From bd_sicyproh..CatGralSubReporteDinamico
		Where iIdReporte = 0 )--No existe la configuraci�n para el reporte, pero existe el template de configuracion (el reporte no registrado)
	Begin
		Select 'El reporte no existe en las tablas de reportes dinamicos, se tomaron los registro' + @Enter +
		'de la configuracion default (catreporte where iIdReporte = 0).' + @Enter +
		'Favor de hacer las modificaciones necesarias, ejecutar en el servidor de desarollo y recuperar' + @Enter +
		'nuevamente los registro para que en todos los servidor se tengan los mismos id en las tablas.'
		Select 'If not Exists (Select * From CatGralSubReporteDinamico Where iIdReporte = ' + 
		Convert(VarChar(8000), @iIdReporte) + ')' + @Enter + Char(9)	+
		'Insert Into CatGralSubReporteDinamico(iIdReporte, iIdSubreporte, iIdUbicacion, iIdEstatus)' + @Enter + char(9) + 'Values (' + 
		Convert(VarChar(8000), @iIdReporte) + ', ' + 
		Case When iIdSubreporte Is Null Then 'Null' Else Convert(VarChar(8000), iIdSubreporte) End + ', ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ')' + 
		@Enter + 'GO' + @Enter + @Enter 
		From  bd_sicyproh..CatGralSubReporteDinamico
		Where iIdReporte = 0

		Select 'If not Exists (Select * From CatReporteFormulaDinamica Where iIdReporte = ' + 
		Convert(VarChar(8000),@iIdReporte) +
		' And vchNombreFormula = ' + char(39) + vchNombreFormula + char(39) + ')' + @Enter + Char(9) + 
		'Insert Into CatReporteFormulaDinamica( iIdReporte, iIdUbicacion, iNumUbicacion, iIdAlineacion, ' + @Enter + Char(9) + 
		'vchNombreFormula, vchFormula, fltTop, fltLeft, ' + @Enter + Char(9) + 
		'fltWidth, tiTama�oFuente, iIdEstatus, iIdUsuario)' + @Enter + char(9) + 'Values (' + 		
		Convert(VarChar(8000), @iIdReporte) + ', ' + 
		Case When iIdUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdUbicacion) End + ', ' + 
		Case When iNumUbicacion Is Null Then 'Null' Else Convert(VarChar(8000), iNumUbicacion) End + ', ' + 
		Case When iIdAlineacion Is Null Then 'Null' Else Convert(VarChar(8000), iIdAlineacion) End + ', '  + @Enter + Char(9) + 
		Case When vchNombreFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchNombreFormula,'''','''''')) + '''' End + ', ' + 
		case when vchNombreFormula = 'NombreReporte' then char(39) + '"' + @vchNombreReporte + '"' + char(39)
			when vchNombreFormula = 'TituloReporte' then char(39) + '"' + @vchTituloReporte + '"' + char(39)
			else ( Case When vchFormula Is Null Then 'Null' Else '''' + Convert(VarChar(8000), Replace(vchFormula,'''','''''')) + '''' End ) end + ', ' + 
		Case When fltTop Is Null Then 'Null' Else Convert(VarChar(8000), fltTop) End + ', ' + 
		Case When fltLeft Is Null Then 'Null' Else Convert(VarChar(8000), fltLeft) End + ', '  + @Enter + Char(9) +  
		Case When fltWidth Is Null Then 'Null' Else Convert(VarChar(8000), fltWidth) End + ', ' + 
		Case When tiTama�oFuente Is Null Then 'Null' Else Convert(VarChar(8000), tiTama�oFuente) End + ', ' + 
		Case When iIdEstatus Is Null Then 'Null' Else Convert(VarChar(8000), iIdEstatus) End + ', ' + 
		Case When iIdUsuario Is Null Then 'Null' Else Convert(VarChar(8000), iIdUsuario) End + ')'
		 + 
		@Enter + 'GO' + @Enter + @Enter 
		From bd_sicyproh..CatReporteFormulaDinamica
		Where iIdReporte = 0
	End
End*/

Fin:
Set Nocount Off
GO

If exists ( Select * From sysobjects Where name = 'PpInHelpText' ) 
	Drop procedure dbo.PpInHelpText
GO

CREATE PROC dbo.PpInHelpText
@nvchObjeto NVARCHAR(255),
@nvchBaseDatos NVARCHAR(255)
AS
/*
** Nombre:					PpInHelpText
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			7/Marzo/2011
** Autor creaci�n:			CJFU
** Csd creaci�n:				csd173
** Fecha modificaci�n: 		
** Autor modificaci�n: 		
** Csd modificaci�n:			
** Compatibilidad:			1.75
** Revisi�n:					0
*/
	Declare @nvchTipoObjeto nvarchar(32)
	Declare @nvchConsulta	nvarchar(2048)
	Declare @nvchInput_sql	nvarchar(2048)
	Declare @nvchNuevaLinea nvarchar(2)
	Set @nvchNuevaLinea = CHAR(13) + CHAR(10)

	Set @nvchConsulta =  N'Select @nvchTipoObjeto = xtype '  + @nvchNuevaLinea
	Set @nvchConsulta =  @nvchConsulta + N'From ' +  @nvchBaseDatos + '..sysobjects ' + @nvchNuevaLinea
	Set @nvchConsulta =  @nvchConsulta + N'Where name  = ''' + @nvchObjeto + ''''  + @nvchNuevaLinea

	--Inicializar la variable, la cual contiene si existe el usuario en la base de datos (1) o si no existe la base de datos(0)
	Set @nvchTipoObjeto = '--'
	
	--Select @nvchConsulta as nvchConsulta
	--Ejecutar la consulta, para validar si ya esta asociado el usuario del sistema a la base de datos o no
	--Insert into #tmpCantidadReg(iCantidad)
	EXEC sp_executesql         @nvchConsulta,
	N'@nvchTipoObjeto nvarchar(32) output',
	@nvchTipoObjeto = @nvchTipoObjeto output
			
	--Select @nvchTipoObjeto as nvchTipoObjeto
	If @nvchTipoObjeto in ('V', 'P', 'FN', 'TF', 'TR') --Vistas(V), Procedimientos (P), Funciones (FN, TF), Trigers (TR)
	Begin
		/*Set @nvchConsulta =  N'Select ' + char(39) + 'Sp_HelpText ' + @nvchObjeto + char(39) + ' ' + @nvchNuevaLinea
		
		Execute PpInExecResultSet
		@cmd = @nvchConsulta,
		@dbname = @nvchBaseDatos	*/
	
		Set @nvchConsulta =  N'Select C.text ' + @nvchNuevaLinea
		Set @nvchConsulta =  @nvchConsulta + N'From ' + @nvchBaseDatos + '..sysobjects o, ' + @nvchBaseDatos + '..syscomments c ' + @nvchNuevaLinea 
		Set @nvchConsulta =  @nvchConsulta + N'Where o.id = c.id ' + @nvchNuevaLinea 
		Set @nvchConsulta =  @nvchConsulta + N'And o.name = ' + CHAR(39) + @nvchObjeto + CHAR(39) + @nvchNuevaLinea 

		SET @nvchInput_sql = N'EXEC ' + @nvchBaseDatos + '..sp_executesql @stmt= @nvchConsulta'
		EXEC sp_executesql         @nvchInput_sql,         N'@nvchConsulta NVARCHAR(MAX)',         @nvchConsulta

		--EXEC sp_execute @nvchConsulta
	End
	If @nvchTipoObjeto = 'U' --Tablas (U)
	Begin
		Set @nvchConsulta =  N'Exec sp_ppinScriptTabla ' + @nvchNuevaLinea
		Set @nvchConsulta =  @nvchConsulta + N'@vchTabla	= ''' + @nvchObjeto	+ CHAR(39) + ', ' + @nvchNuevaLinea --Nombre de la Tabla
		Set @nvchConsulta =  @nvchConsulta + N'@tiSoloUnRegistro = 1 ' + @nvchNuevaLinea
		/*
		Execute PpInExecResultSet
		@cmd = @nvchConsulta,
		@dbname = @nvchBaseDatos*/
		SET @nvchInput_sql = N'EXEC ' + @nvchBaseDatos + '..sp_executesql @stmt= @nvchConsulta'
		EXEC sp_executesql         @nvchInput_sql,         N'@nvchConsulta NVARCHAR(MAX)',         @nvchConsulta
	End
GO

If exists ( Select * From sysobjects Where name = 'sp_ppinScriptHerramientaSql' ) 
	Drop procedure dbo.sp_ppinScriptHerramientaSql
GO

CREATE  procedure dbo.sp_ppinScriptHerramientaSql
AS
/*
** Nombre:					sp_ppinScriptHerramientaSql		
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			7/Marzo/2011
** Autor creaci�n:			CJFU
** Csd creaci�n:				csd173
** Fecha modificaci�n: 		30/Mayo/2011
** Autor modificaci�n: 		CJFU
** Csd modificaci�n:		csd173
** Compatibilidad:			1.75
** Revisi�n:					1
*/
Declare @tmpGralObjeto table 
(
iIdGralObjeto int not null,
vchNombre varchar(80) not null
)

Declare @tmpScript table
(
	iIdGralScript int not null identity(1,1),
	vchScript nvarchar(max) not null
)

Declare @iIdGralObjeto int
Declare @vchNombre varchar(80)
Declare @nvchEnter nvarchar(2)

Set nocount on

Set @nvchEnter = char(13) + char(10)
--Revisar que sea el servidor de desarrollo
If exists ( Select * From bd_sicyproh.dbo.sysParametro 
	Where iCodParametro = 133649 And vchvalor = '1' )
Begin

	--Almacenar en una temporal los objetos a recuperar
	Insert into @tmpGralObjeto ( iIdGralObjeto, vchNombre )
	Select TGDD.iIdObjeto, TGDD.vchNombre
	From Bd_sicyproh.dbo.TrangraldiccionarioDatos TGDD
	Where TGDD.siTipoObjeto = 153001 --procedimiento almacenado Select * From CatConstanteDiccionarioDatos
	And TGDD.siIdModulo = 152197 --Aplicaci�n interna 
	And TGDD.siSubSistema = 153265 --Herramientas para sql, para agilizaci�n del desarrollo en base de datos
	And siEstatus in ( 153200, 153202) --Activo, Bloqueado
	And siBaseDatos = 153216 --Master
	
	--Contanenar el uso de la base de datos master
	--Para que los objetos sean creados en esta base de datos
	Insert into @tmpScript ( vchScript )
	values ( 'Use master' )
	
	--Contanenar el salto de linea	
	Insert into @tmpScript ( vchScript )
	values ( @nvchEnter )

	--Contanenar un go.
	Insert into @tmpScript ( vchScript )
	values ( 'GO' )

	--Contanenar el salto de linea	
	Insert into @tmpScript ( vchScript )
	values ( @nvchEnter )
	
	--Recuperar los registros de la temporal
	while exists ( Select * From @tmpGralObjeto )
	Begin
		--Recuperar el id de uno de los objetos
		Select top 1 @iIdGralObjeto = iIdGralObjeto,
		@vchNombre = vchNombre
		From @tmpGralObjeto
		
		--Eliminar el objeto de la tabla temporal
		Delete @tmpGralObjeto
		Where iIdGralObjeto = @iIdGralObjeto
		
		--Almacen el script que en caso de existir el objeto lo destruye
		Insert into @tmpScript ( vchScript )
		Exec sp_ppinGeneraScriptObjeto
		@iAccion = 1,
		@vchValor1 = @vchNombre
		
		--Contanenar el salto de linea
		Insert into @tmpScript ( vchScript )
		values ( @nvchEnter )
		
		--Concatener el objeto
		Insert into @tmpScript ( vchScript )
		exec master.dbo.PpInHelpText 
		@vchNombre,
		'master'
		
		--Contanenar el salto de linea
		Insert into @tmpScript ( vchScript )
		values ( @nvchEnter )
		
		--Contanenar un go.
		Insert into @tmpScript ( vchScript )
		values ( 'GO' )
		
		--Contanenar el salto de linea
		Insert into @tmpScript ( vchScript )
		values ( @nvchEnter )
	End
	
	--Retornar el resultado
	Select vchScript
	From @tmpScript
	Order by iIdGralScript
End
Else
Begin
	Select 'Est� funci�n unicamente se permite ejecutar en el servidor de desarrollo'
End

If @@NESTLEVEL <= 1 --No existe otro procedimiento ejecutando a este
Begin
	Set nocount off  
End
GO

If Exists (select * from sysobjects where name = 'VpTdCatObjetoBD_SQL')
	Drop View dbo.VpTdCatObjetoBD_SQL
GO 

CREATE   view dbo.VpTdCatObjetoBD_SQL
(            
 iIdObjeto, vchNombre, vchNombreSquema, vchType,
 iIdEsquema
)            
AS            
/*            
** Nombre:     VpTdCatObjetoBD_SQL            
** Prop�sito:    Retornar el esquema al que pertence el objeto            
** Campos:                 
** Dependencias:                 
** Error Base:                
** Retorna:                 
**                   
** Fecha creaci�n:   3/Febrero/2012            
** Autor creaci�n:   CJFU            
** Csd creaci�n:    csd173            
** Fecha modificaci�n:   3/Julio/2012, 22/Agosto/2012, 31/Agosto/2012, 6/Octubre/2012, 8/Noviembre/2012, 3/Diciembre/2012, 10/Diciembre/2012  ,      
**		02/Enero/2013, 9/Enero/2013   ,11/Febrero/2013, 20/Febrero/2013,27/Febrero/2013,
**		15/Abril/2013, 12/Julio/2013, 26/Julio/2013, 6/Agosto/2013, 12/Septiembre/2013, 20/Septiembre/2013,
**		24/Octubre/2013, 22/Noviembre/2013
** Autor modificaci�n:   rgomez, CJFU, CJFU, VGARIBAY, CJFU, VGARIBAY, CJFU,      
**		RConstante, CGUTIERREZ    ,RConstante, CJFU  ,RConstante,
**		CJFU, VGARIBAY, CJFU, CGUTIERREZ, CGUTIERREZ, VGARIBAY, CGUTIERREZ,
**		CJFU, CJFU
** Csd modificaci�n:   csd173, csd173, csd173, csd173, csd173, csd173, csd173,      
**		Csd173, Csd173 ,Csd173, Csd173, Csd173,
**		Csd173, Csd173, Csd173, Csd173, Csd173, Csd173, Csd173,
**		Csd173, Csd173
** Compatibilidad:   1.75            
** Revisi�n:     21
*/            
SELECT  O.object_id, cast( O.name as nvarchar(50)), cast(S.name as nvarchar),         
cast( O.type as nvarchar) COLLATE database_default, --SQL_Latin1_General_CP850_CI_AI,          
case S.name            
When 'Aba' then 155044        
When 'Adm' then 155054
when 'Afl' then 155034          
when 'Arc' then 155039  
when 'Btc' then 155052 -- Bitacoras
When 'Cad' Then 155057 -- Control de caducidades
when 'CGr' then 155037
when 'Cli' then 155058
When 'Cnt' then 155048 -- Contabilidad    
When 'Crd' then 155060 --Credencializaci�n, identificaci�n de los trabajadores de la instituci�n(cedula, especialidades)
When 'Cst' then 155053 -- Costos    
when 'CTI' then 155040 --Control de incidencias       
when 'dbo' then 155031          
when 'Fac' then 155038          
when 'fnc' then 155036          
when 'Gja' then 155032          
When 'GPo' then 155043        
When 'Her' then 155045 -- Herramientas        
When 'Hmd' then 155042   
When 'Inf' then 155051 -- Informaci�n referente a base de datos y mensajes de error.     
When 'LAC' then 155047 -- Laboratorio de analisis cl�nicos 
When 'Ntr' then 155056 -- Nutrici�n    
When 'Seg' then 155041        
when 'sys' then 155035          
When 'PVt' then 155050 -- Punto de venta 
When 'PEm' then 155055 -- Persona Empresa
When 'Qxs' then 155049 -- Quirofano   
When 'Rpt' then 155059 -- Reporte - informe - vista
when 'tmp' then 155033          
else 155030          
end as iIdEsquema          
FROM sys.objects  O            
INNER JOIN sys.schemas S ON O.schema_id = S.schema_id
Go

Alter procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:				?
** Fecha modificaci�n: 		28/Junio/2011, 8/Mayo/2012,06/Junio/2014,22/Dic/2017
** Autor modificaci�n: 		CJFU, CJFU, RConstante,RConstante
** Csd modificaci�n:			csd173, csd173,Csd173
** Compatibilidad:			1.75
** Revisi�n:					3
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			'  --+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print ''
	Print 'Declare @Respuesta as table'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(Max),'
	Print '		iRegistrosAfectados int'
	Print ')'
	Print 'Set nocount on'

	Print 'Insert Into @Respuesta (bResultado,vchMensaje,iRegistrosAfectados)'
	Print 'Select 1,''Operaci�n realizada correctamente.'',0 '
	Print ''
	Print 'BEGIN TRY'
	Print '	BEGIN TRAN;'
	Print '	SAVE TRAN <%NomTran%>;'
	Print '	BEGIN'
	Print '	'
	Print '	END'
	Print '	COMMIT TRAN <%NomTran%>'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	Update @Respuesta'
	Print '	set bResultado = 0,'
	Print '	vchMensaje = CONCAT(''ERROR: '', ERROR_MESSAGE())'
	Print ''
	Print '	If XACT_STATE() = -1'
	Print '		ROLLBACK TRAN'
	Print 'END CATCH'
	Print ''
	Print '_Fin:'
	Print ' Set nocount off'
	Print '		select XACT_STATE()'
	Print '		If XACT_STATE() = 1 '
	Print '			COMMIT TRAN'
	Print '		SELECT bResultado,vchMensaje,iRegistrosAfectados'
	Print '		From @Respuesta'
	Print 'GO'
End

Go
Use Master
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_ppinTipoLongitud' ) 
	DROP FUNCTION dbo.sp_ppinTipoLongitud
GO
Create Function sp_ppinTipoLongitud
(
    @xtype int,
    @length int,
    @isnullable int
)
Returns Varchar(512)
As 
Begin
    -- Funci�n que a partir de un tipo de datos y una logitud, devuelve el texto del tipo.
    -- Por ejemplo: para xtype=varchar y length=10 devolver� "varchar(10)"
    Declare @ret varchar(512)
    Set @ret = ''

    Select @ret = t.name +
    Case When name in ('varchar', 'nvarchar', 'char', 'nchar') Then '(' + Convert(varchar, @length) + ')' Else '' End + ' ' +
    Case @isnullable When 1 Then 'NULL' Else 'NOT NULL' End
    From systypes t 
    Where t.xtype = @xtype

    Return @ret
End
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_ppinScriptLlavesForaneas' ) 
	DROP PROCEDURE dbo.sp_ppinScriptLlavesForaneas
GO
Create Procedure sp_ppinScriptLlavesForaneas
(
    @vchTabla sysname,
    @vchResultado varchar(8000) output
)
AS 
Begin

    DECLARE @tmpFK table(
        TablaF sysname,
        TablaR sysname,
        ColF sysname,
        ColR sysname,
        FKName sysname)

    -- obtengo las llaves foraneas en @vchForeign
    Declare @vchForeign varchar(8000), @FKName sysname, @vchColumnasF varchar(4000), @vchColumnasR varchar(4000), @ColF sysname, @ColR sysname
    Declare @vchTemp varchar(1000), @TablaR sysname

    Insert into @tmpFK
    Select TablaF.name AS TablaF, TablaR.name AS TablaR, ColF.name AS ColF, ColR.name AS ColR, ofk.name AS FKName
    From sysforeignkeys fk, sysobjects ofk, sysobjects TablaF, sysobjects TablaR, 
    syscolumns ColF, syscolumns ColR
    Where TablaF.name = @vchTabla
    And ofk.id = fk.constid
    And TablaF.id = fk.fkeyid
    And TablaR.id = fk.rkeyid
    And ColF.id = TablaF.id And ColF.colid = fk.fkey
    And ColR.id = TablaR.id And ColR.colid = fk.rkey
    order by FKName

    Set @vchForeign = ''
    While Exists ( Select * From @tmpFK )
    Begin
        Select Top 1 @FKName = FKName From @tmpFK
        Set @vchColumnasF = ''
        Set @vchColumnasR = ''
        While Exists ( Select * From @tmpFK Where FKName = @FKName )
        Begin
            Select Top 1 @ColF = ColF, @ColR = ColR, @TablaR = TablaR From @tmpFK Where FKName = @FKName
            Delete From @tmpFK Where ColF = @ColF And ColR = @ColR And TablaR = @TablaR And FKName = @FKName
            Set @vchColumnasF = @vchColumnasF + @ColF + ', '
            Set @vchColumnasR = @vchColumnasR + @ColR + ', '
        End

        Set @vchColumnasF = LEFT(@vchColumnasF, LEN(@vchColumnasF) - 1)
        Set @vchColumnasR = LEFT(@vchColumnasR, LEN(@vchColumnasR) - 1)
        Set @vchTemp = 'Constraint ' + @FKName + ' Foreign Key (' + @vchColumnasF + ') '
        Set @vchTemp = @vchTemp + 'References ' + @TablaR + ' (' + @vchColumnasR + ')'
        Set @vchForeign = @vchForeign + char(9) + @vchTemp + ',' + char(13) 
    End

    Select @vchResultado = Case When Len(@vchForeign) >=2 Then Left(@vchForeign, Len(@vchForeign) - 2) Else @vchForeign End
End
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_ppinScriptTabla' ) 
	DROP PROCEDURE dbo.sp_ppinScriptTabla
GO
Create Procedure sp_ppinScriptTabla
(
    @vchTabla sysname
)
AS

Set nocount on

-- Obtengo las foreign keys
Declare @foreign varchar(8000)
Exec sp_ppinScriptLlavesForaneas @vchTabla, @foreign output

-- SELECT que devuelve el script de Create Table de la tabla
Select 'Create ' + 
Case o.xtype When 'U' Then 'Table' When 'P' Then 'Procedure' Else '??' End + ' ' +
@vchTabla + char(13) + '('
From sysobjects o
Where o.name = @vchTabla
Union all
-- Campos + identitys + DEFAULTS
select char(9) + c.name + ' ' +                                 -- Nombre
dbo.sp_ppinTipoLongitud(t.xtype, c.length, c.isnullable) +          -- Tipo(longitud)
Case When c.colstat & 1 = 1                                     -- Identity (si aplica)
    Then ' Identity(' + convert(varchar, ident_seed(@vchTabla)) + ',' + Convert(varchar, ident_incr(@vchTabla)) + ')' 
    Else '' 
End + 
Case When not od.name is null                                   -- Defaults (si aplica)
    Then ' Constraint ' + od.name + ' Default ' + replace(replace(cd.text, '((', '('), '))', ')')
    Else ''
End + ', '
from sysobjects o, syscolumns c
LEFT OUTER JOIN sysobjects od On od.id = c.cdefault LEFT OUTER join syscomments cd On cd.id = od.id, 
systypes t
where o.id = object_id(@vchTabla)
and o.id = c.id
and c.xtype = t.xtype
Union all
-- Primary Keys y Unique keys
select char(9) + 'Constraint ' + o.name + ' ' +
Case o.xtype When 'PK' Then 'Primary Key' Else 'Unique' End + ' ' +
dbo.sp_ppinCamposIndice (db_name(), @vchTabla, i.indid) + ', '
from sysobjects o, sysindexes i
where o.parent_obj = object_id(@vchTabla)
and o.xtype in ('PK','UQ')
and i.id = o.parent_obj
and o.name = i.name
Union all
-- Check constraints
select char(9) + 'Constraint ' + o.name + ' Check ' + c.text + ', '
from sysobjects o, syscomments c
where o.parent_obj = object_id(@vchTabla)
and o.xtype in ('C')
and o.id = c.id
Union all
-- Foreign keys
Select @foreign
Union all
Select ')'

Set nocount off
GO



If exists ( Select * From sysobjects Where name = 'sp_ppinGeneraScriptObjeto' ) 
	Drop procedure dbo.sp_ppinGeneraScriptObjeto
GO
CREATE  procedure dbo.sp_ppinGeneraScriptObjeto
(
@iAccion int = 0,
@vchValor1 varchar(512) = '',
@vchValor2 varchar(512) = '',
@vchValor3 varchar(512) = '',
@vchValor4 varchar(512) = '',
@vchValor5 varchar(512) = '',
@vchValor6 varchar(512) = NULL
)
AS
/*
** Nombre:		sp_ppinGeneraScriptObjeto
** Prop�sito:	Procedimiento almacenado para facilir el manejo o manipulacion de la base de datos
** Campos:			@iAccion -->0		ejecuta un sp_helptext de este procedimiento almacenado
**							
**					@iAccion -->1		Arma el query para eliminar o crear un objeto de la base de datos. El crear o eliminar depende del tipo de objeto
**									@vchValor1		Nombre del objeto (Requerido)
**
**					@iAccion -->2		Retorna los objecto que cumplen los criterios proporcionado (Nombre o parte del nombre)
**									@vchValor1		Nombre del objeto (Requerido)
**									@vchValor2		Tipo de objeto (opcional)
**					@iAccion -->3		Retorna los objetos que contienen en el cuerpo del mismo un texto (procedimientos, vistas y triggers)
**									@vchValor1		Texto a busca (Requerido)
**									@vchValor2		Tipo de objeto (opcional)
**					@iAccion -->4		Retorna las columnas de una tabla o vista o par�metros de un SP separados por coma
**									@vchValor1		Nombre de la Tabla, vista o SP
**									@vchValor2		Cantidad de columnas por cada fila devuelta
**									@vchValor3		Alias de la tabla
**									@vchValor4		Enviar el numéro uno si se quiere que lugar de separar los campos
**																por espacios se desea separarlos por tabuladores.
**					@iAccion -->5		Retorna el script CREATE TABLE de la tabla existente proporcionada
**									@vchValor1		Nombre de la Tabla
**					@iAccion -->6		Retorna los comentarios que deben llevar los objetos de la base datos
**					@iAccion -->7		Agrega una columna a la tabla especificada
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		Nombre de la nueva columna
**									@vchValor3		Tipo de dato de la nueva columna
**									@vchValor4		Longitud del tipo de datos (si aplica)
**									@vchValor5		Permite nulos (0:no  1:si)
**									@vchValor6		Valor por defecto (o NULL si no aplica)
**					@iAccion -->8		Genera autom�ticamente un script con los alter table (drop o add) de los constraints
**									que hacen referencia (for�nea) a la tabla especificada
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		0: Genera los Add Constraint (por defecto)
**												1: Genera los Drop Constraint
**					@iAccion -->9		Genera el script que inserta los datos de una tabla
**									@vchValor1		Nombre de la Tabla
**									@vchValor2		Cl�usula where a usar (opcional)
**									@vchValor3		Tipo de resultado: 0: Genera el If Exists->Delete->Insert (dafault)
**																	 1: Genera el If Not Exists->Insert
**					@iAccion -->10		Muestra la consulta al diccionario de datos para un objeto dado
**									@vchValor1		Nombre del objeto.
**					@iAccion -->11		Retorna las consultas a las listas y constantes mas usuales
**									@vchValor1 Nombre de la tabla, si se envia un vacio se retorna todas las tablas ( catconstante, sysvalorlista, catvaloreslista, sysparametro, configuracion)
**									@vchValor2 Valor para el filtro, si se envia un vacio no se coloca ningun filtro
**									@vchValor3 Campo por que se quiere filtrar(nombre) o enviar un uno para filtrar por el consecutivo y un 2
**									para filtrar por el agrupador, si se envia un vacio se filtra por el consecutivo (llave de la tabla).
**					@iAccion -->12		Retorna los objectos que hacen un insert a x tabla de la base de datos
**									@vchValor1 Nombre de la tabla, debe especificarse una sola tabla
**									@vchValor2 Tipo de objeto (<p>-->Procedimientos almacenados, <TR>--Trigrer, <vac�o>-->Sin filtrar el tipo de objeto.
**									@vchValor4 varchar(512) = '',
**									@vchValor5 varchar(512) = '',
**									@vchValor6		Filtro
**					@iAccion -->13		Dada una 

tabla y un alias para la misma, Retorna un SELECT con FROM y WHERE seg�n sus llaves for�neas
**									@vchValor1 Nombre de la tabla
**									@vchValor2 Alias que tendr� la tabla en el select devuelto
**
**					@iAccion -->14		Dado un comando del shell lo ejecuta y devuelve el output
**									@vchValor1: Comando. por ejemplo: 'dir *.exe'
**
**					@iAccion -->15		Devuelve los inserts a CatGralObjeto y CatEspObjeto para una tabla que existe en el diccionario de datos
**									@vchValor1: Nombre de la tabla. por ejemplo: 'ACFMATE1'
**
**					@iAccion -->16		Devuelve las dependencias de un SP (execs anidados a otros SPs)
**									@vchValor1: Nombre del procedimiento almacenado
**									@vchValor2: Cantidad m�xima de niveles de anidamiento
**					@iAccion -->17		Devuelve el script para la creaccion de los objetos de estas herramientes de sql para el desarrollador 
**										(no se requieren parametros). Los objetos tomados son los que est�n asociados a el m�dulo "aplicaci�n interna",
**										est� asociados a el subsistema "herramientas para el sql,...", es un procedimiento almacenado y pertence a la base
**										de datos master.
**
** Dependencias: 	
**
** Fecha creaci�n: 	12/Noviembre/2005
** Autor creaci�n: 	CJFU
** Csd creaci�n:				?
**					
** Fecha modificaci�n: 	24/11/05, 03-03-07, 05-07-06, 01-08-07, 30-04-08, 12-01-09, 26/Febrero/2010,
**						7/Marzo/2011, 29/Agosto/2013, 20/Septiembre/2013, 9/Mayo/2014,11/Abril/2018
** Autor modificacion: 	FDCG, FDCG, FDCG, FDCG, FDCG, FDCG, CJFU,
**						CJFU, CJFU, CJFU, CJFU, RConstante
** Csd modificaci�n:		
**						?, ? , ?, csd173
** Compatibilidad:	1.75
** Revision:		13
*/
Declare @vchQuery nvarchar(4000)
Declare @vchNvaLn nVarchar(2)

Set nocount on

set @vchNvaLn = CHAR(13) + CHAR(10)

If @iAccion = 0 --Ejecutar sp_helptext de este procedimiento almacenado
	Begin
		Select c.text
		From master..sysobjects o, master..syscomments c
		Where o.id = c.id
		And o.name = 'sp_ppinGeneraScriptObjeto'
	End

If @iAccion = 1	--Drop
	Begin
		If rtrim(ltrim(@vchValor1)) = ''
		Begin
			Print 'Esta accion es para la validaci�n de la creaccion/eliminacion de un objeto '
			Print '(creacion para tablas, reportes o campos de tablas y eliminacion para el resto de objetos'
			Print 'Favor de proporcionar el nombre del objeto'
			Print '@vchValor1		-->Nombre del objeto'
			Print '@vchValor2		-->Nombre del campo (siempre y cuando sea una tabla)'
		End
		Else
		If Substring(@vchValor1, 1, 1) = '#' 	-- Si es una temporal
			Begin				
				Select 'If exists ( Select * From tempdb..sysobjects Where name like ''' + @vchValor1 + '[_]%'' ) ' + @vchNvaLn +
					char(9) + 'Drop Table ' + @vchValor1 + @vchNvaLn + 'GO' + @vchNvaLn
				From tempdb..sysobjects o
				Where o.name like @vchValor1 + '[_]%'
			End
		Else If substring(@vchValor1, len(@vchValor1) -3, 4) = '.rpt'--Es un reporte
		Begin
			Exec SP_PpInGenera_ScriptRpt
			@vchNombreReporte = @vchValor1
		End
		Else
			Begin
				
				Set @vchValor2 = ltrim(rtrim(@vchValor2))
				--Revisar si es una tabla, si es as� para cambiar que en lugar del exists sea un not exists
				If exists ( Select * From sysobjects o
					Where o.name = @vchValor1 And o.xtype = 'U' ) --Tabla
				Begin
					--Si no se est� enviando el segundo parametro, quiere decir que unicamente se quiere el if not exists
					If @vchValor2 = '' or @vchValor2 is null
					Begin
						Select 'If not exists ( Select * From sysobjects Where name = ''' + name + ''' ) '
						From sysobjects o
						Where o.name = @vchValor1						
					End
					Else
					Begin
						Select 'If not exists ( Select * From sysobjects o, syscolumns c Where o.id = c.id' + @vchNvaLn + char(9) +
							'And o.name = ''' + o.name + ''' And c.name = ''' + @vchValor2 + ''' ) '
						From sysobjects o
						Where o.name = @vchValor1
					End
				End
				Else
				Begin
					--Select db_name() as Xdb_name
					
					Set @vchQuery = N'SELECT ''IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = '' + char(39) + O.vchNombre + char(39) + '' ) ''' + 
									' + char(13) + char(10) + char(9) + ' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'''DROP '' + CASE WHEN O.vchType = ''U'' THEN ''TABLE''
									WHEN O.vchType = ''P'' THEN ''PROCEDURE''
									WHEN O.vchType = ''TR'' THEN ''TRIGGER''
									WHEN O.vchType = ''V'' THEN ''VIEW''
									WHEN O.vchType = ''FN'' THEN ''FUNCTION''
									WHEN O.vchType = ''TF'' THEN ''FUNCTION''
									End 					
							 + '' '' + O.vchNombreSquema + ''.'' + O.vchNombre + char(13) + char(10) + ''GO'' + char(13) + char(10) ' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'From ' + db_name() + '..VpTdCatObjetoBD_SQL O' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'Where O.vchNombre =  ''' + @vchValor1 + '''' + @vchNvaLn
					Set @vchQuery = @vchQuery + N'And O.vchType in ( ''U'',  --TABLE
					''P'', --PROCEDURE
					''TR'', --TRIGGER
					''V'', --View
					''FN'', --Function
					''TF'') --Function'
					
					--Select @vchQuery as vchQuery
					
					EXECUTE sp_executesql @vchQuery
				End
			End
	End
If @iAccion = 2	--Nombres like...
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el filtro'
		Else
		Begin
			Set @vchQuery  = 'Select Distinct o.name '
			Set @vchQuery  = @vchQuery + 'From sysobjects o '
			Set @vchQuery  = @vchQuery + 'Where o.name like ' + char(39) + '%' + @vchValor1 + '%' + char(39) + ' '
			If @vchValor2 <> ''
				Set @vchQuery  = @vchQuery + 'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
			Exec ( @vchQuery  )

			Select @vchQuery as vchQuery
		End
If @iAccion = 3	--Comentarios like
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el filtro'
		Else
		Begin
			
			Set @vchQuery  = 'Select Distinct o.name '
			Set @vchQuery  = @vchQuery + 'From sysobjects o, syscomments c '
			Set @vchQuery  = @vchQuery + 'Where o.id = c.id '
			Set @vchQuery  = @vchQuery + 'And c.text like ' + char(39) + '%' + @vchValor1 + '%' + char(39) + ' '
			If @vchValor2 <> ''
				Set @vchQuery  = @vchQuery + 'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
			Set @vchQuery  = @vchQuery + ' And o.name <> ''' + @vchValor1 + ''' '
			Exec ( @vchQuery  )

			Select @vchQuery as vchQuery
		End

If @iAccion = 4	-- Campos de una tabla o vista � par�metros de un SP separados por coma
Begin
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el nombre del objeto'
		Else
		Begin
			Declare @ColName varchar(70), @cont int

			Create Table #tmpColumnas (
				ya smallint default(0),
				colName varchar(70),
			)
			Set nocount on
			
			If @vchValor3 is null
			Begin
					Set @vchValor3 = ''
			End
			Else
			Begin--eliminar los espacios
				Set @vchValor3 = rtrim(ltrim(@vchValor3))
			End
	
			If @vchValor3 <> ''
			Begin
				Set @vchValor3 = @vchValor3 + '.'
			End

			If exists (Select * from sysobjects where name = @vchValor1 and type IN ('U', 'S', 'V', 'P'))
			BEGIN
				Insert #tmpColumnas
				Select 0, @vchValor3 + c.name
				From sysobjects o, syscolumns c
				Where o.id = c.id
				AND o.name = @vchValor1
				Order by c.colid 
			END
			ELSE
			BEGIN
				If exists ( Select * from tempdb..sysobjects where name like @vchValor1 + '[_]%' )
				Begin
					-- Es una tabla temporal
					Insert #tmpColumnas
					Select 0, @vchValor3 + c.name
					From tempdb..sysobjects o, tempdb..syscolumns c
					Where o.id = c.id
					AND o.name = (Select Top 1 name From tempdb..sysobjects Where name like @vchValor1 + '[_]%')
					Order by c.colid 					
				End
				Else
				Begin
					-- No existe o no es un objeto v�lido
					If object_id(@vchValor1) Is Null
						Print 'El objeto [' + @vchValor1 + '] no existe en [' + db_name() + ']'
					else
						Print 'El objeto [' + @vchValor1 + '] no es un objeto v�lido'
					Return -1
				End
			END

			Set @vchQuery = ''
			If (@vchValor2 = '')
				Set @vchValor2 = '0'
			Set @cont = 0
			while exists (Select * From #tmpColumnas Where ya=0)
				Begin
					Select TOP 1 @ColName = colName From #tmpColumnas Where ya=0
					Update #tmpColumnas Set ya = 1 Where colName = @colName
					Set @vchQuery = @vchQuery + @ColName 

					If @vchValor4 = 1
					Begin
						Set @vchQuery = @vchQuery + char(9)
					End			
					Else If exists (Select * From #tmpColumnas Where ya=0)
					Begin
						Set @vchQuery = @vchQuery + ', '
					End

					Set @cont = @cont + 1
					If @cont = Convert(int, @vchValor2)
						BEGIN
							Set @vchQuery = @vchQuery + @vchNvaLn
							Set @cont = 0
						END
				End
			Set nocount off
			Select @vchQuery

		End
	End

If @iAccion = 5 	-- Create Table con los campos, tipo, longitud y nullable
	Begin
		If rtrim(ltrim(@vchValor1)) = ''
			Print 'Favor de proporcionar el nombre del objeto'
		Else
		Begin 
			Exec sp_ppinScriptTabla @vchValor1
		End
	End	

If @iAccion = 6 	--Retorna los comentarios que deber�n llevar los objetos de la base de datos (triggers, vistas y procedimientos almacenados)
	Begin
		Declare @vchTmp varchar(8000)
		Set @vchTmp = @vchNvaLn + Char(10) + '/*' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Nombre:			?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Prop�sito:			?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Campos/Par�metros:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '**' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Dependencias:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '**' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Fecha creaci�n:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Autor creaci�n:		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** csd creaci�n: 		?' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Fecha modificaci�n:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Autor modificacion:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** csd modificaci�n:' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Compatibilidad:		1.75' + @vchNvaLn
		Set @vchTmp = @vchTmp + '** Revision:			0' + @vchNvaLn
		Set @vchTmp = @vchTmp + '*/'
		Print @vchTmp
	End

If @iAccion = 7	-- Agregar columna a tabla
	Begin
	-- Variable que indica si el tipo de datos elegido EXIGE una longitud
	Declare @tiAplicaLongitud tinyint
	Set @tiAplicaLongitud = 0
	If rtrim(ltrim(@vchValor1)) = '' OR rtrim(ltrim(@vchValor2)) = '' OR rtrim(ltrim(@vchValor3)) = '' 
		BEGIN
			Print 'Par�metros incorrectos'
			Print 'Formato: 7, NOMBRE_TABLA, NOMBRE_COLUMNA, TIPO_DATO, LONGITUD, PERMITE_NULOS, POR_DEFECTO'
			Print '-------------------------------------------------------------------------------------------------'
			Print 'NOMBRE_TABLA: Nombre de la tabla						ej: ACFDEPE1'
			Print 'NOMBRE_COLUMNA: Nombre de la columna a insertar					ej: vchCodigoABC'
			Print 'TIPO_DATO: Nombre completo del tipo de datos					ej: varchar'
			Print 'LONGITUD: Longitud del tipo de datos (si aplica)				ej: 10'
			Print 'PERMITE_NULOS: 0->No se permiten nulos  1->Se permiten nulos			ej: 0'
			Print 'POR_DEFECTO: Valor por defecto o NULL si no aplica				ej: ''ABC'''
		END
	Else
		BEGIN
			-- Es una tabla no existente ?
			If not exists (Select * from sysobjects where name = @vchValor1 and type IN ('U', 'S'))
			BEGIN

				If object_id(@vchValor1) Is Null
					Print 'El objeto [' + @vchValor1 + '] no existe en [' + db_name() + ']'
				else
					Print 'El objeto [' + @vchValor1 + '] no es un objeto v�lido'
				Return -1
			END
			-- Existe la comuna en la tabla?
			If exists ( Select * from SysObjects o, syscolumns c Where o.id = c.id And o.name = @vchValor1 And c.name = @vchValor2)
			BEGIN
				Print 'Advertencia: La columna [' + @vchValor1 + '.' + @vchValor2 + '] ya existe'
			END
			-- Es un tipo de datos no v�lido?
			If Not exists (Select * From SysTypes Where Name = @vchValor3)
			BEGIN
				Print 'El tipo de datos proporcionado [' + @vchValor3 + '] no es v�lido' + @vchNvaLn + @vchNvaLn
				Print 'Los tipos de datos v�lidos son: '
				Select Name AS ' ' From SysTypes
				Return -1
			END
			-- El tipo de datos exige una longitud y fue proporcionada incorrectamente?
			if Exists (Select * from SysTypes Where name = @vchValor3 AND status = 2)
				Set @tiAplicaLongitud = 1
			If (@tiAplicaLongitud = 1) and (isnumeric(@vchValor4) = 0 Or @vchValor4 = '' Or @vchValor4 = '0')
			BEGIN
				Print 'El tipo de datos [' + @vchValor3 + '] exige una longitud v�lida'
				Return -1
			END
						
			Set @vchQuery = 'ALTER TABLE ' + @vchValor1 + @vchNvaLn 					-- Alter Table TABLA
			Set @vchQuery = @vchQuery + char(9) + 'ADD ' + @vchValor2 + ' '			-- Alter Table TABLA Add COLUMNA
			Set @vchQuery = @vchQuery + @vchValor3 + ' '							-- Alter Table TABLA Add COLUMNA TIPO
			If @tiAplicaLongitud = 1					
				Set @vchQuery = @vchQuery + '(' + @vchValor4 + ') '				-- Alter Table TABLA Add COLUMNA TIPO (XX)
			If @vchValor5 = '0'
				Set @vchQuery = @vchQuery + 'NOT '								-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT
			Set @vchQuery = @vchQuery + 'NULL'									-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT NULL
			If @vchValor6 IS NOT NULL
				Set @vchQuery = @vchQuery + ' CONSTRAINT DF_' + upper(@vchValor1) + '_' + upper(@vchValor2) + ' DEFAULT (''' + @vchValor6 + ''')'		-- Alter Table TABLA Add COLUMNA TIPO (XX) NOT NULL DEFAULT (YY)
			Select @vchQuery
		END
	End
If @iAccion = 8	--SP_AddDrop_ForeignKeys
	Exec ('SP_ppinAddDrop_ForeignKeys ''' + @vchValor1 + ''',''' + @vchValor2 + '''')
If @iAccion = 9	--SP_Genera_Insert 
	Exec ('SP_ppinGenera_Insert ''' + @vchValor1 + ''',''' + @vchValor2 + ''', ' + @vchValor3)
If @iAccion = 10 --Consulta al diccionario de datos
Begin
	Set Nocount On
	Select CC2.vchdescripcion AS Base, G.vchNombreObjeto, CC1.vchdescripcion AS TipoObjeto, 
	CC3.vchdescripcion AS Modulo,	vchDescripcionObjeto, E.vchNombreCampo, E.vchDescripcionCampo, G.siTipoObjeto,
	G.iIdObjeto
	Into #tmpAccion10
	From Bd_Req.Dic.vpinGralDiccionarioDatos G, bd_sicyproh..vpinEspDiccionarioDatos E,
	bd_sicyproh..CatConstanteDiccionarioDatos CC1, bd_sicyproh..CatConstanteDiccionarioDatos CC2,
	bd_sicyproh..CatConstanteDiccionarioDatos CC3
	Where G.iIdObjeto=E.iIdObjeto
	And CC1.siconsecutivo = G.siTipoObjeto
	And CC2.siconsecutivo = G.siBaseDatos
	And CC3.siconsecutivo = G.siIdModulo
	And vchNombreObjeto = @vchValor1
	And CC2.vchdescripcion = db_name()
	

	If Not Exists ( Select * From #tmpAccion10 )
	Begin
		Print 'El objeto [' + @vchValor1 + '] no existe en el diccionario de datos para [' + db_name() + ']'
		Return -1
	End
	
	Declare @tmpBase varchar(512), @tmpObjeto varchar(512), @tmpTipoObjeto varchar(512), @tmpDescripcionObjeto varchar(1000)
	Declare @tmpTipo int, @tmpModulo varchar(512), @iIdObjeto int
	Declare @tmpCampo varchar(512), @tmpDesc varchar(1000)
	Select Top 1 @tmpBase = Base, @tmpObjeto = vchNombreObjeto, @tmpTipoObjeto = TipoObjeto, 
	@tmpDescripcionObjeto = vchDescripcionObjeto, @tmpTipo = siTipoObjeto, @tmpModulo = Modulo, @iIdObjeto = iIdObjeto
	From #tmpAccion10

	Print '---------------- CONSULTA AL DICCIONARIO DE DATOS ----------------'
	Print 'Objeto: [' + @tmpTipoObjeto + '] ' + @tmpBase + '..' + @tmpObjeto + ' (' + @tmpDescripcionObjeto + ')' + @vchNvaLn + 'ID Objeto: ' + convert(varchar, @iIdObjeto) + @vchNvaLn
	Print 'M�dulo: ' + @tmpModulo + @vchNvaLn + @vchNvaLn
	if @tmpTipo = 1		-- Store Procedure
		Select '@' + vchNombreCampo AS Parametro, Replace(vchDescripcionCampo, @vchNvaLn,' ') AS Descripcion From #tmpAccion10
	Else
		Select vchNombreCampo AS Campo, Replace(vchDescripcionCampo,@vchNvaLn,' ') AS Descripcion From #tmpAccion10
	Set Nocount Off
End

If @iAccion = 11 --Retornar las consultas de tablas de listas m�s usuales
Begin
	If @vchValor1 = ''
	Begin
		Select @vchQuery = 'Select * From catconstante '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'siconsecutivo = ' + @vchValor2
													when '1' then 'siconsecutivo = ' + @vchValor2
													when '2' then 'siagrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn  --Un enter
		Select @vchQuery = @vchQuery + 'Select * From sysvalorlista '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'siConsecutivo = ' + @vchValor2
													when '1' then 'siConsecutivo = ' + @vchValor2
													when '2' then 'siAgrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn --Un enter
		Select @vchQuery = @vchQuery + 'Select * From catvaloreslista '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iconsecutivo = ' + @vchValor2
													when '1' then 'iconsecutivo = ' + @vchValor2
													when '2' then 'sicodpal = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
		Select @vchQuery = @vchQuery + 'Select * From sysparametro '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iCodParametro = ' + @vchValor2
				
									when '1' then 'iCodParametro = ' + @vchValor2
													when '2' then 'siAgrupador = ' + @vchValor2
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
		Select @vchQuery = @vchQuery + 'Select * From configuracion '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iCodConfiguracion = ' + @vchValor2
													when '1' then 'iCodConfiguracion = ' + @vchValor2													
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
			+ @vchNvaLn
	End
	Else
	Begin
		Select @vchQuery + 'Select * From ' + @vchValor1 + ' '
			+ case @vchValor2 when '' then ''
				else 'Where ' +
					case @vchValor3 when '' then 'iConsecutivo = ' + @vchValor2
													when '1' then 'iConsecutivo = ' + @vchValor2
													when '2' then 'iAgrupador = ' + @vchValor2													
													else @vchValor3 + ' = ' + @vchValor2 end
			end--Fin del case
	End
	Select @vchQuery as vchQuery
End

If @iAccion = 12 --Retornar las consultas de tablas de listas m�s usuales
Begin
	Set @vchValor1 = ltrim(rtrim(@vchValor1))
	Set @vchValor2 = ltrim(rtrim(@vchValor2))
	If @vchValor1 <> '' And @vchValor1 is not null
	Begin
		--Insert con un espacio
		Set @vchQuery = 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Insert con dos espacios
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert  ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Insert con tabulador
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%insert	' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con un espacio
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%into ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con dos espacios
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%Into  ' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '
		End
		--Into con tabulador
		Set @vchQuery = @vchQuery +	'Union all '		
		Set @vchQuery = @vchQuery + 'Select Distinct o.name '
		Set @vchQuery = @vchQuery +	'From sysobjects o, syscomments c '
		Set @vchQuery = @vchQuery +	'Where o.id = c.id '
		Set @vchQuery = @vchQuery +	'And c.text like ' + char(39) + '%Into	' + @vchValor1 + '%' + char(39) + ' '
		If @vchValor2 <> '' And @vchValor2 is not null
		Begin
			Set @vchQuery = @vchQuery +	'And o.type = ' + char(39) + @vchValor2 + char(39) + ' '

		End
		Execute ( @vchQuery )
		Select @vchQuery as vchQuery
	End
	Else
	Begin
		Select 'Favor de proporcionar el nombre la de la tabla'
		Select '@iAccion -->12		Retorna los objectos que hacen un insert a x tabla de la base de datos'
		Select '@vchValor1 Nombre de la tabla, debe especificarse una sola tabla'
		Select '@vchValor2 Tipo de objeto (<p>-->Procedimientos almacenados, <TR>--Trigrer, <vac�o>-->Sin filtrar el tipo de objeto.'
	End
End

If @iAccion = 13 -- Retornar SELECT de una tabla seg�n sus llaves for�neas
Begin
	If @vchValor2 = '' Set @vchValor2 = 'A'
	Exec ('sp_ppinSelectFromWhereForaneo ''' + @vchValor1 + ''',''' + @vchValor2 + '''')
End

If @iAccion = 14 -- cmdShell
Begin
	Exec ('master..xp_cmdshell ''' + @vchValor1 + '''')
End

If @iAccion = 15 -- Inserts a CatGralObjeto y CatEspObjeto
Begin
	Exec ('sp_ppinTablaCatObjeto ''' + @vchValor1 + '''')
End

If @iAccion = 16 -- Dependencias por execs anidados
Begin
	If len(ltrim(rtrim(@vchValor2))) = 0
		Set @vchValor2 = '10'
	Exec ('sp_ppinDependenciaSP ''' + @vchValor1 + ''', ' + @vchValor2)
End

-- Recuperar los objetos de todas las herramientas de sql para que se ejecuten en otro servidor
If @iAccion = 17 
Begin
	Exec sp_ppinScriptHerramientaSql
End

Go
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'sp_comentario_nuevo' ) 
	DROP PROCEDURE sp_comentario_nuevo
GO
CREATE procedure dbo.sp_comentario_nuevo
@iOpcion int = 0,
@vchMnemonicoUsuario varchar(20) = ''
AS
/*
** Nombre:		sp_comentario_nuevo			
** Prop�sito:	Retornar templates para la documentacion de los objetos de la base de datos
**				Template del manejo de las transacciones
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					
**							
** Fecha creaci�n:			?
** Autor creaci�n:			?
** Csd creaci�n:			?
** Fecha modificaci�n: 		06/Junio/2014,22/Dic/2017,29/Abril/2020
** Autor modificaci�n: 		RConstante,RConstante,RConstante
** Revisi�n:				4
*/


Declare @vchFecha varchar(50)

If @iOpcion = 0
Begin
	Print '/*<inicio><nuevo></inicio>*/'
	Print '/*<inicio></nuevo></inicio>*/' + char(13) + char(10) + char(13) + char(10) 
	Print '/*<inicio>'
	Print '</inicio>*/'
End
Else If @iOpcion = 1
Begin
 Print '/*<inicio><nuevo>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</inicio>*/'  
 Print '/*<inicio>' + ' ' + @vchMnemonicoUsuario + ' '  + @vchFecha + '</nuevo></inicio>*/' + char(13)  + char(10) + char(13) + char(10)   
	Print '/*<inicio>'
	Print '</inicio>*/' + char(13)  + char(10) + char(13) + char(10) 
	print '-- ['+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]'
	print '-- [/'+@vchMnemonicoUsuario+' - <%Incidencia/Asignacion%>]' + char(13)  + char(10) + char(13) + char(10) 

End
Else If @iOpcion = 2 --Comentarios para los objetos
Begin
	Print 'AS'	
	Print '/*' --+ char(13) 
	Print '** Nombre:					' --+ char(13) + char(10) 
	Print '** Prop�sito:				' --+ char(13) + char(10) 
	Print '** Campos:					' --+ char(13) + char(10) 
	Print '** Dependencias: 				' --+ char(13) + char(10) 
	Print '** Error Base:				'
	Print '** Retorna:					'
	Print '**							' --+ char(13) + char(10) 
	Print '** Fecha creaci�n:			'  --+ char(13) + char(10) 
	Print '** Autor creaci�n:			' + @vchMnemonicoUsuario --+ char(13) + char(10) 
	Print '** Fecha modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Autor modificaci�n: 		' --+ char(13) + char(10) 
	Print '** Revisi�n:					0' --+ char(13) + char(10) 
	Print '*/' --+ char(13) + char(10) 
	Print ''
	Print 'DECLARE @ID INT'
	Print 'DECLARE @trancount INT'
	Print 'DECLARE @identity AS TABLE (id INT NOT NULL)'
	Print 'DECLARE @Respuesta AS TABLE'
	Print '('
	Print '		bResultado bit,'
	Print '		vchMensaje varchar(Max)'
	Print ')'
	Print ''	
	Print 'SET NOCOUNT ON'
	Print ''	
	Print 'SET @trancount = @@trancount;'
	Print ''	
	Print ''	
	Print 'INSERT INTO @Respuesta (bResultado,vchMensaje)'
	Print 'SELECT 0,''Error no especificado.'' '
	Print ''
	Print 'BEGIN TRY'	 
	Print '	IF @trancount = 0 '
	Print '	  BEGIN TRANSACTION '
	Print '	ELSE '
	Print '	  SAVE TRANSACTION <%NomTran%>;'
	Print '	'
	Print '	INSERT <%NomTable%>'
	Print '	OUTPUT INSERTED.ID'
	Print '	INTO @identity'
	Print '	VALUES (...'
	Print '	'
	Print '	SET @ID = ( '
    Print '    SELECT TOP 1 id '
    Print '    FROM @identity '
    Print '    );'
	Print '	'
	Print 'Goto _FinTran'
	Print ''
	Print '_RollBack:'	
	Print '	If @trancount = 1'
	Print '	 ROLLBACK TRAN'
	Print '	Else If @trancount > 1'
	Print '	 COMMIT TRAN'
	Print '	'
	Print '_FinTran:	'
	Print '		IF @trancount = 0	'
	Print '			COMMIT	'
	Print '		ELSE	'
	Print '			COMMIT TRAN <%NomTran%>'
	Print 'END TRY'
	Print 'BEGIN CATCH'
	Print '	DECLARE @xstate INT = XACT_STATE()	'
	Print '	'
	Print '	UPDATE @Respuesta '
    Print '	SET bResultado = 0,'
    Print '	   vchMensaje = ERROR_MESSAGE()'
	Print '	'
	Print '	IF @xstate = - 1'
	Print '		ROLLBACK;'
	Print '	'
	Print 'IF @xstate = 1'
    Print '  AND @trancount = 0'
    Print '  ROLLBACK	'
	Print '	'
	Print '	IF @xstate = 1 '
    Print '  AND @trancount > 0 '
    Print '  ROLLBACK TRANSACTION <%NomTran%>;'
	Print 'END CATCH	'
	Print '	'
	Print '_Fin:	'
	Print 'SET NOCOUNT OFF	'
	Print '	'
	Print '		SELECT bResultado,vchMensaje'
	Print '		From @Respuesta'
	Print 'GO'
End

Go
