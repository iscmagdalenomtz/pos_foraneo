IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcGuardaGralComanda' ) 
		DROP PROCEDURE prcGuardaGralComanda
GO
CREATE PROCEDURE prcGuardaGralComanda (

			@iIdGralComanda INT,
			@vchNombre VARCHAR(500),
			@vchComentarioGeneral VARCHAR(500),
			@iIdTipoVenta INT,
			@iIdEstatus INT,
			@mTotalCobrado MONEY,
			@mSubTotal MONEY,
			@smDescuento SMALLINT
			)
AS
/*
** Nombre:					prcGuardaGralComanda
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			31/10/2021
** Autor creaci�n:			MMartinez
*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '' 
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT('')
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION prcGuardaGralComanda;
	ELSE 
	  BEGIN TRANSACTION 
	

	IF (@iIdGralComanda <= 0)
	BEGIN

		INSERT GralComanda
			(dtFechaAlta,	vchNombre,	vchComentarioGeneral,
			iIdTipoVenta,	iIdEstatus,	mTotalCobrado, mSubTotal,
			smDescuento,	dtFechaUltModificacion)
		OUTPUT INSERTED.iIdGralComanda
		INTO @identity
		VALUES 
			(GETDATE(),	@vchNombre,	@vchComentarioGeneral,
			@iIdTipoVenta,	@iIdEstatus,	@mTotalCobrado, @mSubTotal,
			@smDescuento,	GETDATE())
	
		SET @iIdGralComanda = ( 
		SELECT TOP 1 ID 
		FROM @identity 
		);
	
	END
	ELSE
	BEGIN
		UPDATE GralComanda
			SET vchNombre = @vchNombre,
			vchComentarioGeneral = @vchComentarioGeneral,
			iIdTipoVenta = @iIdTipoVenta,
			iIdEstatus = @iIdEstatus,
			mTotalCobrado = @mTotalCobrado,
			mSubTotal = @mSubTotal,
			smDescuento = @smDescuento,
			dtFechaUltModificacion = GETDATE()
		WHERE iIdGralComanda = @iIdGralComanda
	END
	
	
Goto _FinTran
 
_RollBack:
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION prcGuardaGralComanda;  	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcGuardaGralComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	SET @vchError = Concat('prcGuardaGralComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION prcGuardaGralComanda;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	IF(LEN(@vchError) > 0 )	
		BEGIN
			INSERT INTO @Respuesta (bResultado,vchMensaje)	
			SELECT 0, @vchError	
		END	
	ELSE	
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 1, ''	
 
	SELECT bResultado,vchMensaje, @iIdGralComanda AS iIdGralComanda
	From @Respuesta
 
GO

