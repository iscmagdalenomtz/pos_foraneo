IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaConstantes' ) 
		DROP PROCEDURE prcConsultaConstantes
GO
CREATE PROCEDURE prcConsultaConstantes (
			@iIdCatConstante INT,
			@iIdSubAgrupador INT
			)
AS
/*
** Nombre:					prcConsultaConstantes
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			31/10/2021
** Autor creaci�n:			MMartinez
** Fecha modificaci�n: 		
** Autor modificaci�n: 		
** Revisi�n:					0
*/
 

DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT(''),
		iIdCatConstante INT DEFAULT(-1),
		iIdSubAgrupador INT DEFAULT(-1),
		vchNombre VARCHAR(50) DEFAULT(-1),
		vchDescripcion VARCHAR(500) DEFAULT('')
)
 
SET NOCOUNT ON
 
BEGIN TRY
	
	
	INSERT INTO @Respuesta
		(bResultado ,vchMensaje ,iIdCatConstante 
		,iIdSubAgrupador ,vchNombre ,vchDescripcion )
	SELECT 
		1 ,'' ,iIdCatConstante 
		,iIdSubAgrupador ,vchNombre ,vchDescripcion 
	FROM CatConstantes
	WHERE (iIdCatConstante = @iIdCatConstante OR @iIdCatConstante <= 0 )
	  AND (iIdSubAgrupador = @iIdSubAgrupador OR @iIdSubAgrupador <= 0 )

	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaConstantes: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta


	go

