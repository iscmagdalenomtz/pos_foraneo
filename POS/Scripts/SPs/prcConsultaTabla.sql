IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaTabla' ) 
		DROP PROCEDURE prcConsultaTabla
GO
CREATE PROCEDURE prcConsultaTabla (
			@vchValor VARCHAR(100) = ''
			)
AS
/*
** Nombre:					prcConsultaTabla
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			31/10/2021
** Autor creaci�n:			MMartinez
** Fecha modificaci�n: 		17/11/2021
** Revisi�n:					1
*/
 


 DECLARE @vchQuery VARCHAR (500) = ''
SET NOCOUNT ON
 
BEGIN TRY
	
	IF(LEN(@vchValor) > 0)
	BEGIN
		IF(ISNUMERIC(@vchValor) = 1)
		BEGIN
			
			DECLARE @iIdSubAgrupador INT = -1;
			SELECT @iIdSubAgrupador = iIdSubAgrupador FROM CatConstantes WHERE iIdCatConstante = CAST( @vchValor AS INT)

			SET @vchQuery = 'SELECT * FROM CatConstantes WHERE iIdCatConstante =' + @vchValor
			SET @vchQuery += ' SELECT * FROM CatConstantes WHERE iIdSubAgrupador =' + CAST(@iIdSubAgrupador AS VARCHAR(5))
			SET @vchQuery += ' SELECT * FROM CatConstantes WHERE iIdSubAgrupador =' + @vchValor
		END
		ELSE
			SET @vchQuery = 'SELECT * FROM ' + @vchValor
	END

	EXEC (@vchQuery)
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaTabla: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
END CATCH	

GO
