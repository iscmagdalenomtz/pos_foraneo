

IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaProducto' ) 
		DROP PROCEDURE prcConsultaProducto
GO
CREATE PROCEDURE prcConsultaProducto (
			@iIdCatProducto INT,
			@iIdCatCategoria INT,
			@vchNombre VARCHAR(150),
			@bSoloActivos BIT,
			@bTodos BIT
			)
AS
/*
** Nombre:					prcConsultaProducto
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			17/10/2021
** Autor creaci�n:			MMartiez
*/

DECLARE @Respuesta AS TABLE
(
		bResultado bit DEFAULT(0),
		vchMensaje varchar(500) DEFAULT(''),
		iIdCatProducto INT DEFAULT(-1),
		vchNombre varchar(500) DEFAULT(''),
		vchDescripcion varchar(500) DEFAULT(''),
		bActivo bit DEFAULT(0),
		iIdCatCategoria INT DEFAULT(-1),
		vchNombreCategoria varchar(500) DEFAULT(''),
		iIdEspProductosPrecioDefault INT DEFAULT(-1),
		mPrecioDefault MONEY DEFAULT(0)

)
 
SET NOCOUNT ON
 
BEGIN TRY
	

	INSERT INTO @Respuesta
	(
		bResultado,vchMensaje,iIdCatProducto
		,vchNombre,vchDescripcion,iIdCatCategoria ,
		vchNombreCategoria, bActivo , iIdEspProductosPrecioDefault, mPrecioDefault
	)
	SELECT 
		1,'',CP.iIdCatProducto, 
		CP.vchNombre, CP.vchDescripcion,CP.iIdCatCategoria,
		CC.vchNombre,CP.bActivo , EPP.iIdEspProductosPrecio, mPrecio
	FROM	CatProductos CP
		INNER JOIN CatCategorias CC ON CP.iIdCatCategoria = CC.iIdCatCategoria
		INNER JOIN EspProductosPrecios EPP ON EPP.iIdCatProducto  = CP.iIdCatProducto AND EPP.bDefault = 1
	WHERE CP.iIdCatProducto > 0
		AND (@bTodos = 1 OR
			(CP.vchNombre LIKE '%'+@vchNombre+'%' OR @vchNombre = '')
			AND (CP.bActivo = @bSoloActivos OR  @bSoloActivos = 0)
			AND (CP.iIdCatProducto = @iIdCatProducto OR  @iIdCatProducto <= 0)
			AND (CP.iIdCatCategoria = @iIdCatCategoria OR  @iIdCatCategoria <= 0)
			)


	IF NOT EXISTS(SELECT * FROM @Respuesta)
	BEGIN
		INSERT INTO @Respuesta
		(bResultado, vchMensaje)
		SELECT 0, 'NO SE ENCONTRARON REGISTROS DEL PRODCUTO POR LOS PARAMETROS DE BUSQUEDA'
	END
 
END TRY
BEGIN CATCH
	PRINT Concat(' prcConsultaProducto: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	

SELECT *
From @Respuesta

GO