
 
 
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcGuardaCategoria' ) 
		DROP PROCEDURE prcGuardaCategoria
GO
CREATE PROCEDURE prcGuardaCategoria (
			@iIdCatCategoria INT,
			@vchNombre VARCHAR(150),
			@vchDescripcion VARCHAR(150),
			@iIdCatCategoriaPadre INT,
			@bActivo BIT
			)
AS
/*
** Nombre:					prcGuardaCategoria
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			17/10/2021
** Autor creaci�n:			MMartiez
*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '',  @ID INT = -1
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado bit DEFAULT(1),
		vchMensaje varchar(500) DEFAULT(''),
		iIdCatCategoria INT DEFAULT(-1)
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION  prcGuardaCategoria;
	ELSE 
	  BEGIN TRANSACTION 

		IF(@iIdCatCategoria <= 0)
		BEGIN

			INSERT INTO CatCategorias
			(
				vchNombre,vchDescripcion,iIdCatCategoriaPadre
				,bActivo,dtFechaUltModificacion
			)
			OUTPUT INSERTED.iIdCatCategoria INTO @identity	
			SELECT
				@vchNombre,@vchDescripcion
				,@iIdCatCategoriaPadre,@bActivo,GETDATE()

	
			SET @iIdCatCategoria = ( 
			SELECT TOP 1 ID 
			FROM @identity 
			);
		END
		ELSE
		BEGIN
			UPDATE CatCategorias
			SET vchNombre = @vchNombre,
			vchDescripcion = @vchDescripcion,
			iIdCatCategoriaPadre = @iIdCatCategoriaPadre,
			bActivo = @bActivo,
			dtFechaUltModificacion = GETDATE()
			WHERE iIdCatCategoria = @iIdCatCategoria
		END

	
	
	
Goto _FinTran
 
_RollBack:
		
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION  prcGuardaCategoria;  		
	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
END TRY
BEGIN CATCH
	PRINT Concat(' prcGuardaCategoria: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	SET @vchError = Concat(' prcGuardaCategoria: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )

	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION  prcGuardaCategoria;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	
IF(LEN(@vchError) > 0 )	
	BEGIN
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 0, @vchError
	END	
ELSE	
	INSERT INTO @Respuesta (bResultado,vchMensaje,iIdCatCategoria)	
	SELECT 1, '', @iIdCatCategoria	
 
SELECT bResultado,vchMensaje
From @Respuesta