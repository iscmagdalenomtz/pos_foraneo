IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcGuardaEspCorteCaja' ) 
		DROP PROCEDURE prcGuardaEspCorteCaja
GO
CREATE PROCEDURE prcGuardaEspCorteCaja (

	@iIdGralCorteCaja INT,
	@iIdGralComanda INT,
	@iIdTipoPago INT,
	@mTotalRecaudado MONEY,
	@vchComentario VARCHAR(500) = '',
	@iIdTipoMovimiento INT = 15
)
AS
/*
** Nombre:					prcGuardaEspCorteCaja
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			11/12/2021
** Autor creaci�n:			MMartinez
*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '', @iIdEspCorteCaja INT
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT('')
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION prcGuardaEspCorteCaja;
	ELSE 
	  BEGIN TRANSACTION 
	

	INSERT INTO EspCorteCaja
	(
		iIdGralCorteCaja,	iIdGralComanda,	iIdTipoPago,
		mTotalRecaudado,	dtFechaUltModificacion, vchComentario, iIdTipoMovimiento
	)
	SELECT
		@iIdGralCorteCaja,	@iIdGralComanda,	@iIdTipoPago,
		@mTotalRecaudado,	GETDATE() , @vchComentario, @iIdTipoMovimiento
	


	IF(@iIdTipoPago = 12 )
	BEGIN
		UPDATE GralCorteCaja
		SET mTotalSistema = (mTotalSistema + (@mTotalRecaudado)),
		dtFechaUltModificacion = GETDATE()
		WHERE iIdGralCorteCaja = @iIdGralCorteCaja

	
	END


	IF(@iIdGralComanda > 0)
	BEGIN
		UPDATE GralComanda
		SET iIdEstatus = 6,	--	PAGADA,
		dtFechaUltModificacion = GETDATE()
		WHERE iIdGralComanda = @iIdGralComanda
	END
	
Goto _FinTran
 
_RollBack:
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION prcGuardaEspCorteCaja;  	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcGuardaEspCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	SET @vchError = Concat('prcGuardaEspCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION prcGuardaEspCorteCaja;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	IF(LEN(@vchError) > 0 )	
		BEGIN
			INSERT INTO @Respuesta (bResultado,vchMensaje)	
			SELECT 0, @vchError	
		END	
	ELSE	
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 1, ''	
 
	SELECT bResultado,vchMensaje, @iIdEspCorteCaja AS iIdEspComanda
	From @Respuesta
 
GO

