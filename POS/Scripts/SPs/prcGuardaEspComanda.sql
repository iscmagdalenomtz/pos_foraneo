IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcGuardaEspComanda' ) 
		DROP PROCEDURE prcGuardaEspComanda
GO
CREATE PROCEDURE prcGuardaEspComanda (

			@iIdGralComanda INT,
			@iIdEspComanda INT,
			@iIdEstatus INT,
			@iIdCatProducto INT,
			@iIdEspProductosPrecio INT,
			@vchDescripcion VARCHAR(500),
			@vchComentario	VARCHAR(500),
			@smCantidad SMALLINT,
			@smDescuento SMALLINT,
			@mTotal	MONEY
			)
AS
/*
** Nombre:					prcGuardaEspComanda
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			17/11/2021
** Autor creaci�n:			MMartinez
*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '' 
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT('')
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION prcGuardaEspComanda;
	ELSE 
	  BEGIN TRANSACTION 
	

	IF (@iIdEspComanda <= 0)
	BEGIN

		INSERT EspComanda
			(iIdEstatus,	iIdCatProducto,	iIdEspProductosPrecio,
			vchDescripcion,	vchComentario,	smCantidad,
			smDescuento,	mTotal,	dtFechaUltModificacion,	iIdGralComanda)
		OUTPUT INSERTED.iIdEspComanda
		INTO @identity
		VALUES 
			(@iIdEstatus,	@iIdCatProducto,@iIdEspProductosPrecio,
			@vchDescripcion,	@vchComentario,	@smCantidad,
			@smDescuento,	@mTotal,	GETDATE(),	@iIdGralComanda)
	
		SET @iIdEspComanda = ( 
		SELECT TOP 1 ID 
		FROM @identity 
		);
	
	END
	ELSE
	BEGIN 
		
		UPDATE EspComanda
		SET iIdEstatus = @iIdEstatus,
		iIdEspProductosPrecio = @iIdEspProductosPrecio,
		vchComentario =@vchComentario,
		smCantidad = @smCantidad,
		smDescuento = @smDescuento,
		mTotal = @mTotal,
		dtFechaUltModificacion = GETDATE()
		WHERE iIdEspComanda = @iIdEspComanda

	END
	
Goto _FinTran
 
_RollBack:
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION prcGuardaEspComanda;  	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcGuardaEspComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	SET @vchError = Concat('prcGuardaEspComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION prcGuardaEspComanda;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	IF(LEN(@vchError) > 0 )	
		BEGIN
			INSERT INTO @Respuesta (bResultado,vchMensaje)	
			SELECT 0, @vchError	
		END	
	ELSE	
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 1, ''	
 
	SELECT bResultado,vchMensaje, @iIdEspComanda AS iIdEspComanda
	From @Respuesta
 
GO

