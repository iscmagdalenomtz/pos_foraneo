IF EXISTS (SELECT *
		   FROM   sysobjects
		   WHERE  NAME = 'prcConsultaCategoria')
	DROP PROCEDURE prcConsultaCategoria

GO

CREATE PROCEDURE prcConsultaCategoria
(
	@iIdCatCategoria      INT,
	@iIdCatCategoriaPadre INT,
	@vchNombre            VARCHAR(150),
	@bSoloActivos         BIT,
	@bTodos               BIT
)
AS
	/*
	** Nombre:     prcConsultaCategoria
	** Prop�sito:
	** Campos:
	** Dependencias:
	** Error Base:
	** Retorna:     Si @bRetornarMensaje
	1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje , iError )
	0 - Retorna solo el No de error
	**
	** Fecha creaci�n:   17/10/2021
	** Autor creaci�n:   MMartiez
	** Fecha modificaci�n:    30/10/2021
	** Revisi�n:     1
	*/

	DECLARE @Respuesta AS TABLE
	(
		 bResultado             BIT DEFAULT(1),
		 vchMensaje             VARCHAR(500) DEFAULT(''),
		 iIdCatCategoria        INT DEFAULT(-1),
		 vchNombre              VARCHAR(150) DEFAULT(''),
		 vchDescripcion         VARCHAR(150) DEFAULT(''),
		 iIdCatCategoriaPadre   INT DEFAULT(-1),
		 vchNombrePadre         VARCHAR(150) DEFAULT(''),
		 bActivo                BIT DEFAULT(0),
		 dtFechaUltModificacion DATETIME DEFAULT('1900-01-01')
	)

	SET NOCOUNT ON

	BEGIN TRY
		INSERT INTO @Respuesta
		(
			bResultado,vchMensaje,iIdCatCategoria,
			vchNombre,vchDescripcion,iIdCatCategoriaPadre,
			vchNombrePadre,bActivo,dtFechaUltModificacion
		)
		SELECT 1,'',CC1.iIdCatCategoria,
			   CC1.vchNombre,CC1.vchDescripcion,CC1.iIdCatCategoriaPadre,
			   CC2.vchNombre,CC1.bActivo,CC1.dtFechaUltModificacion
		FROM   CatCategorias CC1,
			   CatCategorias CC2
		WHERE      CC1.iIdCatCategoriaPadre = CC2.iIdCatCategoria
			   AND CC1.iIdCatCategoria      > 0
			   AND (@bTodos                  = 1
					 OR (CC1.vchNombre LIKE '%' + @vchNombre + '%'
						  OR @vchNombre               = '')
						AND (CC1.bActivo              = @bSoloActivos
							  OR @bSoloActivos            = 0)
						AND (CC1.iIdCatCategoriaPadre = @iIdCatCategoriaPadre
							  OR @iIdCatCategoriaPadre    <= -2)
						AND (CC1.iIdCatCategoria      = @iIdCatCategoria
							  OR @iIdCatCategoria         <= 0))

	END TRY
	BEGIN CATCH
		PRINT Concat(' prcConsultaCategoria: ', ERROR_MESSAGE(), ' ', ERROR_PROCEDURE(), ' ', ERROR_LINE())

	END CATCH

	_FIN:

	SET NOCOUNT OFF

	SELECT *
	FROM   @Respuesta 
