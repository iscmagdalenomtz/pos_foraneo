
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaGralComanda' ) 
		DROP PROCEDURE prcConsultaGralComanda
GO
CREATE PROCEDURE prcConsultaGralComanda (
			@iIdGralComanda INT,
			@bActivas BIT
			)
AS
/*
** Nombre:					prcConsultaGralComanda
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			02/11/2021
** Autor creaci�n:			MMartinez

*/
 

DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT(''),
		iIdGralComanda INT DEFAULT(-1),
		dtFechaAlta DATETIME DEFAULT('1900-01-01'),
		vchNombre VARCHAR(50) DEFAULT(-1),
		vchComentarioGeneral VARCHAR(500) DEFAULT(''),
		iIdTipoVenta  INT DEFAULT(-1),
		vchTipoVenta VARCHAR(500) DEFAULT(''),
		iIdEstatus  INT DEFAULT(-1),
		vchEstatus VARCHAR(500) DEFAULT(''),
		mTotalCobrado MONEY DEFAULT(0),
		mSubTotal MONEY DEFAULT(0),
		smDescuento SMALLINT DEFAULT(0)
)

SET NOCOUNT ON
 
BEGIN TRY
	
	
	INSERT INTO @Respuesta
		(bResultado		,vchMensaje		,iIdGralComanda 
		,dtFechaAlta	,vchNombre		,vchComentarioGeneral 
		,iIdTipoVenta	,vchTipoVenta	,iIdEstatus  
		,vchEstatus		,mTotalCobrado  ,mSubTotal, smDescuento 
		)
	SELECT 
		1,'',iIdGralComanda 
		,dtFechaAlta	,vchNombre		,vchComentarioGeneral 
		,iIdTipoVenta	,dbo.GetIdConstante(iIdTipoVenta,0)	,iIdEstatus  
		,dbo.GetIdConstante(iIdEstatus,0)		,mTotalCobrado, mSubTotal	,smDescuento 
	FROM GralComanda
	WHERE (iIdGralComanda = @iIdGralComanda OR @iIdGralComanda <= 0 )
	  AND (( iIdEstatus not in (6,7,-1) AND  @bActivas = 1 ) OR  @bActivas = 0) 

	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaGralComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta

GO

