
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaGralCorteCaja' ) 
		DROP PROCEDURE prcConsultaGralCorteCaja
GO
CREATE PROCEDURE prcConsultaGralCorteCaja (
			@bActiva BIT
			)
AS
/*
** Nombre:					prcConsultaGralCorteCaja
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			11/12/2021
** Autor creaci�n:			MMartinez
*/
 

DECLARE @Respuesta AS TABLE
(
	bResultado BIT  DEFAULT(1),
	vchMensaje VARCHAR(500)  DEFAULT(''),
	iIdGralCorteCaja INT DEFAULT(-1),
	dtFechaIncicio DATETIME DEFAULT('1900-01-01'),	
	dtFechaFin DATETIME DEFAULT('1900-01-01'),	
	vchDescripcion VARCHAR(500)  DEFAULT(''),
	mTotal MONEY  DEFAULT(0),
	mTotalSistema MONEY  DEFAULT(0),
	dtFechaUltModificacion DATETIME DEFAULT('1900-01-01'),	
	bActivo BIT DEFAULT('')
)

SET NOCOUNT ON
 
BEGIN TRY
		
	INSERT INTO @Respuesta
		(iIdGralCorteCaja ,
		dtFechaIncicio,	dtFechaFin,	vchDescripcion,
		mTotal, mTotalSistema,	dtFechaUltModificacion,	bActivo )
	SELECT 
		iIdGralCorteCaja ,
		dtFechaIncicio,	dtFechaFin,	vchDescripcion,
		mTotal, mTotalSistema,	dtFechaUltModificacion,	bActivo 
	FROM GralCorteCaja
	WHERE (bActivo = @bActiva OR @bActiva = 0)
	AND iIdGralCorteCaja > 0

	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaGralCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta

GO
