

IF EXISTS ( SELECT *
FROM sysobjects
WHERE NAME = 'prcConsultaPrecioProducto' ) 
		DROP PROCEDURE prcConsultaPrecioProducto
GO
CREATE PROCEDURE prcConsultaPrecioProducto
    (
    @iIdCatProducto INT,
    @bSoloActivos BIT
)
AS
/*
** Nombre:					prcConsultaPrecioProducto
** Propósito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creación:			18/01/2022
** Autor creación:			MMartinez
*/


DECLARE @Respuesta AS TABLE
(
    bResultado BIT DEFAULT(1),
    vchMensaje VARCHAR(500) DEFAULT(''),
    iIdEspProductosPrecio INT DEFAULT(-1),
    iIdCatProducto INT DEFAULT(-1),
    mPrecio MONEY DEFAULT(0),
    vchDescripcion VARCHAR(550) DEFAULT(''),
    bActivo BIT DEFAULT(1),
    bDefault BIT DEFAULT(0)


)

SET NOCOUNT ON

BEGIN TRY

        
    INSERT INTO @Respuesta
        (iIdEspProductosPrecio, iIdCatProducto ,mPrecio , vchDescripcion
        ,bActivo ,bDefault )
    SELECT
        iIdEspProductosPrecio, iIdCatProducto , mPrecio , vchDescripcion
            , bActivo , bDefault
    FROM EspProductosPrecios
    WHERE iIdCatProducto = @iIdCatProducto
        AND (@bSoloActivos = bActivo OR @bSoloActivos = 0 )

	
    IF NOT EXISTS (SELECT *
FROM @Respuesta)
    BEGIN
    INSERT INTO @Respuesta
        (bResultado,vchMensaje )
    SELECT 0, 'No existen precios para este producto'
END
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaPrecioProducto: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
END CATCH

SELECT *
From @Respuesta


	go

