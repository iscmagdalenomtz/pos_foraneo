
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcInsertaProducto' ) 
		DROP PROCEDURE prcInsertaProducto
GO
CREATE PROCEDURE prcInsertaProducto (
			@iIdCatProducto INT,
			@vchNombre VARCHAR(150),
			@vchDescripcion VARCHAR(150),
			@iIdCatCategoria INT,
			@bActivo BIT
			)
AS
/*
** Nombre:					prcInsertaProducto
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			10/10/2021
** Autor creaci�n:			MMartiez

*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = ''
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado bit DEFAULT(1),
		vchMensaje varchar(500) DEFAULT('')
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION  prcInsertaProducto;
	ELSE 
	  BEGIN TRANSACTION 


	IF(@iIdCatProducto <= 0)
	BEGIN

		INSERT INTO CatProductos
		(
			vchNombre ,iIdCatCategoria, vchDescripcion
			,bActivo ,dtFechaUltModificacion
		)
		OUTPUT INSERTED.iIdCatProducto  INTO @identity	
		SELECT 
			@vchNombre ,@iIdCatCategoria, @vchDescripcion
			,1 ,GETDATE()

			
		SET @iIdCatProducto = ( 
		SELECT TOP 1 ID 
		FROM @identity 
		);
	
	END
	BEGIN 
		UPDATE CatProductos
		SET vchNombre =@vchNombre,
			iIdCatCategoria = @iIdCatCategoria,
			vchDescripcion = @vchDescripcion,
			bActivo = @bActivo,
			dtFechaUltModificacion = GETDATE()
		WHERE iIdCatProducto = @iIdCatProducto
	END
	

	
Goto _FinTran
 
_RollBack:
		
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION  prcInsertaProducto;  		
	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
END TRY
BEGIN CATCH
	PRINT Concat(' prcInsertaProducto: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	SET @vchError = Concat(' prcInsertaProducto: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION  prcInsertaProducto;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	
IF(LEN(@vchError) > 0 )	
	BEGIN
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 0, @vchError
	END	
ELSE	
	INSERT INTO @Respuesta (bResultado,vchMensaje)	
	SELECT 1, ''	
 
SELECT bResultado,vchMensaje, @iIdCatProducto AS iIdCatProducto
From @Respuesta

GO