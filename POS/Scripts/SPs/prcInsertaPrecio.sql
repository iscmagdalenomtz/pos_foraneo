IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcInsertaPrecio' ) 
		DROP PROCEDURE prcInsertaPrecio
GO
CREATE PROCEDURE prcInsertaPrecio (
		@iIdEspProductosPrecio INT,
		@iIdCatProducto INT,
		@mPrecio MONEY,
		@vchDescripcion VARCHAR(500),
		@bActivo BIT,
		@bDefault BIT
		)
AS
/*
** Nombre:					prcInsertaPrecio
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			24/10/2021
** Autor creaci�n:			MMartinez
*/
 
DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '' 
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(1),
		vchMensaje VARCHAR(500)  DEFAULT(  ''  )
)
 
SET NOCOUNT ON
 
BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION prcInsertaPrecio;
	ELSE 
	  BEGIN TRANSACTION 
	
	IF(@iIdEspProductosPrecio <= 0)
	BEGIN
		INSERT INTO EspProductosPrecios
		(iIdCatProducto	,mPrecio, bDefault
		,vchDescripcion	,bActivo	,dtFechaUltModificacion)
	SELECT 
		@iIdCatProducto	,@mPrecio, @bDefault
		,@vchDescripcion	,@bActivo	,GETDATE()
	END
	ELSE
	BEGIN
		UPDATE EspProductosPrecios
		SET mPrecio = @mPrecio,
			bDefault =@bDefault,
			vchDescripcion = @vchDescripcion,
			bActivo = @bActivo
		WHERE iIdEspProductosPrecio = @iIdEspProductosPrecio
	END


	
	
	
Goto _FinTran
 
_RollBack:
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION prcInsertaPrecio;  	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcInsertaPrecio: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION prcInsertaPrecio;
 
END CATCH	
	
_Fin:	
SET NOCOUNT OFF	
	
	IF(LEN(@vchError) > 0 )	
		BEGIN
			INSERT INTO @Respuesta (bResultado,vchMensaje)	
			SELECT 0, @vchError	
		END	
	ELSE	
		INSERT INTO @Respuesta (bResultado,vchMensaje)	
		SELECT 1, ''	
 
	SELECT bResultado,vchMensaje
	From @Respuesta
 
GO

