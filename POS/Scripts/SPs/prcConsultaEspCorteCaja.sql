
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaEspCorteCaja' ) 
		DROP PROCEDURE prcConsultaEspCorteCaja
GO
CREATE PROCEDURE prcConsultaEspCorteCaja (
			@iIdGralCorteCaja INT
			)
AS
/*
** Nombre:					prcConsultaEspCorteCaja
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			12/11/2021
** Autor creaci�n:			MMartinez
*/
 

DECLARE @Respuesta AS TABLE
(
	bResultado BIT  DEFAULT(1),
	vchMensaje VARCHAR(500)  DEFAULT(''),
	iIdEspCorteCaja	INT DEFAULT(-1),
	iIdGralCorteCaja INT DEFAULT(-1),
	iIdGralComanda INT DEFAULT(-1),
	iIdTipoPago INT DEFAULT(-1),
	vchTipoPago VARCHAR(50)  DEFAULT(''),
	mTotalRecaudado INT DEFAULT(-1),
	iIdTipoMovimiento INT DEFAULT(-1),
	vchTipoMovimiento VARCHAR(50)  DEFAULT(''),
	dtFechaUltModificacion DATETIME DEFAULT('1900-01-01')
)

SET NOCOUNT ON
 
BEGIN TRY
			
	INSERT INTO @Respuesta
		(iIdEspCorteCaja, iIdGralCorteCaja, iIdGralComanda,
		iIdTipoPago,vchTipoPago, mTotalRecaudado, iIdTipoMovimiento, vchTipoMovimiento,
		dtFechaUltModificacion )
	SELECT
		iIdEspCorteCaja, iIdGralCorteCaja, iIdGralComanda,
		iIdTipoPago, dbo.GetIdConstante(iIdTipoPago,0), mTotalRecaudado, iIdTipoMovimiento, dbo.GetIdConstante(iIdTipoMovimiento,0),
		dtFechaUltModificacion
	FROM EspCorteCaja
	WHERE (iIdGralCorteCaja = @iIdGralCorteCaja OR @iIdGralCorteCaja <= 0)

END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaEspCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta

GO
