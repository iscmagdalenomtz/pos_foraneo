
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaCorteActual' ) 
		DROP PROCEDURE prcConsultaCorteActual
GO
CREATE PROCEDURE prcConsultaCorteActual 
AS
/*
** Nombre:					prcConsultaCorteActual
** Propósito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creación:			05/02/2022
** Autor creación:			MMartinez
*/
 

DECLARE @Respuesta AS TABLE
(
	bResultado BIT  DEFAULT(1),
	vchMensaje VARCHAR(500)  DEFAULT(''),
	iIdGralCorteCaja	INT DEFAULT(-1),
	dtFechaIncicio DATETIME DEFAULT ('1900-01-01'),
	dtFechaFin DATETIME DEFAULT ('1900-01-01'),
	vchDescripcion VARCHAR(50) DEFAULT(''),
	mTotal MONEY DEFAULT(0),
	mTotalSistema MONEY DEFAULT(0),
	bActivo BIT DEFAULT (0)
)

SET NOCOUNT ON
 
BEGIN TRY
		


	INSERT INTO @Respuesta
		(	iIdGralCorteCaja, dtFechaIncicio, dtFechaFin, 
        vchDescripcion, mTotal,mTotalSistema, bActivo  )
	SELECT 
		iIdGralCorteCaja, dtFechaIncicio, dtFechaFin, 
        vchDescripcion, mTotal, mTotalSistema, bActivo 
	FROM GralCorteCaja
	WHERE bActivo = 1

    IF(SELECT COUNT(iIdGralCorteCaja) FROM @Respuesta) > 1
    BEGIN
        UPDATE @Respuesta 
        SET bResultado = 0,
        vchMensaje = 'Existe mas de un corte de caja activo'
    END

END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaCorteActual: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta

GO

