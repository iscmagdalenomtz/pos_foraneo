


IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'prcConsultaEspComanda' ) 
		DROP PROCEDURE prcConsultaEspComanda
GO
CREATE PROCEDURE prcConsultaEspComanda (
			@iIdEspComanda INT,
			@iIdGralComanda INT,
			@bTodas BIT
			)
AS
/*
** Nombre:					prcConsultaEspComanda
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			18/11/2021
** Autor creaci�n:			MMartinez

*/
 

DECLARE @Respuesta AS TABLE
(
		bResultado BIT  DEFAULT(0),
		vchMensaje VARCHAR(500)  DEFAULT(''),
		iIdGralComanda INT DEFAULT(-1),
		iIdEspComanda INT DEFAULT(-1),
		iIdEstatus  INT DEFAULT(-1),
		vchEstatus VARCHAR(500) DEFAULT(''),
		iIdCatProducto  INT DEFAULT(-1),
		vchNombreProducto VARCHAR(50) DEFAULT(-1),
		iIdEspProductosPrecio INT DEFAULT(-1),
		mPrecioUnitario MONEY DEFAULT(0),
		vchDescripcion  VARCHAR(500) DEFAULT(''),
		vchComentario VARCHAR(500) DEFAULT(''),
		smCantidad SMALLINT DEFAULT(0),
		smDescuento SMALLINT DEFAULT(0),
		mTotal MONEY DEFAULT(0)
)

SET NOCOUNT ON
 
BEGIN TRY
	
	
	INSERT INTO @Respuesta
		(bResultado ,vchMensaje ,iIdGralComanda 
		,iIdEspComanda	,iIdEstatus		,vchEstatus 
		,iIdCatProducto		,vchNombreProducto		,iIdEspProductosPrecio 
		,mPrecioUnitario	,vchDescripcion	,vchComentario 
		,smCantidad		,smDescuento	,mTotal 
		)
	SELECT 1 ,'' ,EC.iIdGralComanda 
		,EC.iIdEspComanda	,EC.iIdEstatus		,dbo.GetIdConstante(EC.iIdEstatus,0)
		,EC.iIdCatProducto		,CP.vchNombre	,EC.iIdEspProductosPrecio 
		,EPP.mPrecio	,EC.vchDescripcion	,EC.vchComentario 
		,EC.smCantidad		,EC.smDescuento	,EC.mTotal 
	FROM EspComanda AS EC
		LEFT JOIN CatProductos CP ON CP.iIdCatProducto = EC.iIdCatProducto
		LEFT JOIN EspProductosPrecios EPP ON EPP.iIdEspProductosPrecio = EC.iIdEspProductosPrecio
	WHERE (EC.iIdEspComanda = @iIdEspComanda OR  @iIdEspComanda <= 0)
	AND (EC.iIdGralComanda = @iIdGralComanda OR  @iIdGralComanda <= 0)
	OR @bTodas = 1
	

	
	
END TRY
BEGIN CATCH
	PRINT Concat('prcConsultaEspComanda: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	
	
END CATCH	

	SELECT *
	From @Respuesta

GO

EXEC prcConsultaEspComanda 
			@iIdEspComanda = 0,
			@iIdGralComanda = -1,
			@bTodas =1