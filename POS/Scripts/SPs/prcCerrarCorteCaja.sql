


IF EXISTS ( SELECT *
FROM sysobjects
WHERE NAME = 'prcCerrarCorteCaja' ) 
		DROP PROCEDURE prcCerrarCorteCaja
GO
CREATE PROCEDURE prcCerrarCorteCaja
	(
	@iIdGralCorteCaja INT,
	@vchDescripcion VARCHAR(150),
	@mTotal 	MONEY
)
AS
/*
** Nombre:					prcCerrarCorteCaja
** Prop�sito:				
** Campos:					
** Dependencias: 				
** Error Base:				
** Retorna:					Si @bRetornarMensaje  
											1 - Retorna el mensaje para la aplicacion (bResultado ,vchMensaje ,	iError )
											0 - Retorna solo el No de error		
**							
** Fecha creaci�n:			05/02/2022
** Autor creaci�n:			MMartiez
*/

DECLARE @trancount INT = -1,  @vchError VARCHAR(200) = '',  @ID INT = -1
DECLARE @identity AS TABLE (ID INT NOT NULL)
DECLARE @Respuesta AS TABLE
(
	bResultado bit DEFAULT(1),
	vchMensaje varchar(500) DEFAULT('')
)

SET NOCOUNT ON

BEGIN TRY
	SET @trancount = @@TRANCOUNT
	IF @trancount > 0 
	  SAVE TRANSACTION  prcCerrarCorteCaja;
	ELSE 
	  BEGIN TRANSACTION 

		

	UPDATE GralCorteCaja
	SET mTotal = @mTotal,
		vchDescripcion = @vchDescripcion,
		dtFechaFin = GETDATE(),
		dtFechaUltModificacion = GETDATE(),
		bActivo = 0
	WHERE iIdGralCorteCaja = @iIdGralCorteCaja


	INSERT INTO GralCorteCaja
		(dtFechaIncicio, dtFechaFin,
		vchDescripcion, mTotal, dtFechaUltModificacion,
		bActivo, mTotalSistema)
	VALUES
		(GETDATE(), '1900-01-01',
			'', 0, GETDATE(),
			1, 0)
		
	
Goto _FinTran
 
_RollBack:
		
	IF @trancount = 0
	 ROLLBACK TRANSACTION;
	Else 
	 IF  @trancount <> -1 AND XACT_STATE() <> -1
		ROLLBACK TRANSACTION  prcCerrarCorteCaja;  		
	
	Goto _Fin
_FinTran:	
		IF @trancount = 0
			COMMIT TRANSACTION;  	
END TRY
BEGIN CATCH
	PRINT Concat(' prcCerrarCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )
	SET @vchError = Concat(' prcCerrarCorteCaja: ', ERROR_MESSAGE(),' ', ERROR_PROCEDURE(),' ', ERROR_LINE() )

	IF @trancount = 0
		ROLLBACK TRANSACTION
	ELSE IF @trancount <> -1 
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION  prcCerrarCorteCaja;
 
END CATCH

_Fin:
SET NOCOUNT OFF


IF(LEN(@vchError) > 0 )	
	BEGIN
	INSERT INTO @Respuesta
		(bResultado,vchMensaje)
	SELECT 0, @vchError
END	
ELSE	
	INSERT INTO @Respuesta
	(bResultado,vchMensaje)
SELECT 1, ''

SELECT bResultado, vchMensaje
From @Respuesta