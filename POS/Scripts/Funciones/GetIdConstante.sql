
IF EXISTS ( SELECT * FROM sysobjects WHERE NAME = 'GetIdConstante' ) 
		DROP FUNCTION GetIdConstante
GO
CREATE FUNCTION GetIdConstante (
	@iIdConstante INT,
	@iOpcion      INT = 0
)
RETURNS VARCHAR( 150 )
/**
** Nombre:					GetIdConstante
** Prop�sito:				Retorna el valor de la constante homologado
** Campos:
** Dependencias:
** Error Base:
** Retorna:
**
** Autor creaci�n:			MMartinez
** Fecha creaci�n:			02/11/2021
**
** Fecha modificaci�n:		
** Revisi�n:			0
**/
BEGIN
	DECLARE @Result VARCHAR(150)

	IF (@iOpcion = 0) -- Con Id
	BEGIN
		SET @Result = ( SELECT CASE WHEN C.iIdCatConstante <> -1 THEN CONCAT ( '[', C.iIdCatConstante, '] - ', RTRIM( C.vchNombre ) )
							   ELSE ''
							   END
						FROM
							CatConstantes AS C
						WHERE
						  C.iIdCatConstante = @iIdConstante );
	END
	ELSE IF (@iOpcion = 1) -- Sin Id
	BEGIN
		SET @Result = ( SELECT CASE WHEN C.iIdCatConstante <> -1 THEN RTRIM( C.vchNombre )
							   ELSE ''
							   END
						FROM
							CatConstantes AS C
						WHERE
						  C.iIdCatConstante = @iIdConstante );
	END


	RETURN @Result
END

